package com.mainacad.module1.labs.practice;

public class Lab1 {
    public static void main(String[] args) {
        //Создать массив содержащий все четные числа от 2 до 30

        //Создать массив int нужного размера
        int[] arr = new int[15];
        //В каждый элемент массива внести значения и напечатать его в красивом формате
        for(int i=0;i<arr.length;i++)
            arr[i]=2*(i+1);
        for(int x:arr)
            System.out.printf("%2d%n",x);
    }
}
