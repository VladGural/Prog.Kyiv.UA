package com.mainacad.module1.labs.Lab1_5;

//Напечатать первые 10 чисел которые без остатка делятся на 3 или 4 из диапозона от 1 до 300
public class Lab4 {
    public static void main(String[] args) {
        int tenth=0;
        for(int i = 1;i<=300;i++) {
            if ((i % 3) == 0 || (i % 4) == 0) {
                System.out.println(i);//Если число делется без остатка на 3 или 4 выводим результат
                ++tenth;
            }
            if(tenth==10)
                break;//Остановить печать при выводе 10ти значений
        }
    }
}
