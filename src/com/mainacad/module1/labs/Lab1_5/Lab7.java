package com.mainacad.module1.labs.Lab1_5;

import java.util.Scanner;

public class Lab7 {
    public static void main(String[] args) {
        //Задача: Найти идеальные числа из диапозона
        //от 1 до велечены заданной пользователем
        //Идеальное число это число которое равно сумме его безостаточных делителей
        //которые не могут равнятся самому числу

        //Вводим верхнее значение диапозона
        System.out.println("Введите положительное натуральное число");
        System.out.println("для определения итервала 1 - n поиска perfect numbers");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        //Проходим по всем числам диапозона
        for(int i = 1;i <= n;i++){
            int sum = 0;
            //для кждого числа находим сумму безостаточных делителей
            for(int j = (i-1);j >0;j--){
                if((i%j)==0)
                    sum+=j;
            }

            //Проверяем равняется ли сумма безостаточных делителей самому числу
            if(sum == i)
                System.out.println("Perfect number is " + i);
        }
        scanner.close();
    }
}
