package com.mainacad.module1.labs.Lab1_5;

import java.util.Scanner;

public class Lab6 {
    public static void main(String[] args) {
        //Задача: подсчитать сумму квадратов чифр из введонного числа
        int sum = 0;
        //Запрашиваем пользователя ввсести число
        System.out.println("Для вычесления суммы квадратов цифр числа N введите число");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        //Подсчитываем сумму квадратов цифр числа
        while(n>0){
            //System.out.println("Выводим цифру числа " + n%10);
            sum+=Math.pow((n%10),2);
            n/=10;
            //System.out.println("Выводим изминение числа " + n);
        }
        //Вывести результат
        System.out.println("Сумма квадратов цифр числа равна " + sum);
    }
}
