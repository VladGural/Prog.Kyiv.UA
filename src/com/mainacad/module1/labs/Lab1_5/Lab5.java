package com.mainacad.module1.labs.Lab1_5;

import java.util.Scanner;

public class Lab5 {
    public static void main(String[] args) {
        System.out.println("Для расчета суммы чисел и средненго значения диапозона чисел");
        System.out.println("от 1 до N, введите значение N");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        scanner.close();
        double sum = 0,  avg;
        for(int i = 1;i<=n;i++)//Подсчет суммы чисел диапозона
            sum+=i;
        avg=sum/n;//Найти среднее арефметическое диапозона
        System.out.println("Sum from 1 to " + n + " is " + sum);//Печать суммы чисел диапозона
        System.out.println("Average from 1 to " + n + " is " + avg);//Печать среднего арефметического диапозона
    }
}
