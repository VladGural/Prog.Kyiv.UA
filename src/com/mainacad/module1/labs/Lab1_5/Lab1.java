package com.mainacad.module1.labs.Lab1_5;

public class Lab1 {
    public static void main(String[] args) {
        //Задача написать программу выводящую на консоль
        //прямоугольный треугольник
        //который заполнен цифрами
        //у которого по вертикальному катету идет возростание от 1 до 8
        //по горизонтальному убывание цифр от 8 до 1
        //и гепотинуза состоит из 1
        //вертикольно выводить числа от 1 до n где n = 8

        for(int n=1;n<9;n++) {
            System.out.print(n);
            //горизонтально выводить диапозон чисел от n-1 до 1
            for(int i = n-1;i>0;i--)
                System.out.print(i);
            System.out.println();
        }

        //Другой вариант
        System.out.println();
        for(int n = 1;n<9;n++){
            int rez=0;
            for(int i=n;i>0;i--){
                rez+=i * (int) Math.pow(10,(i-1));
            }
            System.out.println(rez);
        }
    }
}
