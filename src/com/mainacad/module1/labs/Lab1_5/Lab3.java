package com.mainacad.module1.labs.Lab1_5;

public class Lab3 {
    public static void main(String[] args) {
        System.out.println("*  |  1  2  3  4  5  6  7  8  9");
        System.out.println("-------------------------------");
        for(int i =1;i<10;i++){
            System.out.print(i+"  | ");
            for (int j=1;j<10;j++) {
                if((i * j) < 10)
                    System.out.print(" "); //Для форматорования вывода столбцов
                System.out.print(i * j + " ");
            }
            System.out.println();
        }
        //Вариант через printf()
        System.out.println();
        System.out.println("*  |  1  2  3  4  5  6  7  8  9");
        System.out.println("-------------------------------");
        for(int i =1;i<10;i++){
            System.out.print(i+"  |");
            for (int j=1;j<10;j++) {
                System.out.printf("%3d",i*j);
            }
            System.out.println();
        }
    }
}
