package com.mainacad.module1.labs.Lab1_4;

public class Lab5 {
    public static void main(String[] args) {
        int a = 15, b = 15;
        System.out.println("Variable a = " + a + "  0x" + Integer.toBinaryString(a));
        System.out.println("Variable b = " + b + "  0x" + Integer.toBinaryString(b));
        System.out.println("a>>2 = " + (a>>=2) + "  0x" + Integer.toBinaryString(a));
        System.out.println("b/(2*2) = " + (b/=(2*2)) + "  0x" + Integer.toBinaryString(b));
        System.out.println("a<<4 = " + (a<<=4) + "  0x" + Integer.toBinaryString(a));
        System.out.println("b*(2*2*2*2) = " + (b*=(2*2*2*2)) + "  0x" + Integer.toBinaryString(b));
        a = b = -125;
        System.out.println("Variable a = " + a + "  0x" + Integer.toBinaryString(a));
        System.out.println("Variable b = " + b + "  0x" + Integer.toBinaryString(b));
        System.out.println("a>>1 = " + (a>>=1) + "  0x" +
                Integer.toBinaryString(a) + "  Отрицательные числа при сдвиге в право и делении");
        System.out.println("b/(2) = " + (b/=(2)) + "  0x" +
                Integer.toBinaryString(b) + "  на двойку в степени могут давать разые значения");
        System.out.println("a>>>2 = " + (a>>>2) +  "  0x" +
                Integer.toBinaryString(a) + " При беззнаковом сдвиге отрицательных чисел теряется знак");

    }
}
