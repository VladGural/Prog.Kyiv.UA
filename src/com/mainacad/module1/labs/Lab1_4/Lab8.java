package com.mainacad.module1.labs.Lab1_4;

public class Lab8 {
    public static void main(String[] args) {
        byte b;
        int i = 128;
        double d = 12.99;
        System.out.println("Переменная int i = " + i);
        b = (byte) i; //Возможно искажение данных
        System.out.println("При приведении переменной int i (128) к переменной (byte) b мы искажаем данные " + b);
        i = (int) (8 + d); //Данные вначале приводятся к типу double, а потом явно приводятся к int с потрей дробной части
        System.out.println("(int) (8+12.99) = " + i);
        b = (byte) (b + 3);//java приводит типы к int и поэтому необходимо явно приводить их к byte при присвоении
        System.out.println("(byte) (b + 3) = " + b);
        b = 12 + 3 + 105;//константы приводить к byte не нужно, если они попадают в диапозон -128   127
        System.out.println("12 + 3 + 105 = " + b);
        b = (byte) (12 + 3 + 115);//При выходе за деопазон нужно приводить к byte и искажаем данные
        System.out.println("(byte) 12 + 3 + 115 = " + b);
    }
}