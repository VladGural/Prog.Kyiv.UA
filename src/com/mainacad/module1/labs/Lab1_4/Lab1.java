package com.mainacad.module1.labs.Lab1_4;

public class Lab1 {
    public static void main(String[] args) {
        byte b = (byte) 0b10011011;
        System.out.println("byte " + b);
        short sh = (short) 123456;
        System.out.println("short " + sh);
        int i = 983764287;
        System.out.println("int " + i);
        long l = 14_830_234_535_623_469L;
        System.out.println("long " + l);
        char ch = 'Ы';
        System.out.println("char " + ch);
        float f = 65432.98776F;
        System.out.println("float " + f);
        double d = 12.34567E4;
        System.out.println("double " + d);
        boolean bl = false;
        System.out.println("boolean " + bl);
    }
}
