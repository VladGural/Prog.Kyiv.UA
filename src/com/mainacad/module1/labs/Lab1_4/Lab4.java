package com.mainacad.module1.labs.Lab1_4;

public class Lab4 {
    public static void main(String[] args) {
        int a = 0;
        int x;
        x = ++a + a++ - ++a * a-- / --a;
        //  a=1 (a->1)
        //        a=2 (a->1)
        //              a=3 (a->3)
        //                    a=2 (a->3)
        //                          a=1 (a>1)
        //  1   +  1  - 3   * 3   / 1  =
        //  1   +  1  - 9   = -7
        System.out.println("Variable a = " + a);
        System.out.println("Variable x = " + x);
    }
}
