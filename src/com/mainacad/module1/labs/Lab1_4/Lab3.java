package com.mainacad.module1.labs.Lab1_4;

public class Lab3 {
    public static void main(String[] args) {
       int count = 0;
        System.out.println("Пример \"false & ++ count > 0\" выдает результат -> " + (false & ++ count > 0));
        System.out.println("Переменная count -> " + count);
        System.out.println("Пример \"false && ++ count > 0\" выдает результат -> " + (false && ++ count > 0));
        System.out.println("Переменная count -> " + count);
        System.out.println("Пример \"true | ++ count > 0\" выдает результат -> " + (true | ++ count > 0));
        System.out.println("Переменная count -> " + count);
        System.out.println("Пример \"true || ++ count > 0\" выдает результат -> " + (true || ++ count > 0));
        System.out.println("Переменная count -> " + count);
        System.out.println("Логические операции \"false & true && false || true ^ !false\" выдает результат -> " +
                (false & true && false || true ^ !false)); //false нв выходе
        System.out.println("Логические операции \"false && true ^ true ^ false | true\" выдает результат -> " +
                (false && true ^ true ^ false | true)); //false на выходе
    }
}
