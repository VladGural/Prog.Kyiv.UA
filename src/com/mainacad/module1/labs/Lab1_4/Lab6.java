package com.mainacad.module1.labs.Lab1_4;

public class Lab6 {
    public static void main(String[] args) {
        int a , b;
        a = 15;
        b = 9;
        System.out.println("a     " + Integer.toBinaryString(a));
        System.out.println("b     " + Integer.toBinaryString(b));
        System.out.println("a & b " + Integer.toBinaryString((a&b)));
        System.out.println();
        System.out.println("a     " + Integer.toBinaryString(a));
        System.out.println("b     " + Integer.toBinaryString(b));
        System.out.println("a | b " + Integer.toBinaryString((a|b)));
        System.out.println();
        System.out.println("a     " + Integer.toBinaryString(a));
        System.out.println("b     " + Integer.toBinaryString(b));
        System.out.println("a ^ b  " + Integer.toBinaryString((a^b)));
        System.out.println();
        System.out.println("a     " + Integer.toBinaryString(a));
        System.out.println("~a       " + Integer.toBinaryString((~a & 0xf)));
    }
}
