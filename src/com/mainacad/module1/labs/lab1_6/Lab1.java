package com.mainacad.module1.labs.lab1_6;

import java.util.Arrays;

public class Lab1 {
    public static void main(String[] args) {
        //Создать массив содержащий все четные числа от 2 до 30

        //Создать массив int нужного размера
        int[] arr = new int[15];
        //В каждый элемент массива внести значения
        for(int i=0;i<arr.length;i++)
            arr[i]=2*(i+1);
        //И напечатать его в красивом формате
        for(int x:arr)
            System.out.printf("%3d",x);//Первый варианит печати массива
        System.out.println();
        System.out.println(Arrays.toString(arr));//Второй вариант печати массива
    }
}
