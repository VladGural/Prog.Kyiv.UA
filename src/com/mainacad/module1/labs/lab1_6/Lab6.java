package com.mainacad.module1.labs.lab1_6;

import java.util.Arrays;

public class Lab6 {
    public static void main(String[] args) {
        //Задача: Написать программу для сортировки массива от отрицательных значений к положительным

        //Создаем исходный массив
        int[] tempManth = { -7, -12, 0, 5, 12, 25, 27, 21, 18, 7, -2, -9 };

        //Печатаем исходный массив
        System.out.println("Исходный массив");
        System.out.println(Arrays.toString(tempManth));

        //Сортируем исходный массив от меньшего к большему
        for(int i = 0; i < tempManth.length; i++){
            int min = 1000;
            int point = 0,temp;
            for(int j = i;j<tempManth.length;j++){
                if(tempManth[j]<min){
                    min=tempManth[j];
                    point=j;
                }
            }
            temp=tempManth[i];
            tempManth[i]=min;
            tempManth[point]=temp;
        }
        //Печатаем отсортированный массив
        System.out.println("Отсортированный массив");
        System.out.println(Arrays.toString(tempManth));
    }
}
