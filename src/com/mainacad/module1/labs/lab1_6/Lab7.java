package com.mainacad.module1.labs.lab1_6;

import java.util.Arrays;
import java.util.Scanner;

public class Lab7 {
    public static void main(String[] args) {
        //Задача: Написать пргограмму которая выводит построчно номера индексов массива
        //в которых расположенны эквивалентные значения которые задает пользователь

        //Создаем исходный массив
        int[][] array = {{1,1,1,3,4},
                         {2,1,3,1,2},
                         {2,2,3,4,1},
                         {3,3,3,1,4}};
        //Выводим исходный массив в консоль
        for(int[] x:array)
            System.out.println(Arrays.toString(x));
        System.out.println();

        //Пользователь вводит число расположение которого необходимо выяснить
        System.out.println("Введите число расположение которого в матрице Вы хотите узнать");
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        scanner.close();

        //Ищем расположение заданного числа и печатаем его построчно
        for(int i=0;i<array.length;i++){
            System.out.print("Line " + i +": [");
            boolean first = true;
            for(int j=0;j<array[i].length;j++){
                if(array[i][j]==number){
                    if(first){
                        System.out.print(j);
                        first=false;
                    }
                    else {
                        System.out.print(", " + j);
                    }
                }
            }
            System.out.println("]");
        }
    }
}
