package com.mainacad.module1.labs.lab1_6;

import java.util.Arrays;
import java.util.Scanner;

public class Lab4 {
    public static void main(String[] args) {
        //Задача используя исходный массив отсортировать его
        //и найти в нем значение введеное пользователем

        //Исходный массив
        int[] arr = { 10, 21, 5, 22, 9, 29, 25, 22, 11, 14, 8, 14};

        //Выведем значение неотсортированного массива
        System.out.print("{");
        for (int x:arr)
            System.out.print(" " + x + " ");
        System.out.println("}");

        //отсортируем массив и выводим его на консоль
        System.out.println();
        Arrays.sort(arr);
        System.out.print("{");
        for (int x:arr)
            System.out.print(" " + x + " ");
        System.out.println("}");

        //Предлагаем пользователю ввести значение искомой велечены
        System.out.println("Введите искомое знечение");
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();

        //Ищем заданное значение в массиве
        number = Arrays.binarySearch(arr,number);
        System.out.println("Данное значение находится в массиве под индексом " + number);
    }
}
