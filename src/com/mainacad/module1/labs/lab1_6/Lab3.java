package com.mainacad.module1.labs.lab1_6;

public class Lab3 {
    public static void main(String[] args) {
        //Создать матрицу матрицу 4х4, заполнить ее числами начинач с первого столбца последовательно до последнего

        //Созаем двухмерный массив 4х4
        int[][] arr = new int[4][4];

        //Заполнить массив постолбцам с 1 до 16
        int number = 1;
        for(int col=0;col<4;col++) {
            for (int row = 0; row < 4; row++) {
                arr[row][col] = number;
                ++number;
            }
        }

        //Распечатываем двухмерный массив в виде матрицы 4х4
        for(int row=0;row<4;row++) {
            for (int col = 0; col < 4; col++) {
                System.out.printf("%3d",arr[row][col]);
            }
            System.out.println();
        }
        //Печатаем второй вариант
        System.out.println();
        for(int[] rows:arr) {
            for(int x:rows){
                System.out.printf("%3d",x);
            }
            System.out.println();
        }
    }
}
