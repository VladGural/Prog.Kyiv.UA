package com.mainacad.module1.labs.lab1_6;

public class Lab5 {
    public static void main(String[] args) {
        //Задача: Транпонировать матрицы 4х4

        //Создаем первый массив и заполняем его
        int[][] arr1= {{ 1, 2, 3, 4},
                       { 5, 6, 7, 8},
                       { 9,10,11,12},
                       {13,14,15,16}};

        //Создаем второй массив
        int[][] arr2 = new int[4][4];

        //Транспонируем матрицу arr1 в arr2
        for(int i=0;i<arr1.length;i++){
            for(int j = 0;j<arr1.length;j++){
                arr2[j][i]=arr1[i][j];
            }
        }
        System.out.println("Исходная матрица");
        printArr(arr1);
        System.out.println();
        System.out.println("Транспонированная матрица");
        printArr(arr2);
    }

     static void printArr(int[][] arr){
        for(int[] array:arr) {
            for (int x : array)
                System.out.printf("%3d", x);
            System.out.println();
        }
    }
}
