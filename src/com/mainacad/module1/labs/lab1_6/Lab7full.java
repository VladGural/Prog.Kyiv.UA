package com.mainacad.module1.labs.lab1_6;

import java.util.Arrays;
import java.util.Scanner;

public class Lab7full {
    public static void main(String[] args) {
        //Задача: Написать пргограмму которая выводит построчно номера индексов массива
        //в которых расположенны эквивалентные значения которые задает пользователь

        //Создаем исходный массив
        int[][] array = {{1,1,1,3,4},
                         {2,1,3,1,2},
                         {2,2,3,4,1},
                         {3,3,3,1,4}};
        //Выводим исходный массив в консоль
        for(int[] x:array)
            System.out.println(Arrays.toString(x));
        System.out.println();

        //Пользователь вводит число расположение которого необходимо выяснить
        System.out.println("Введите число расположение которого в матрице Вы хотите узнать");
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        scanner.close();

        //Ищем расположение заданного числа и печатаем его построчно
        for(int i=0;i<array.length;i++){ //цикл строк
            System.out.print("Line " + i +": [");
            boolean first = true; //первый совпадающий элемент
            boolean minus = false; //напечатан знак "-" или нет


            for(int j=0;j<array[i].length;j++){ //цикл элементов в строке
                if(array[i][j]==number){ //если элемент равен искомому
                    if(first){ //и это первый найденный элемент в строке
                        System.out.print(j); //печатаем его индекс без "," в переди
                        first=false; //первого символ в строке уже использован
                    }
                    else if(array[i][j-1]!=number) //если это не первый найденый в строке и предидущий элемент не равен искомому
                        System.out.print("," + j); //пичатаем индекс элемента с "," впереди
                    else if(!minus) { //если предидущий элемент равен искомому но еще не печатали "-"
                        System.out.print("-"); //печатаем "-"
                        minus = true; // и помечаем что "-" напечатан
                        if ((j + 1) == array[i].length || array[i][j + 1] != number) { //если следующий элемент конец массива или не равен искомому
                            System.out.print(j); //печатаем индекс значения
                            minus = false; // сбрасываем флаг напечатанного "-"
                        }
                    }
                    else if((j+1)==array[i].length || array [i][j+1]!=number) { //если минус уже печатался не печатаем его
                            System.out.print(j);
                            minus = false;
                    }
                }
            }
            System.out.println("]");
        }
    }
}
