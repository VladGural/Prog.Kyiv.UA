package com.mainacad.module1.labs.lab1_6;


import java.util.Arrays;

public class Lab2 {
    public static void main(String[] args) {
        //Задача: Имеется массив двнных
        //Найти в нем максимальное и минимальное значения и среднее арифметическое

        //Масив данных
        int[] arr = { 10, 21, 5, 22, 9, 29, 25, 22, 11, 14, 8, 14};

        //Печатаем исходный массив
        System.out.println("Исходный массив");
        System.out.println(Arrays.toString(arr));
        System.out.println();
        int maxVal, minVal;
        double avgVal=0;
        //Найти максимальное значение
        maxVal = 0;
        for (int x:arr) {
            if(maxVal<x)
                maxVal=x;
        }
        //Находим минимальное значение
        minVal = maxVal;
        for (int x:arr){
            if(minVal>x)
                minVal=x;
        }
        //Находим среднее арифметическое
        for (int x:arr)
            avgVal+=x;
        avgVal/=arr.length;

        //Печатаем результаты
        System.out.println("Минимальное значение равно " + minVal);
        System.out.println("Макимальное значение равно " + maxVal);
        System.out.println("Среднее арефметическое равно " + avgVal);
    }
}
