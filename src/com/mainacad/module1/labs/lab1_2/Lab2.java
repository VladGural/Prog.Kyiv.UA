package com.mainacad.module1.labs.lab1_2;

import java.util.Scanner;

public class Lab2 {
    public static void main(String[] args) {
        // Вывести на консоль имя студентя которое
        // он введет с клавиатуры

        // Вывести на консоль запрос на ввод имени студента с клавиатуры
        System.out.println("Введите Ваше имя ");
        // Принять введенный данные
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();
        // Вывести введенные данные на консоль
        System.out.println("Name of student is: " + line);
        scanner.close();
    }
}
