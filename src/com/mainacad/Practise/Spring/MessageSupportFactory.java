package com.mainacad.Practise.Spring;

import java.io.FileInputStream;
import java.util.Properties;

public class MessageSupportFactory {
    private static MessageSupportFactory instance;
    private Properties props;
    private MessageRenderer renderer;
    private MessageProvider provider;
    private MessageSupportFactory(){
        props = new Properties();
        try{
            props.load(new FileInputStream("c:\\Temp\\Java\\LadWorks\\my-labs\\src\\" +
                    "com\\mainacad\\Practise\\Spring\\msf.properties"));
            String rendererClass = props.getProperty("renderer.Class");
            String providerClass = props.getProperty("provider.Class");
            renderer = (MessageRenderer) Class.forName(rendererClass).newInstance();
            provider = (MessageProvider) Class.forName(providerClass).newInstance();

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    static {
        instance = new MessageSupportFactory();
    }

    public static MessageSupportFactory getInstance(){
        return instance;
    }

    public MessageRenderer getMessageRenderer(){
        return renderer;
    }

    public MessageProvider getMessageProvider(){
        return provider;
    }
}
