package com.mainacad.Practise.Spring;

import java.util.Scanner;

public class HelloWorldScannerMessageProvider implements MessageProvider{
    @Override
    public String getMessage(){
        System.out.println("Enter Message");
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        return str;
    }
}
