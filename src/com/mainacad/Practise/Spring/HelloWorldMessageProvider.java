package com.mainacad.Practise.Spring;

public class HelloWorldMessageProvider implements MessageProvider {
    @Override
    public String getMessage(){
        return "HelloWorld";
    }
}
