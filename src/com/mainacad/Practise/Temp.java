package com.mainacad.Practise;

import com.mainacad.Practise.Spring.StandartOutMessageRenderer;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Formatter;

public class Temp {

    public static void main(String[] args) {
        BigDecimal bd1 = new BigDecimal(1);
        BigDecimal bd2 = new BigDecimal(5);
        System.out.println(bd1.divide(bd2));

        BigInteger bi1 = BigInteger.ONE;
        BigInteger bi2 = BigInteger.TWO;
        System.out.println(bi1.compareTo(bi2));

        System.out.println(Math.sin(90));
    }
}
