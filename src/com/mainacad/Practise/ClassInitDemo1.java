package com.mainacad.Practise;

public class ClassInitDemo1 {
    private int y=100;
    {
        System.out.println("inside one bloc and y = " + y);
    }
    ClassInitDemo1(){
        System.out.println("inside constructor y = " + y);
    }

    public static void main(String[] args) {
        ClassInitDemo1 cit = new ClassInitDemo1();
    }
}
