package com.mainacad.Practise;

public class ClassInstance {
    public static void main(String[] args) {
        A[] a = new A[3];
        a[0] = new A();
        a[1] = new B();
        a[2] = new A();

        int countA = 0;
        for(A as:a)
            if(as instanceof A) countA++;

        System.out.println(countA);
    }
}

class A{

}

class B extends A{

}
