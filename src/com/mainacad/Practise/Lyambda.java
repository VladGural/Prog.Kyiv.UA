package com.mainacad.Practise;

import javax.swing.*;
import java.util.Date;

public class Lyambda {
    public static void main(String[] args) {
        Timer t = new Timer(5000,event ->
                System.out.println("The time is " + new Date()));
        t.start();

        JOptionPane.showMessageDialog(null,"Quit programm?");
        System.exit(0);
    }
}
