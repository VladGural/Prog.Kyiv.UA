package com.mainacad.Practise;

public class Lambda {
    public static void main(String[] args) {
        sumOfNumber sum;
        sum = (a,b) -> a+b;
        System.out.println(sum.sumNumber(10.5,10.5));
    }
}

interface sumOfNumber{
    double sumNumber(double a,double b);
}
