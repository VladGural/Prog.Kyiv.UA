package com.mainacad.Practise;

import java.io.IOException;
import java.net.InetAddress;

public class InetAdressTest {
    public static void main(String[] args) throws IOException{
        if(args.length>0){
            String host = args[0];
            InetAddress[] addresses = InetAddress.getAllByName(host);
            for(InetAddress ia:addresses)
                System.out.println(ia);

        }else{
            InetAddress LocalHostAddress = InetAddress.getLocalHost();
            System.out.println(LocalHostAddress);
        }
    }
}
