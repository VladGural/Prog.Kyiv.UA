package com.mainacad.module2.labs.Lab2_15.Lab6;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        MyTranslator mt = new MyTranslator();
        mt.addNewWord("cat","кот");
        mt.addNewWord("catch","поймал");
        mt.addNewWord("mouse","мишь");
        mt.addNewWord("big","большую");

        System.out.println("Enter line to translate");
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        String[] strs = str.split(" ");
        for(int i=0;i<strs.length;i++){
            System.out.print(mt.translate(strs[i]) + " ");
        }
        System.out.println();
    }
}
