package com.mainacad.module2.labs.Lab2_15.Lab6;

import java.util.HashMap;

public class MyTranslator {
    private HashMap<String ,String> dictionary = new HashMap<>();

    public void addNewWord(String enW,String ruW){
        dictionary.put(enW,ruW);
    }

    public String translate(String enW){
        return dictionary.getOrDefault(enW,"");
    }
}
