package com.mainacad.module2.labs.Lab2_15.Lab4_5;

import java.util.List;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        List<Integer> list;
        MyNumGenerator mng1 = new MyNumGenerator(6,20);
        list = mng1.generate();
        System.out.println(list);

        Set<Integer> set;
        MyNumGenerator mng2 = new MyNumGenerator(6,7);
        set = mng2.generateDistinct();
        System.out.println(set);
    }
}
