package com.mainacad.module2.labs.Lab2_15.Lab4_5;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MyNumGenerator {
    private int numOfElm;
    private int maxNumb;

    MyNumGenerator(int numOfElm,int maxNumb){
        this.numOfElm=numOfElm;
        this.maxNumb=maxNumb;
    }

    public List<Integer> generate(){
        List<Integer> list = new ArrayList<>();
        for(int i=0;i<numOfElm;i++)
            list.add((int) (Math.random()*maxNumb));
        return list;
    }

    public Set<Integer> generateDistinct(){
        Set<Integer> set = new HashSet<>();
        while(set.size()!=numOfElm)
            set.add((int) (Math.random()*maxNumb));
        return set;
    }
}
