package com.mainacad.module2.labs.Lab2_15.Lab2;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        List<String> list = new LinkedList<>();

        for(int i=0;i<10;i++) {
            Random rnd = new Random();
            list.add(rnd.nextInt(i+1),"num_" + i);
        }

        for(String str:list)
            System.out.println(str);
    }
}
