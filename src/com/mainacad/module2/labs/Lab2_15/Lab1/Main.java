package com.mainacad.module2.labs.Lab2_15.Lab1;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        for(int i = 0;i<10;i++)
            list.add("number_" + i);

        for(String str:list)
            System.out.println(str);

        System.out.println();

        Iterator<String> itr = list.iterator();
        while(itr.hasNext())
            System.out.println(itr.next());

        System.out.println();

        for(int i =0;i<10;i++)
            System.out.println(list.get(i));
    }
}
