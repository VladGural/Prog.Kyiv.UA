package com.mainacad.module2.labs.Lab2_15.Lab3;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        List<String> listA = new ArrayList<>();
        List<String> listL = new LinkedList<>();

        fillOfList(listA,"numA_",10);
        fillOfList(listL,"numL_",10);

        printEllements(listA);
        printEllements(listL);

        int i=0;
        ListIterator<String> iter = listA.listIterator();
        while(iter.hasNext()) {
            listL.add(i*2+1, iter.next());
            i++;
        }

        printEllements(listL);

        List<String> listL2 = new LinkedList<>();
        ListIterator<String > iterL = listL.listIterator();
        while(iterL.hasNext())
            iterL.next();

        while(iterL.hasPrevious())
            listL2.add(iterL.previous());



        printEllements(listL2);
    }

    public static void fillOfList(List list,String str,int number){
        for(int i=0;i<number;i++)
            list.add(str + i);
    }

    public static void printEllements(List list){
        Iterator<String> itr = list.iterator();
        while(itr.hasNext())
            System.out.print(itr.next() + " ");
        System.out.println();
    }
}
