package com.mainacad.module2.labs.Lab2_14.Lab2_3;

import com.mainacad.module2.labs.Lab2_14.Lab1.MyTuple;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Integer[] ints = {11,22,33,44,55,66,77};
        Double[] dbls = {1.1,2.2,3.3,4.4,5.5,6.6,7.7};
        System.out.println("Array values " + Arrays.toString(ints));
        System.out.println("Number of elements that are greater than 33: " + MyTestMethod.calcNum(ints,33));
        System.out.println("Sum of elements that are greater than 33: " + MyTestMethod.calcSum(ints,33));
        System.out.println("Array values " + Arrays.toString(dbls));
        System.out.println("Number of elements that are greater than 5.5: " + MyTestMethod.calcNum(dbls,5.5));
        System.out.println("Sum of elements that are greater than 5.5: " + MyTestMethod.calcSum(dbls,5.5));
    }
}
