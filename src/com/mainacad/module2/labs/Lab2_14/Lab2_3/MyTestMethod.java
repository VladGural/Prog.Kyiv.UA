package com.mainacad.module2.labs.Lab2_14.Lab2_3;

public class MyTestMethod {
    public static <T extends Comparable> int calcNum(T[] array,T maxElem){
        int number = 0;
        for(T item:array){
            if(item.compareTo(maxElem)>0)
                number++;
        }
        return number;
    }

    public static <T extends Number & Comparable> double calcSum(T[] array,T maxElem){
        double sum = 0;
        for(T item:array){
            if(item.compareTo(maxElem)>0)
                sum+=item.doubleValue();
        }
        return sum;
    }
}
