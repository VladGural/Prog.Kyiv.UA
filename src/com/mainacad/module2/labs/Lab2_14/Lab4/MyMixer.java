package com.mainacad.module2.labs.Lab2_14.Lab4;

public class MyMixer <T> {
    T[] array;

    MyMixer(T[] array){
        this.array = array;
    }
    public T[] shuffle(){
        int number = array.length;
        for(int i = 0; i<number; i++){
            int randomPoss = (int) (Math.random()*number);
            T temp = array[i];
            array[i] = array[randomPoss];
            array[randomPoss] = temp;
        }
        return array;
    }
}
