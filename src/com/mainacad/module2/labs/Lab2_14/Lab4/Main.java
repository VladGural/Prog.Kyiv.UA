package com.mainacad.module2.labs.Lab2_14.Lab4;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {


        String[] arrstr = {"one", "two", "three", "four", "five"};
        Integer[] arrint = {1, 2, 3, 4, 5};

        MyMixer mixString = new MyMixer(arrstr);
        mixString.shuffle();

        System.out.println(Arrays.toString(arrstr));

        MyMixer mixInteger = new MyMixer(arrint);
        mixInteger.shuffle();

        System.out.println(Arrays.toString(arrint));
    }
}
