package com.mainacad.module2.labs.Lab2_14.Lab1;

public class Main {
    public static void main(String[] args) {
        MyTuple<String,Integer,Long> mt1 = new MyTuple<>("Hello",10,20L);
        System.out.println(mt1.toString());
        MyTuple<Double,String,String> mt2 = new MyTuple<>(20.4,"String 1","String 2");
        System.out.println(mt2.toString());
    }
}
