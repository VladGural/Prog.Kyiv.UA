package com.mainacad.module2.labs.Lab2_11.Lab3;

public class Main2_11_3 {
    public static void main(String[] args) {
        Person person = new Person();
        person.setFirstName("Vova");
        person.setLastName("Generalov");
        try{
            person.setAge(140);
        }catch(InvalidAgeException e){
            System.out.println("Значения возраста выходяи за рамки 1-120");
        }
    }
}

class Person {
    private String firstName;
    private String lastName;
    private int age;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if(age<1 || age>120) throw new InvalidAgeException();
        this.age = age;
    }
}

class InvalidAgeException extends RuntimeException{

}