package com.mainacad.module2.labs.Lab2_11.Lab4;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String str;
        System.out.println("Enter string of shape to create new Shape");
        Scanner scanner = new Scanner(System.in);
        str = scanner.next();

        Shape shape=null;
        try {
            shape = Shape.parseShape(str);
        }catch(InvalidShapeStringException e){
            System.out.println("Invalid String of Shape");
            return;
        }
        System.out.println(shape);
    }
}
