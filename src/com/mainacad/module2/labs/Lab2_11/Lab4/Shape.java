package com.mainacad.module2.labs.Lab2_11.Lab4;

import jdk.jshell.spi.ExecutionControlProvider;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Shape {
    //Переменная цвет
    private String shapeColour;

    //Конструктор
    public Shape(String shapeColour){
        this.shapeColour = shapeColour;
    }

    public static Shape parseShape(String str)
    throws InvalidShapeStringException
    {
        Pattern pat = Pattern.compile("[A-Z][a-z]+:[A-Z]+:\\d+,?\\d*,?\\d*");
        Matcher mat = pat.matcher(str);
        if(!mat.matches())
            throw new InvalidShapeStringException();

        String[] strs = str.split("[:,]");
        Shape shp = null;
        switch(strs[0].toLowerCase()){
            case "rectangle":
                shp = new Rectangle(strs[1],Double.parseDouble(strs[2]),Double.parseDouble(strs[3]));
                break;
            case "circle":
                shp = new Circle(strs[1],Double.parseDouble(strs[2]));
                break;
            case "triangle":
                shp = new Triangle(strs[1],Double.parseDouble(strs[2]),Double.parseDouble(strs[3]),Double.parseDouble(strs[4]));
                break;
        }
        return shp;
    }

    //Метод возвращает строку названия класса БЕЗ предшествующего названия пакета
    public String getShapeName(){
        String str = getClass().getName();
        int index = str.lastIndexOf('.');
        if(index>0)
            str = str.substring(++index);
        return str;
    }

    //Переопределяем метод toString
    @Override
    public String toString() {
        return "This is " + getShapeName() +
                ", colour is " + shapeColour;
    }

    //Пустой метод calcArea
    public double calcArea(){
        return 0.0;
    }
}

class InvalidShapeStringException extends Exception{

}
