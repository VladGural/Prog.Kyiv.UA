package com.mainacad.module2.labs.Lab2_11.Lab1_2;

//Lab2_11_1
public class Main {
    public static void main(String[] args) {
        try{
            throw new Exception("We throw and catch Exception");
            //System.exit(0);
        }catch(Exception e){
            System.out.println(e);
        }finally {
            System.out.println("We are inside finally");
        }
    }
}

//Lab2_11_2_1
class MyException extends Exception {
    String message;

    public MyException(String str){
        message = str;
    }

    public void printMyException(){
        System.out.println(message);
    }

//Lab2_11_2_1
    public static void main(String[] args) {
        try {
            throw new MyException("Throw and catch MyException");
        }catch(MyException e){
            e.printMyException();
        }
    }
}

//Lab2_11_2_2
class MyTest {
    public void test()
    throws MyException
    {
        throw new MyException("Throw MyExeption from method test");
    }

    public static void main(String[] args)
    //throws MyException
    {
        //Lab2_11_2_2
        MyTest mt = new MyTest();
        try {
            mt.test();
        } catch (MyException e) {
            e.printMyException();
        }

        //Lab2_11_2_3
        MyTest myTestNull = null;
        try {
            myTestNull.test();
        } catch (MyException e) {
            e.printMyException();
        } catch (NullPointerException e) {
            throw new NullPointerException("Null error");
        }
    }
}


