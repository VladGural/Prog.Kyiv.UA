package com.mainacad.module2.labs.Lab2_4;

//Lab2_4_5
public class MyCalc {

    public static double calcPi(int n){
        final int FOUR = 4;
        double pi=0;
        for(int i = 0; i<n; i++){
            if(i%2==0)
                pi+=(double)FOUR/((i<<1)+1);
            else
                pi-=(double)FOUR/((i<<1)+1);
        }
        return pi;
    }

    public static void main(String[] args) {
        System.out.println("Value of number pi is " + calcPi(10000));
    }
}
