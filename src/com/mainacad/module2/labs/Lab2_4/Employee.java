package com.mainacad.module2.labs.Lab2_4;

//Lab2_4_4
public class Employee {
    private String firstName;
    private String lastName;
    private String occupation;
    private String telephoneNamber;
    private static int numberOfEmployees;

    public Employee(String firstName, String lastName, String occupation, String telephoneNamber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.occupation = occupation;
        this.telephoneNamber = telephoneNamber;
        numberOfEmployees++;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", occupation='" + occupation + '\'' +
                ", telephoneNamber='" + telephoneNamber + '\'' +
                ", namberOfEmployees='" + numberOfEmployees + '\'' +
                '}';
    }

    public static void main(String[] args) {
        Employee em1 = new Employee("Vasya","Gnedoy","Engineer","050-328-11-11");
        System.out.println(em1.toString());
        Employee em2 = new Employee("Vova","Dremov","Engineer","050-328-11-12");
        System.out.println(em2.toString());
        Employee em3 = new Employee("Yaric","Stepnoy","Engineer","050-328-11-13");
        System.out.println(em3.toString());

        System.out.println("Field 'numberOfEmployees' is " + numberOfEmployees);
    }
}
