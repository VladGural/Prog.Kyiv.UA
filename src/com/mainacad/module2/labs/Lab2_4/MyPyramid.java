package com.mainacad.module2.labs.Lab2_4;

import java.util.InputMismatchException;
import java.util.Scanner;

public class MyPyramid {
    public static void printPyramid(int numberOfLines){
        int numberOfLastLine = (numberOfLines << 1) - 1; //Число чимел в последней строке

        for(int i = 1; i <= numberOfLines; i++){//Печатаем линии
            int numberOfLine = (i << 1) - 1;//Количество печатаемых чисел в текущей строке
            int numberOfLinePlus = (numberOfLine >> 1) + 1;//Количество возрастающих символов в текущей линии
            int voidOfLine = (numberOfLastLine - numberOfLine) >> 1;//Количество пробелов слева в текущей строке
            for(int j = 1; j <= voidOfLine; j++)//Печатаем пробелы слева в текущей линии
                System.out.print(" ");
            int number = 0;//Печатаемая цифра в текущей строке
            for(int j = 1; j <= numberOfLine; j++) {//Печатаем числа в текущей линии
                if(j <= numberOfLinePlus)
                    System.out.print(++number);
                else
                    System.out.print(--number);
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        System.out.println("Для построения пирамидки из цифр");
        System.out.println("Необходимо ввести число в диапозоне от 1 до 9 (количество линий в пирамиде");
        int numberOfLines=0;
        try{
            Scanner scanner = new Scanner(System.in);
            numberOfLines = scanner.nextInt();
            scanner.close();
        }catch(InputMismatchException e){
            System.out.println("Нужно ввести значение типа int");
            return;
        }


        if(numberOfLines<1 || numberOfLines >9){
            System.out.println("Введенное значение находится вне диапозона 1-9");
            return;
        }

        MyPyramid.printPyramid(numberOfLines);
    }
}