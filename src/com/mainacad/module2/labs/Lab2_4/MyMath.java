package com.mainacad.module2.labs.Lab2_4;

//Lab2_4_1 Lab2_4_3

import java.util.Arrays;

public class MyMath {
    public static final double PI = 3.14;


    public static int findMin(int[] arr){
        Arrays.sort(arr);
        return arr[0];
    }

    public static int findMax(int[] arr){
        Arrays.sort(arr);
        return arr[(arr.length-1)];
    }

    public static double areaOfCircle(double radius){
        return PI*Math.pow(radius,2);
    }
}

//Lab2_4_2
class Calculation{
    public static void main(String[] args) {
        int[] arr1 = {12,34,-9,100};
        System.out.println("Array1 is " + Arrays.toString(arr1));
        int[] arr2 = {-123,-5,12,67};
        System.out.println("Array2 is " + Arrays.toString(arr2));
        System.out.println("Min value of array1 is " + MyMath.findMin(arr1));
        System.out.println("Max value of array2 is " + MyMath.findMax(arr2));
        System.out.print("Area of Cercle with radius 12.4 is ");
        System.out.printf("%.2f",MyMath.areaOfCircle(12.4));
    }
}
