package com.mainacad.module2.labs.Lab2_4;

import java.util.InputMismatchException;
import java.util.Scanner;

public class GravityCalculator {
    private static final double A_EARTH = -9.81;

    public static double calcDist(double time){
        return (A_EARTH * Math.pow(time,2))/2;
    }

    public static void main(String[] args) {
        System.out.println("Введите время (секунд) и программа расчитает");
        System.out.println("на какое расстояние (метров) упадет тело за это время");
        double time=0;

        try{
            Scanner scanner = new Scanner(System.in);
            time = scanner.nextDouble();
            scanner.close();
        }catch(InputMismatchException e){
            System.out.println("Нужно ввести значение типа double");
            return;
        }
        System.out.println("За промежуток времени " + time + " секунд");
        System.out.println("тело упадет на рассояние " + calcDist(time) + " метров");
    }
}
