package com.mainacad.module2.labs.Lab2_12.Lab1_2;

public class Main {
    public static void main(String[] args) {
        MyPhoneBook mpb = new MyPhoneBook();

        mpb.addPhoneNumber("Vova","050 328 09 87");
        mpb.addPhoneNumber("Dima","097 123 45 43");
        mpb.addPhoneNumber("Nika","050 245 12 33");

        System.out.println("PhoneBook");
        mpb.printPhoneBook();
        System.out.println();

        System.out.println("Sort PhoneBook by Name");
        mpb.sortByName();
        mpb.printPhoneBook();
        System.out.println();

        System.out.println("Sort PhoneBook by PhoneNumber");
        mpb.sortByPhoneNumber();
        mpb.printPhoneBook();
        System.out.println();
    }
}
