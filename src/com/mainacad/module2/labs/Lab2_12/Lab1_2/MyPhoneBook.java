package com.mainacad.module2.labs.Lab2_12.Lab1_2;

import java.util.Arrays;
import java.util.Comparator;

public class MyPhoneBook {
    public static class PhoneNumber{
        private String name;
        private String phone;

        public PhoneNumber(String name,String phone){
            this.name = name;
            this.phone = phone;
        }

        public String getName() {
            return name;
        }

        public String getPhone() {
            return phone;
        }

        @Override
        public String toString() {
            return "PhoneNumber{" +
                    "name='" + name + '\'' +
                    ", phone='" + phone + '\'' +
                    '}';
        }
    }

    private PhoneNumber[] phoneNumbers = new PhoneNumber[10];

    public void addPhoneNumber(String name, String phone){
        PhoneNumber phn = new PhoneNumber(name,phone);
        for(int i =0;i<phoneNumbers.length;i++){
            if (phoneNumbers[i] == null) {
                phoneNumbers[i] = phn;
                return;
            }
        }
    }

    public void printPhoneBook(){
        for(PhoneNumber phn:phoneNumbers){
            if(phn!=null)
                System.out.println(phn.toString());
        }
    }

    public void sortByName(){
        Arrays.sort(phoneNumbers, new Comparator<PhoneNumber>() {
            @Override
            public int compare(PhoneNumber o1, PhoneNumber o2) {
                if(o1!=null && o2!=null)
                    return o1.name.compareToIgnoreCase(o2.name);
                else
                    return 0;
            }
        });
    }

    public void sortByPhoneNumber(){
        Arrays.sort(phoneNumbers, new Comparator<PhoneNumber>() {
            @Override
            public int compare(PhoneNumber o1, PhoneNumber o2) {
                if (o1 != null && o2 != null)
                    return o1.phone.compareToIgnoreCase(o2.phone);
                else
                    return 0;
            }
        });
    }
}
