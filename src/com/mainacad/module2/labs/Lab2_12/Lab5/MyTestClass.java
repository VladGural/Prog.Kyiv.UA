package com.mainacad.module2.labs.Lab2_12.Lab5;

import com.mainacad.module2.labs.Lab2_12.Lab4.MyPhone;
import com.mainacad.module2.labs.Lab2_5.MyInit;

public class MyTestClass {

    MyTestClass(){
        System.out.println("Inside MyTestClass constructor");
    }

    public void test(){
        class MyLocal{
            final int x = 10;
            public void testLocal(){
                System.out.println("Inside class MyLocal x = " + x);
            }
        }

        MyLocal ml = new MyLocal();
        ml.testLocal();
    }

    static class V{
        V(){
            System.out.println("Inside static class V constructor");
        }

    }

    class MyInner{
        MyInner(){
            System.out.println("Inside MyInner class constructor");
        }
    }
}
