package com.mainacad.module2.labs.Lab2_12.Lab3;

public class Main {
    public static void main(String[] args) {
        MyPhone myPhone = new MyPhone();
        myPhone.switchOn();

        System.out.println("My Phone Book");
        myPhone.printMyPhoneBook();
        System.out.println();

        myPhone.call(1);
    }
}
