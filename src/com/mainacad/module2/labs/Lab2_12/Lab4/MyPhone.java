package com.mainacad.module2.labs.Lab2_12.Lab4;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Random;

public class MyPhone {
    private MyPhoneBook myPhoneBook;

    public MyPhone(){
        myPhoneBook = new MyPhoneBook();
    }

    public void switchOn(){
        Random rnd = new Random();
        for(int i=0;i<myPhoneBook.phoneNumbers.length;i++){
            String name = String.valueOf((char)(rnd.nextInt(26) + 'a'));
            String phone = String.valueOf(rnd.nextInt(1000000));

            myPhoneBook.addPhoneNumber(name,phone);
        }

        System.out.println("Loading PhoneBook records... Ok!");
    }

    public void call(int number){
        System.out.print("Calling to ");
        System.out.println(myPhoneBook.getPhoneNumbers()[number].toString());
    }

    public void printMyPhoneBook(){
        myPhoneBook.printPhoneBook();
    }

    public class MyPhoneBook {
        public class PhoneNumber{
            private String name;
            private String phone;

            public PhoneNumber(String name,String phone){
                this.name = name;
                this.phone = phone;
            }

            public String getName() {
                return name;
            }

            public String getPhone() {
                return phone;
            }

            @Override
            public String toString() {
                return "PhoneNumber{" +
                        "name='" + name + '\'' +
                        ", phone='" + phone + '\'' +
                        '}';
            }
        }



        private PhoneNumber[] phoneNumbers = new PhoneNumber[10];

        public PhoneNumber[] getPhoneNumbers() {
            return phoneNumbers;
        }

        public void addPhoneNumber(String name, String phone){
            PhoneNumber phn = new PhoneNumber(name,phone);
            for(int i =0;i<phoneNumbers.length;i++){
                if (phoneNumbers[i] == null) {
                    phoneNumbers[i] = phn;
                    return;
                }
            }
        }

        public void printPhoneBook(){
            for(PhoneNumber phn:phoneNumbers){
                if(phn!=null)
                    System.out.println(phn.toString());
            }
        }

        public void sortByName(){
            Arrays.sort(phoneNumbers, new Comparator<PhoneNumber>() {
                @Override
                public int compare(PhoneNumber o1, PhoneNumber o2) {
                    if(o1!=null && o2!=null)
                        return o1.name.compareToIgnoreCase(o2.name);
                    else
                        return 0;
                }
            });
        }

        public void sortByPhoneNumber(){
            Arrays.sort(phoneNumbers, new Comparator<PhoneNumber>() {
                @Override
                public int compare(PhoneNumber o1, PhoneNumber o2) {
                    if(o1!=null && o2!=null)
                        return o1.phone.compareToIgnoreCase(o2.phone);
                    else
                        return 0;
                }
            });
        }
    }

    //Lab4
    //Все представленные ниже классы не будут обращаться
    //к полям и методам класса MyPhone
    //поэтому их лкчше объявить как ststic

    static class Camera{

    }
    static class Bluetooth{

    }
    static class SimCard{

    }
    static class MemoryCard{

    }
    static class PowerOnButton{

    }
    static class HeadPhone{

    }
    static class PhoneBattery{

    }
    static class PhoneCharger{

    }
    static class PhoneDisplay{

    }
    static class PhoneSpeaker{

    }
}
