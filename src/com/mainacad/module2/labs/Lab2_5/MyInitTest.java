package com.mainacad.module2.labs.Lab2_5;

import com.mainacad.module2.labs.Lab2_4.MyPyramid;


//Lab2_5_1
public class MyInitTest {
    static {
        System.out.println("static initialisation block1");
    }

    static {
        System.out.println("static initialisation block2");
    }

    {
        System.out.println("non-static initialisation block1");
    }

    {
        System.out.println("non-static initialisation block2");
    }

    MyInitTest(){
        this(10);
        System.out.println("non arguments constuctor");
    }

    MyInitTest(int i){
        System.out.println("int argument conctructor = " + i);
    }

    public static void main(String[] args) {
        System.out.println("Main");
        MyInitTest myclass = new MyInitTest();
        MyInitTest myclass1 = new MyInitTest(100);
    }
}
