package com.mainacad.module2.labs.Lab2_5;

public class InitTest {
    private int id;
    private static int nextId;

    static{
        System.out.print("Inside static initialisation and nextId is ");
        nextId = (int) (Math.random ()*1000);
        System.out.println(nextId);
    }

    {
        System.out.println("inside non-static block ++nextId");
        id = ++nextId;
    }

    public int getNextId() {
        return id;
    }

    public static void main(String[] args) {
        InitTest it1 = new InitTest();
        System.out.println("Value of id is " + it1.getNextId());

        InitTest it2 = new InitTest();
        System.out.println("Value of id is " + it2.getNextId());

        InitTest it3 = new InitTest();
        System.out.println("Value of id is " + it3.getNextId());

        InitTest it4 = new InitTest();
        System.out.println("Value of id is " + it4.getNextId());

        InitTest it5 = new InitTest();
        System.out.println("Value of id is " + it5.getNextId());
    }
}
