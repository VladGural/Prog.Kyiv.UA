package com.mainacad.module2.labs.Lab2_5;

import java.util.Arrays;

public class MyInit {

    //Lab2_5_2
    /*private int[] arr = new int[10];

    {
        System.out.println("Inside in non-static block");
        for(int i = 0; i<10;i++)
            arr[i]=(int)(100*Math.random());
    }*/

    //Lab2_5_3
    static int[] arr = new int[10];

    {
        System.out.println("Inside in non-static block");
        for(int i = 0; i<10;i++)
            arr[i]=(int)(100*Math.random());
    }

    //Lab2_5_4
    /*static int[] arr = new int[10];

    static{
        System.out.println("Inside in non-static block");
        for(int i = 0; i<10;i++)
            arr[i]=(int)(100*Math.random());
    }*/

    public void printArray(){
        System.out.println("Print array" + Arrays.toString(arr));
        System.out.println("And Hashcode of array " + arr.hashCode());
    }

    public static void main(String[] args) {
        MyInit mi1 = new MyInit();
        mi1.printArray();
        MyInit mi2 = new MyInit();
        mi2.printArray();

    }
}
