package com.mainacad.module2.labs.Lab2_18;

import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

public class Lab3 {
    public static void main(String[] args) {
        Locale myLocale = new Locale("ukr","UA");
        NumberFormat nf = NumberFormat.getInstance(myLocale);
        NumberFormat nfc = NumberFormat.getCurrencyInstance(myLocale);
        Date dt = new Date();

        System.out.println("Carrent Locale: "+myLocale.getDisplayLanguage()+" "+myLocale.getDisplayCountry());
        System.out.println("Integer: "+nf.format(1234567890));
        System.out.println("Double: "+nf.format(123123.4567));
        System.out.println("Carrency: "+nfc.format(1234567.54));
        System.out.println("Date: "+dt);
        System.out.println();

        myLocale = Locale.ITALY;
        nf = NumberFormat.getInstance(myLocale);
        nfc = NumberFormat.getCurrencyInstance(myLocale);
        dt = new Date();

        System.out.println("Carrent Locale: "+myLocale.getDisplayLanguage()+" "+myLocale.getDisplayCountry());
        System.out.println("Integer: "+nf.format(1234567890));
        System.out.println("Double: "+nf.format(123123.4567));
        System.out.println("Carrency: "+nfc.format(1234567.54));
        System.out.println("Date: "+dt);
        System.out.println();

        myLocale = Locale.CHINA;
        nf = NumberFormat.getInstance(myLocale);
        nfc = NumberFormat.getCurrencyInstance(myLocale);
        dt = new Date();

        System.out.println("Carrent Locale: "+myLocale.getDisplayLanguage()+" "+myLocale.getDisplayCountry());
        System.out.println("Integer: "+nf.format(1234567890));
        System.out.println("Double: "+nf.format(123123.4567));
        System.out.println("Carrency: "+nfc.format(1234567.54));
        System.out.println("Date: "+dt);
        System.out.println();
    }
}
