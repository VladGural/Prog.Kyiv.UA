package com.mainacad.module2.labs.Lab2_18;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Locale;

public class Lab2 {
    public static void main(String[] args) {
        BigDecimal money = BigDecimal.valueOf(1.7);
        BigDecimal cost = BigDecimal.valueOf(0.1);
        int count = 0;
        while(money.compareTo(cost)>=0){
            count++;
            money = money.subtract(cost);
            cost=cost.add(BigDecimal.valueOf(0.1));
        }
        NumberFormat nf =  NumberFormat.getCurrencyInstance(Locale.US);
        System.out.println("We can buy " + count + " nails");
        System.out.println("and now you have " + nf.format(money) + " money");
    }
}
