package com.mainacad.module2.labs.Lab2_18;

import java.io.IOException;

public class Lab4 {
    public static void main(String[] args) {
        String[] str = {"calc.exe",
                        "charmap.exe",
                        "notepad.exe"};

        Runtime rt = Runtime.getRuntime();
        try {
            for(int i=0;i<str.length;i++)
                rt.exec(str[i]);
        }catch(IOException e){};
    }
}
