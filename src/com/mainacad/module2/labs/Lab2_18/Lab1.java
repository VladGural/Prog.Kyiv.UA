package com.mainacad.module2.labs.Lab2_18;

import java.math.BigInteger;

public class Lab1 {
    public static void main(String[] args) {
        int n;
        n=(int) (Math.random()*40 + 10);
        System.out.println(n+"! = "+factorial(n));
    }

    public static BigInteger factorial(int n){
        BigInteger bi = BigInteger.valueOf(1);
        for(int i=1;i<=n;i++){
            bi=bi.multiply(BigInteger.valueOf(i));
        }
        return bi;
    }
}
