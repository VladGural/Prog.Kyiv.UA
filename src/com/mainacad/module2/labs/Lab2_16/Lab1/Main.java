package com.mainacad.module2.labs.Lab2_16.Lab1;

public class Main {
    public static void main(String[] args) {
        Animal anm = new Dog();
        foo(anm);
        foo(new Dog());
    }

    public static void foo(Animal anm){
        System.out.println("Method Animal");
    }

    public static void foo(Dog dg){
        System.out.println("Method Dog");
    }
}
