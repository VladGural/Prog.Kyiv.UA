package com.mainacad.module2.labs.Lab2_16.Lab2;

public class Main {
    public static void main(String[] args) {
        byte b = 5;
        foo(b);
        Byte bt = 5;
        foo(bt);
    }

    public static void foo(int i){
        System.out.println("int");
    }

    public static void foo(Byte b){
        System.out.println("Byte");
    }
}
