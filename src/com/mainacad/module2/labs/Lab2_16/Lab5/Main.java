package com.mainacad.module2.labs.Lab2_16.Lab5;

public class Main {
    public static void main(String[] args) {
        byte b = 127;
        foo(b);
        foo(5);
        Byte bt = new Byte((byte)5);
        foo(bt);
        Double db = new Double(5.5);
        foo(db.intValue());
        Integer intg = new Integer(100);
        foo(intg);
    }

    public static void foo(int i){
        System.out.println("int");
    }

    public static void foo(byte b){
        System.out.println("byte");
    }
}
