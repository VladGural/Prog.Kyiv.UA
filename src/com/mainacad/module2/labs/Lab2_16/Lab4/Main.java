package com.mainacad.module2.labs.Lab2_16.Lab4;

import java.util.Objects;

public class Main {
    public static void main(String[] args) {
        foo(null);
        Puppy pp = null;
        foo(pp);
        Dog dg = null;
        foo(dg);
        Animal anm = null;
        foo(anm);
        Object obj = null;
        foo(obj);
    }

    public static void foo(Animal anm){
        System.out.println("Method Animal");
    }

    public static void foo(Dog dg){
        System.out.println("Method Dog");
    }

    public static void foo(Puppy pp){
        System.out.println("Method Puppy");
    }

    public static void foo(Object obj){
        System.out.println("Method Object");
    }
}
