package com.mainacad.module2.labs.Lab2_16.Lab3;

public class Main {
    public static void main(String[] args) {
        int a = 1;
        int b = 2;
        int c = 3;
        foo(a);
        foo(a,b);
        foo(a,b,c);
    }

    public static void foo(int a,int b){
        System.out.println("int a and b");
    }

    public static void foo(int ... a){
        System.out.println("array's content");
    }
}
