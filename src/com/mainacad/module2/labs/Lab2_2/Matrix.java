package com.mainacad.module2.labs.Lab2_2;

public class Matrix {
    void printImputMatixes(int[][] MatrixOne,int[][] MatrixTwo){
        System.out.println("Printing MatrixOne");
        printArray(MatrixOne);
        System.out.println("Printing MatrixTwo");
        printArray(MatrixTwo);
    }

    void sumMatrixes(int[][] MatrixOne, int[][] MatrixTwo){
        if(MatrixOne.length!=MatrixTwo.length || MatrixOne[0].length != MatrixTwo[0].length){
            System.out.println("These two Matrixes is not equal dimension");
            return;
        }

        System.out.println();
        System.out.println("Sum of two matrix is");
        for(int i=0;i<MatrixOne.length;i++){
            for(int j=0;j<MatrixOne[i].length;j++) {
                System.out.printf("%3d", MatrixOne[i][j] + MatrixTwo[i][j]);
            }
            System.out.println();
        }
    }

    void multiplicationMatrixes(int[][] MatrixOne, int[][] MatrixTwo){
        if(MatrixOne.length!=MatrixTwo.length || MatrixOne[0].length != MatrixTwo[0].length){
            System.out.println("These two Matrixes is not equal dimension");
            return;
        }

        System.out.println();
        System.out.println("Multiplication of two matrix is");
        for(int i=0;i<MatrixOne.length;i++){
            for(int j=0;j<MatrixOne[i].length;j++) {
                System.out.printf("%3d", MatrixOne[i][j] * MatrixTwo[i][j]);
            }
            System.out.println();
        }
    }



    public static void printArray(int[][] arr){
        for(int[] array:arr) {
            for (int x : array)
                System.out.printf("%3d", x);
            System.out.println();
        }
    }
}
