package com.mainacad.module2.labs.Lab2_2;

public class People {
    public static void main(String[] args) {
        Person prs = new Person();
        prs.setPersonData("Vasya","Ivanov");
        System.out.println(prs.toString());
        prs.setPersonData("Kolya","Pupkin",28,50_328_12_91);
        System.out.println(prs.toString());
        prs.setPersonData("Man");
        System.out.println(prs.toString());
    }
}
