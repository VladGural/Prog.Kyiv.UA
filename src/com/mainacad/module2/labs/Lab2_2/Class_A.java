package com.mainacad.module2.labs.Lab2_2;

public class Class_A {
    void calcSquare(int width){
        //Метод расчета площади квадрата
        System.out.println("Площадь квадрата с шириной сторон " + width + " равна " + (int) Math.pow(width,2));
    }

    void calcSquare(int width, int length){
        //Метол расчета площиди прямоугольника
        System.out.println("Площадь прямоугольника с шириной и длиной сторон " + width + " " + length +
                " равна " + (int) width * length);
    }

    void calcSquare(int radius, double pi){
        //Метод расчета площади окружности
        System.out.print("Площадь окружности с радиусом " + radius + " равна ");
        System.out.printf("%.2f",pi*Math.pow(radius,2));
        System.out.println();
    }
}
