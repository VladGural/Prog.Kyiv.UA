package com.mainacad.module2.labs.Lab2_2;

public class Class_A_Test {
    public static void main(String[] args) {
        final double PI = 3.14;

        Class_A class_a = new Class_A();

        class_a.calcSquare(10);
        class_a.calcSquare(10,20);
        class_a.calcSquare(10,PI);
    }
}
