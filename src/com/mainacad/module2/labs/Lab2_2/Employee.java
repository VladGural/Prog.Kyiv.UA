package com.mainacad.module2.labs.Lab2_2;

public class Employee {
    void calcSalary(String name,double... salary){
        double sum=0;
        for(double x:salary)
            sum+=x;
        System.out.print("Employee name is : " + name + " and his total salary is : ");
        System.out.printf("%.2f",sum);
        System.out.println();
    }
}
