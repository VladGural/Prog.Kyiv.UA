package com.mainacad.module2.labs.Lab2_2;

public class Main {
    public static void main(String[] args) {
        Matrix mtr = new Matrix();

        int[][] MatrixOne = {{1,2,3},{4,5,6},{7,8,9}};
        int[][] MatrixTwo = {{9,8,7},{6,5,4},{3,2,1}};

        mtr.printImputMatixes(MatrixOne,MatrixTwo);
        mtr.sumMatrixes(MatrixOne,MatrixTwo);
        mtr.multiplicationMatrixes(MatrixOne,MatrixTwo);
    }
}
