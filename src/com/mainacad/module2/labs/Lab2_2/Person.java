package com.mainacad.module2.labs.Lab2_2;

public class Person {
    private String firstName;
    private String lastName;
    private int age;
    private String gender;
    private int phoneNamber;

    public void setPersonData(String firstName, String lastName){
        this.firstName=firstName;
        this.lastName=lastName;
    }

    public void setPersonData(String firstName, String lastName,int age, int phoneNamber){
        this.firstName=firstName;
        this.lastName=lastName;
        this.age=age;
        this.phoneNamber=phoneNamber;
    }

    public void setPersonData(String gender){
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", gender='" + gender + '\'' +
                ", phoneNamber=" + phoneNamber +
                '}';
    }
}
