package com.mainacad.module2.labs.Lab2_7.Lab6_7_8_9_10;

public class Main {
    public static void main(String[] args) {
        //Lab2_7_6
        Shape shp = new Shape("Red");
        System.out.println(shp);
        System.out.println(shp.getShapeName() + " area is " + shp.calcArea() + "\n");

        //Lab2_7_7
        Circle crc = new Circle("Green", 12.5);
        System.out.println(crc);
        System.out.println(crc.getShapeName() + " area is " + String.format("%.2f",crc.calcArea()) + "\n");

        //Lab2_7_8
        Rectangle rect = new Rectangle("Yellow",12.5,10.4);
        System.out.println(rect);
        System.out.println(rect.getShapeName() + " area is " + String.format("%.2f",rect.calcArea()) + "\n");

        //Lab2_7_9
        Triangle trg = new Triangle("Black", 5, 5,5);
        System.out.println(trg);
        System.out.println(trg.getShapeName() + " area is " + String.format("%.2f",trg.calcArea()) + "\n");

        //Lab2_7_10
        Shape[] shapes = new Shape[9];

        shapes[0] = new Rectangle("Red", 10, 20);
        shapes[1] = new Circle("Green", 20);
        shapes[2] = new Rectangle("Black", 15,25);
        shapes[3] = new Triangle("Yellow", 3, 4, 5);
        shapes[4] = new Rectangle("Grey", 30,20);
        shapes[5] = new Triangle("Pink", 10, 10, 15);
        shapes[6] = new Rectangle("Blue", 5,2);
        shapes[7] = new Circle("White", 10);
        shapes[8] = new Rectangle("Purple", 20,30);

        for(Shape shps:shapes){
            System.out.println(shps + " area is " + String.format("%.2f",shps.calcArea()));
        }
        System.out.println();

        double sumArea = 0;
        for(Shape shps:shapes)
            sumArea+=shps.calcArea();
        System.out.println("Area of all Shapes is " + String.format("%.2f",sumArea));
        System.out.println();

        double sumRectangleArea = 0;
        double sumTriangleArea = 0;
        double sumCircleArea = 0;

        for(Shape shps:shapes){
            if(shps instanceof Rectangle)
                sumRectangleArea+=shps.calcArea();
            else if(shps instanceof Triangle)
                sumTriangleArea+=shps.calcArea();
            else if(shps instanceof Circle)
                sumCircleArea+=shps.calcArea();
        }
        System.out.println("Area of all Rectangles is " + String.format("%.2f",sumRectangleArea));
        System.out.println("Area of all Triangles is " + String.format("%.2f",sumTriangleArea));
        System.out.println("Area of all Circles is " + String.format("%.2f",sumCircleArea));
    }
}
