package com.mainacad.module2.labs.Lab2_7.Lab6_7_8_9_10;

public class Triangle extends Shape{
    //Переменные размеры сторон треугольника
    private double a;
    private double b;
    private double c;

    //Конструктор
    public Triangle(String colour, double a, double b, double c){
        super(colour);
        this.a = a;
        this.b = b;
        this.c = c;
    }

    //Переопределяем метод toSrting
    @Override
    public String toString() {
        return super.toString() +
                ", a is " + a +
                ", b is " + b +
                ", c is " + c;
    }

    //Переопределяем метод calcArea
    @Override
    public double calcArea() {
        double s = 0;
        double result = 0;
        s = (a+b+c)/2;
        result = Math.sqrt(s * (s - a) * (s - b) * (s - c));
        return result;
    }
}
