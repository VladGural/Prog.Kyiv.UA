package com.mainacad.module2.labs.Lab2_7.Lab6_7_8_9_10;

public class Shape {
    //Переменная цвет
    private String shapeColour;

    //Конструктор
    public Shape(String shapeColour){
        this.shapeColour = shapeColour;
    }

    //Метод возвращает строку названия класса БЕЗ предшествующего названия пакета
    public String getShapeName(){
        String str = getClass().getName();
        int index = str.lastIndexOf('.');
        if(index>0)
            str = str.substring(++index);
        return str;
    }

    //Переопределяем метод toString
    @Override
    public String toString() {
        return "This is " + getShapeName() +
                ", colour is " + shapeColour;
    }

    //Пустой метод calcArea
    public double calcArea(){
        return 0.0;
    }
}


