package com.mainacad.module2.labs.Lab2_7.Lab6_7_8_9_10;

public class Circle extends Shape {
    //Переменная Радиус
    private double radius;

    //Конструктор
    public Circle(String colour, double radius){
        super(colour);
        this.radius = radius;
    }

    //Переопределяем метод toSrting
    @Override
    public String toString() {
        return super.toString() +
                ", radius is " + radius;
    }

    //Переопределяем метод calcArea
    @Override
    public double calcArea(){
        return Math.PI*Math.pow(radius,2);
    }
}
