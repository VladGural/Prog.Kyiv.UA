"C:\Program Files\Java\jdk-9.0.4\bin\java" "-javaagent:C:\Program Files\JetBrains\IntelliJ IDEA Community Edition 2017.3.4\lib\idea_rt.jar=50277:C:\Program Files\JetBrains\IntelliJ IDEA Community Edition 2017.3.4\bin" -Dfile.encoding=UTF-8 -classpath C:\Temp\Java\LadWorks\my-labs\out\production\my-labs com.mainacad.module2.labs.Lab2_7.Lab6_7_8_9_10.Main
This is Shape, colour is Red
Shape area is 0.0

This is Circle, colour is Green, radius is 12.5
Circle area is 490,87

This is Rectangle, colour is Yellow, width is 12.5, height is 10.4
Rectangle area is 130,00

This is Triangle, colour is Black, a is 5.0, b is 5.0, c is 5.0
Triangle area is 10,83

This is Rectangle, colour is Red, width is 10.0, height is 20.0 area is 200,00
This is Circle, colour is Green, radius is 20.0 area is 1256,64
This is Rectangle, colour is Black, width is 15.0, height is 25.0 area is 375,00
This is Triangle, colour is Yellow, a is 3.0, b is 4.0, c is 5.0 area is 6,00
This is Rectangle, colour is Grey, width is 30.0, height is 20.0 area is 600,00
This is Triangle, colour is Pink, a is 10.0, b is 10.0, c is 15.0 area is 49,61
This is Rectangle, colour is Blue, width is 5.0, height is 2.0 area is 10,00
This is Circle, colour is White, radius is 10.0 area is 314,16
This is Rectangle, colour is Purple, width is 20.0, height is 30.0 area is 600,00

Area of all Shapes is 3411,40

Area of all Rectangles is 1785,00
Area of all Triangles is 55,61
Area of all Circles is 1570,80

Process finished with exit code 0
