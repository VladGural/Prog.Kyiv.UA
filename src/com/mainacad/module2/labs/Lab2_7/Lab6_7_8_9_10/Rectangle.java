package com.mainacad.module2.labs.Lab2_7.Lab6_7_8_9_10;

public class Rectangle extends Shape{
    //Переменные размеры сторон прямоугольника
    private double width;
    private double height;

    //Конструктор
    public Rectangle(String colour, double width, double height){
        super(colour);
        this.width = width;
        this.height = height;
    }

    //Переопределяем метод toSrting
    @Override
    public String toString() {
        return super.toString() +
                ", width is " + width +
                ", height is " + height;
    }

    //Переопределяем метод calcArea
    @Override
    public double calcArea(){
        return width*height;
    }
}
