package com.mainacad.module2.labs.Lab2_7.Lab1_2_3_4_5;

import java.util.Objects;

public class Device {
    private String manufacturer;
    private float price;
    private String serialNumber;

    public String getManufacturer() {
        return manufacturer;
    }

    public float getPrice() {
        return price;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    @Override
    public String toString() {
        return "Device{" +
                "manufacturer='" + manufacturer + '\'' +
                ", price=" + String.format("%.2f",price) +
                ", serialNumber='" + serialNumber + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Device)) return false;
        Device device = (Device) o;
        return Float.compare(device.getPrice(), getPrice()) == 0 &&
                Objects.equals(getManufacturer(), device.getManufacturer()) &&
                Objects.equals(getSerialNumber(), device.getSerialNumber());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getManufacturer(), getPrice(), getSerialNumber());
    }
}

class Monitor extends Device {
    private int resolutionX;
    private int resolutionY;

    public int getResolutionX() {
        return resolutionX;
    }

    public void setResolutionX(int resolutionX) {
        this.resolutionX = resolutionX;
    }

    public int getResolutionY() {
        return resolutionY;
    }

    public void setResolutionY(int resolutionY) {
        this.resolutionY = resolutionY;
    }

    @Override
    public String toString() {
        return "Monitor{" +
                "manufacturer='" + getManufacturer() + '\'' +
                ", price=" + String.format("%.2f",getPrice()) +
                ", serialNumber='" + getSerialNumber() + '\'' +
                ", resolutionX=" + resolutionX +
                ", resolutionY=" + resolutionY +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Monitor)) return false;
        if (!super.equals(o)) return false;
        Monitor monitor = (Monitor) o;
        return getResolutionX() == monitor.getResolutionX() &&
                getResolutionY() == monitor.getResolutionY();
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), getResolutionX(), getResolutionY());
    }
}

class EthernetAdapter extends Device {
    private int speed;
    private String mac;

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    @Override
    public String toString() {
        return "EthernetAdapter{" +
                "manufacturer='" + getManufacturer() + '\'' +
                ", price=" + String.format("%.2f",getPrice()) +
                ", serialNumber='" + getSerialNumber() + '\'' +
                ", speed=" + speed +
                ", mac='" + mac + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EthernetAdapter)) return false;
        if (!super.equals(o)) return false;
        EthernetAdapter that = (EthernetAdapter) o;
        return getSpeed() == that.getSpeed() &&
                Objects.equals(getMac(), that.getMac());
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), getSpeed(), getMac());
    }
}

class Main {
    public static void main(String[] args) {
        Object[] devices = new Object[2];

        Monitor monitor = new Monitor();
        monitor.setManufacturer("DELL");
        monitor.setPrice(152.80f);
        monitor.setSerialNumber("AS23J87S7456");
        monitor.setResolutionX(1920);
        monitor.setResolutionY(1080);
        devices[0] = monitor;

        EthernetAdapter ethrnetAdapter = new EthernetAdapter();
        ethrnetAdapter.setManufacturer("IBM");
        ethrnetAdapter.setPrice(75.50f);
        ethrnetAdapter.setSerialNumber("173YYMSG8765");
        ethrnetAdapter.setSpeed(1000);
        ethrnetAdapter.setMac("927436723884");
        devices[1] = ethrnetAdapter;

        for (Object d:devices) {
            System.out.println(d);
        }
    }
}