package com.mainacad.module2.labs.Lab2_3;

public class Box {
    private int width;
    private int height;

    Box(int width,int height){
        //this.width = width;
        //this.height = height;

        width=width;
        height=height;
    }

    @Override
    public String toString() {
        return "Box{" +
                "width=" + width +
                ", height=" + height +
                '}';
    }

    public static void main(String[] args) {
        Box obj = new Box(11,200);
        System.out.println(obj.toString());
    }
}
