package com.mainacad.module2.labs.Lab2_3;

public class ClassConstructor {
    public static void main(String[] args) {
        B b = new B();
        System.out.println(b.toString());
    }
}

class A{
    private String colour;

    A(){
        System.out.println("Начало конструктора класса A colour = " + colour);
        this.colour = "Red";
        System.out.println("Конец конструктора класса A colour = " + colour);
    }

    String getColour(){
        return this.colour;
    }
}

class B extends A{
    private String colour;
    private int length;

    B(){
        System.out.println("Начало конструктора класса B colour = " + colour);
        System.out.println("Начало конструктора класса B length = " + length);
        this.colour = "Green";
        this.length = 100;
        System.out.println("Конец конструктора класса B colour = " + colour);
        System.out.println("Конец конструктора класса B length = " + length);
    }

    String getColour(){
        return this.colour;
    }

    @Override
    public String toString() {
        return  "A{" +
                "colour='" + super.getColour() + "'}" +
                "B{" +
                "colour='" + this.getColour() + '\'' +
                ", length=" + this.length +
                '}';
    }
}
