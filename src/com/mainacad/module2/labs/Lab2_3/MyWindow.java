package com.mainacad.module2.labs.Lab2_3;

public class MyWindow {
    private double width;
    private double height;
    private int numberOfGlass;
    private String colour;
    private boolean isOpen;

    public MyWindow(){
        this.width = 1.5;
        this.height = 1.2;
        this.numberOfGlass = 3;
        this.colour = "Green";
        this.isOpen = false;
    }

    public MyWindow(double width,double height){
        this();
        this.width = width;
        this.height = height;
    }

    public MyWindow(double width,double height,int numberOfGlass){
        this();
        this.width = width;
        this.height = height;
        this.numberOfGlass = numberOfGlass;
    }

    public MyWindow(double width,double height,int numberOfGlass, String colour){
        this();
        this.width = width;
        this.height = height;
        this.numberOfGlass = numberOfGlass;
        this.colour = colour;
    }

    public MyWindow(double width,double height,int numberOfGlass, String colour, boolean isOpen){
        this.width = width;
        this.height = height;
        this.numberOfGlass = numberOfGlass;
        this.colour = colour;
        this.isOpen = isOpen;
    }

    @Override
    public String toString() {
        return "MyWindow{" +
                "width=" + width +
                ", height=" + height +
                ", numberOfGlass=" + numberOfGlass +
                ", colour='" + colour + '\'' +
                ", isOpen=" + isOpen +
                '}';
    }

    public void printFields(){
        System.out.println(toString());
    }
}

class MyWindowMain{
    public static void main(String[] args) {
        MyWindow myWindowDefault = new MyWindow();
        myWindowDefault.toString();

        MyWindow[] arrayMyWindows = new MyWindow[5];
        arrayMyWindows[0] = myWindowDefault;
        arrayMyWindows[1] = new MyWindow(2,2);
        arrayMyWindows[2] = new MyWindow(2.5,0.9, 4);
        arrayMyWindows[3] = new MyWindow(1.0,2.2, 2, "Red");
        arrayMyWindows[4] = new MyWindow(0.8,0.5, 1, "Yellow", true);

        for(MyWindow obj:arrayMyWindows){
            obj.printFields();
            System.out.println();
        }
    }
}