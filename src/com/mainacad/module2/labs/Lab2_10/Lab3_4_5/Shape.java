package com.mainacad.module2.labs.Lab2_10.Lab3_4_5;

public class Shape {
    //Переменная цвет
    private String shapeColour;

    //Конструктор
    public Shape(String shapeColour){
        this.shapeColour = shapeColour;
    }

    public static Shape parseShape(String str){
        String[] strs = str.split(":|,");
        Shape shp = null;
        switch(strs[0].toLowerCase()){
            case "rectangle":
                shp = new Rectangle(strs[1],Double.parseDouble(strs[2]),Double.parseDouble(strs[3]));
                break;
            case "circle":
                shp = new Circle(strs[1],Double.parseDouble(strs[2]));
                break;
            case "triangle":
                shp = new Triangle(strs[1],Double.parseDouble(strs[2]),Double.parseDouble(strs[3]),Double.parseDouble(strs[4]));
                break;
        }
        return shp;
    }

    //Метод возвращает строку названия класса БЕЗ предшествующего названия пакета
    public String getShapeName(){
        String str = getClass().getName();
        int index = str.lastIndexOf('.');
        if(index>0)
            str = str.substring(++index);
        return str;
    }

    //Переопределяем метод toString
    @Override
    public String toString() {
        return "This is " + getShapeName() +
                ", colour is " + shapeColour;
    }

    //Пустой метод calcArea
    public double calcArea(){
        return 0.0;
    }
}


