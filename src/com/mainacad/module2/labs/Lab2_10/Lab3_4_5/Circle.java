package com.mainacad.module2.labs.Lab2_10.Lab3_4_5;

public class Circle extends Shape {
    //Переменная Радиус
    private double radius;

    //Конструктор
    public Circle(String colour, double radius){
        super(colour);
        this.radius = radius;
    }

    //Переопределяем метод toSrting
    @Override
    public String toString() {
        return super.toString() +
                ", radius is " + radius;
    }

    //Переопределяем метод calcArea
    @Override
    public double calcArea(){
        return Math.PI*Math.pow(radius,2);
    }
}
