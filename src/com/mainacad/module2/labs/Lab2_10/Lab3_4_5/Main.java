package com.mainacad.module2.labs.Lab2_10.Lab3_4_5;


public class Main {
    public static void main(String[] args) {
        Shape[] shapes = new Shape[3];

        shapes[0] = Shape.parseShape("Rectangle:RED:10,20");
        shapes[1] = Shape.parseShape("Triangle:GREEN:9,7,12");
        shapes[2] = Shape.parseShape("Circle:BLEAK:10");

        for(Shape shps:shapes){
            System.out.println(shps + " area is " + String.format("%.2f",shps.calcArea()));
        }
        System.out.println();
    }
}
