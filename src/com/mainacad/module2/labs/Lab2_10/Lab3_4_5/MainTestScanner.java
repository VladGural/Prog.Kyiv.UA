package com.mainacad.module2.labs.Lab2_10.Lab3_4_5;

import java.util.Scanner;

public class MainTestScanner {
    public static void main(String[] args) {
        System.out.println("Введите количество желаемых фигур");

        Scanner scanner = new Scanner(System.in);
        int numberShape = scanner.nextInt();

        Shape[] shapes = new Shape[numberShape];

        System.out.println("Введите параметры фигур, и введите q - для завершениея ввода");

        int i = 0;
        while(true){
            String str = scanner.next();

            if(!str.equals("q")) {
                shapes[i] = Shape.parseShape(str);
                i++;
            }
            else
                break;
        }
        for(Shape shps:shapes){
            System.out.println(shps + " area is " + String.format("%.2f",shps.calcArea()));
        }
    }
}
