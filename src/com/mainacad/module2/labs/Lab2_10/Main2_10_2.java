package com.mainacad.module2.labs.Lab2_10;

public class Main2_10_2 {
    public static void main(String[] args) {
        Byte bt = new Byte((byte)5);
        Integer in = new Integer(10);
        Long ln = compute(bt,in);
        System.out.println(ln);
    }

    public static Long compute(Byte bt,Integer in){
        Long ln = (long) (bt + in);
        return ln;
    }
}
