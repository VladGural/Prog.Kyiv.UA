package com.mainacad.module2.labs.lab2_1;

public class Main {
    public static void main(String[] args) {
        Computer[] computers = new Computer[5];
        for (int i=0;i<computers.length;i++){
            Computer computer = new Computer();
            computer.setPrice(Math.round(1000*Math.random()));
            computer.setFrequencyCPU((int)(1000*Math.random()));
            computer.setManufacturer("DELL");
            computer.setQuantityCPU((int)(10*Math.random()));
            computer.setSerialNumber((int)(10000*Math.random()));
            computers[i]=computer;
        }
        System.out.println("Выводим заполненный массив из 5 объектов класса \"Computer\"");
        Computer.view(computers);

        for(int i=0;i<computers.length;i++){
            float price = computers[i].getPrice();
            price*=1.1;
            computers[i].setPrice(price);
        }

        System.out.println();
        System.out.println("Выводим заполненный массив из 5 объектов класса \"Computer\" с увеличенным на 10% полем price ");
        Computer.view(computers);
    }
}
