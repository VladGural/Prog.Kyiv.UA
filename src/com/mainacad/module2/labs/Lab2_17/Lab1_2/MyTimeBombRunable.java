package com.mainacad.module2.labs.Lab2_17.Lab1_2;

public class MyTimeBombRunable implements Runnable {
    Thread t;

    MyTimeBombRunable(){
        t = new Thread(this);
        t.start();
    }

    @Override
    public void run() {
        for(int i=10;i>=0;i--){
            System.out.println(i);
            try{
                Thread.sleep(500);
            }catch (InterruptedException e){
                System.out.println("Thread was interrupted");
            }
        }
        System.out.println("Boom!!!");
    }
}
