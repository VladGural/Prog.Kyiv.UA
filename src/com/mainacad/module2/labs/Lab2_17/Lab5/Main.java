package com.mainacad.module2.labs.Lab2_17.Lab5;

public class Main {
    public static void main(String[] args) {
        Storage storage = new Storage();
        int numberCounting = 15;

        Counter counter = new Counter(storage,numberCounting);
        Printer printer = new Printer(storage,numberCounting);

        counter.start();
        printer.start();
    }
}
