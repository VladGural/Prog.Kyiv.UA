package com.mainacad.module2.labs.Lab2_17.Lab5;

public class Storage {
    private int value=0;
    private boolean flag;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }
}
