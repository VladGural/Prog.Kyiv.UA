package com.mainacad.module2.labs.Lab2_17.Lab5;

import java.beans.IntrospectionException;

public class Counter extends Thread {
    private Storage storage;
    private int number;

    public Counter(Storage storage, int number) {
        this.storage = storage;
        this.number = number;
    }

    @Override
    public void run() {
        for(int i=0;i<number;i++){
            //try {
            //    Thread.sleep(500);
            //}catch(InterruptedException e){}
            synchronized (storage){
                if(storage.isFlag())
                    try {
                        storage.wait();
                    }catch(InterruptedException e){}
                storage.setValue(i);
                storage.setFlag(true);
                storage.notifyAll();
            }
        }
    }
}
