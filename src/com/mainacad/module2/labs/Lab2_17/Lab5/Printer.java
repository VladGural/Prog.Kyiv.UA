package com.mainacad.module2.labs.Lab2_17.Lab5;

public class Printer extends Thread{
    private Storage storage;
    private int number;

    public Printer(Storage storage, int number) {
        this.storage = storage;
        this.number = number;
    }

    @Override
    public void run() {
        for(int i=0;i<number;i++){
            synchronized (storage){
                if(!storage.isFlag())
                    try {
                        storage.wait();
                    }catch(InterruptedException e){}
                System.out.println(storage.getValue());
                storage.setFlag(false);
                storage.notifyAll();
            }
        }
    }
}
