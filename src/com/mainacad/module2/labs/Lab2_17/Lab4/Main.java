package com.mainacad.module2.labs.Lab2_17.Lab4;

import com.mainacad.module2.labs.Lab2_17.Lab4.MySumCount;

public class Main {
    public static void main(String[] args) {
        int[] myArray = new int[1000];
        for(int i=0; i<myArray.length;i++)
            myArray[i]=(int)(Math.random()*1000);

        new MySumCount(100,900,myArray);
        new MySumCount(400,600,myArray);
    }
}
