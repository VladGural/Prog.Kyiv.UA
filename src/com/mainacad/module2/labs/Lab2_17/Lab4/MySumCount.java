package com.mainacad.module2.labs.Lab2_17.Lab4;

public class MySumCount implements Runnable {
    static int numberThread = 0;
    private int startIndex;
    private int stopIndex;

    private int[] array;
    private long resultSum;

    MySumCount(int startIndex, int stopIndex, int[] array){
        this.startIndex=startIndex;
        this.stopIndex=stopIndex;
        this.array=array;
        System.out.println("Start of " + ++numberThread + " thread");
        Thread t = new Thread(this);
        t.start();
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public int getStopIndex() {
        return stopIndex;
    }

    public void setStopIndex(int stopIndex) {
        this.stopIndex = stopIndex;
    }

    public void setArray(int[] array) {
        this.array = array;
    }

    public long getResultSum() {
        return resultSum;
    }

    @Override
    public void run() {
        resultSum=0;
        for(int i=startIndex;i<=stopIndex;i++){
            resultSum+=array[i];
        }

        try {
            Thread.sleep((int) (Math.random() * 1000));
        }catch(InterruptedException e){}
        System.out.println("["+array[stopIndex]+
                ","+array[stopIndex+1]+
                ","+array[stopIndex+2]+
                ",...,"+array[stopIndex]+"]");
        System.out.println("Sum of elements from " + startIndex + " to " + stopIndex + " of array is " + resultSum);
    }
}
