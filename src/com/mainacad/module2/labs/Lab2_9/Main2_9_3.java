package com.mainacad.module2.labs.Lab2_9;

import java.util.Arrays;

public class Main2_9_3 {
    public static void main(String[] args) {
        char[] chars;
        String str = "Using methods of class String";
        System.out.println("Original string is");
        System.out.println("\"" + str + "\"");
        chars = uniqueChars(str);
        System.out.println("Print string from unique char of original string");
        System.out.print("\"");
        for(char cr:chars)
            System.out.print(cr);
        System.out.println("\"");
    }

    public static char[] uniqueChars(String str){
        int[] numberOfChar = new int[str.length()];
        for(int i = 0;i<str.length();i++){
            int counter = 0;
            for(int j = 0;j<str.length();j++){
                if(str.charAt(i)==str.charAt(j))
                    counter++;
            }
            numberOfChar[i]=counter;
        }

        int notUnique = 0;
        for(int i=0;i<numberOfChar.length;i++){
            if(numberOfChar[i]>1)
                notUnique++;
        }

        char[] uniqueChars = new char[str.length()-notUnique];
        for(int i=0,j=0;i<str.length();i++){
            if(numberOfChar[i]==1){
                uniqueChars[j]=str.charAt(i);
                j++;
            }
        }

        return uniqueChars;
    }
}
