package com.mainacad.module2.labs.Lab2_9;

public class Main2_9_2 {
    public static void main(String[] args) {
        String myStr1 = "Cartoon";
        String myStr2 = "Tomcat";
        System.out.println("First string is \"" + myStr1 + "\"");
        System.out.println("Second string is \"" + myStr2 + "\"");
        System.out.print("Letters which is in first string but isn't in second is \"");
        for(int i = 0;i<myStr1.length();i++){
            if(myStr2.indexOf(myStr1.charAt(i))==-1)
                System.out.print(myStr1.charAt(i));
        }
        System.out.println("\"");
    }
}
