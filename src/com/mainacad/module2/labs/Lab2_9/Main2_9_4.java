package com.mainacad.module2.labs.Lab2_9;

import java.util.StringTokenizer;

public class Main2_9_4 {
    public static void main(String[] args) {
        String str = "This is String, split by StringTokenizer. " +
                "Created by Student: Gural Vladyslave";
        StringTokenizer st = new StringTokenizer(str," ,.");
        System.out.println("Original string is \"" + str + "\"");
        while(st.hasMoreTokens()){
            System.out.println(st.nextToken());
        }

    }
}
