package com.mainacad.module2.labs.Lab2_9;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main2_9_5 {
    public static void main(String[] args) {
        String[] strings = {"VOVA",
                            "Ivan",
                            "R2d2",
                            "ZX",
                            "Anna",
                            "12345",
                            "ToAd",
                            "TomCat"};
        System.out.println("Из исходного набора строк");
        for(String str:strings)
            System.out.print(str + " ");
        System.out.println("\n");

        System.out.println("Выбираем строки состоящие только");
        System.out.println("из букв латинского алфавита,");
        System.out.println("причем первая буква должна быть заглавная");
        for(int i=0;i<strings.length;i++){
            if(checkPersonWithRegExp(strings[i]))
                System.out.print(strings[i] + " ");
        }
    }

    public static boolean checkPersonWithRegExp(String userNameString){
        Pattern pat = Pattern.compile("^[A-Z][a-z]+");
        Matcher mat = pat.matcher(userNameString);
        return mat.matches();
    }
}
