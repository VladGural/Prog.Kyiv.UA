package com.mainacad.module2.labs.Lab2_9;

public class Main2_9_1 {
    public static void main(String[] args) {
        String myStr = "abracadabra";
        System.out.println("String is \"" + myStr + "\"");
        System.out.println("Index of first \"ra\" is " + myStr.indexOf("ra"));
        System.out.println("Index of last \"ra\" is " + myStr.lastIndexOf("ra"));
        System.out.println("Substring from 3 to 7 indexes is \"" + myStr.substring(3,7) + "\"");
        System.out.println("Method reverseString do this \"" + reverseString(myStr) + "\"");
    }

    public static String reverseString(String str){
        char[] chars = new char[str.length()];
        String result = null;
        for(int i = 0; i<str.length(); i++){
            chars[i] = str.charAt((str.length()-1)-i);
        }
        result = new String(chars);
        return result;
    }
}
