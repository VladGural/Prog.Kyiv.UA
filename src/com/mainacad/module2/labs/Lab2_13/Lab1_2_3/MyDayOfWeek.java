package com.mainacad.module2.labs.Lab2_13.Lab1_2_3;

public enum MyDayOfWeek {
    SUNDAY,
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY;

    public MyDayOfWeek nextDay(){
        MyDayOfWeek[] mdow = MyDayOfWeek.values();
        int ordinalNamber = ordinal();
        if(ordinalNamber == 6)
            ordinalNamber = 0;
        else
            ++ordinalNamber;

        return mdow[ordinalNamber];
    }
}
