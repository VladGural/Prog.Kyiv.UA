package com.mainacad.module2.labs.Lab2_13.Lab1_2_3;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        //Lab1
        for(MyDayOfWeek mdow:MyDayOfWeek.values())
            System.out.println(mdow.name());
        System.out.println();

        //Lab2
        for(MyDayOfWeek mdow:MyDayOfWeek.values()){
            switch(mdow) {
                case TUESDAY:
                case THURSDAY:
                    System.out.println("My Java Day is " + mdow.name());
            }
        }
        System.out.println();

        //Lab3
        System.out.println("Enter day of week");
        Scanner scanner = new Scanner(System.in);
        String str = scanner.next();
        try {
            MyDayOfWeek nextDay = MyDayOfWeek.valueOf(str.toUpperCase()).nextDay();
            System.out.println("Next Day of week is " + nextDay);
        }catch(IllegalArgumentException e){
            System.out.println("You enter invalid day of week");
        }
    }
}

