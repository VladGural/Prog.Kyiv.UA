package com.mainacad.module2.labs.Lab2_13.Lab5;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        TrainSchedule tsh = new TrainSchedule(4);
        tsh.addTrain();
        System.out.println();

        System.out.println("Schadule of Trains");
        tsh.printTrains();
        System.out.println();

        System.out.println("Enter train to serch");
        Scanner scanner = new Scanner(System.in);
        String str;
        String[] strs;
        str = scanner.next();
        strs = str.split(",");

        System.out.println("You need these trains");
        tsh.searchTrain(strs[0],strs[1],strs[2]);
    }
}
