package com.mainacad.module2.labs.Lab2_13.Lab5;

import java.util.Arrays;
import java.util.Scanner;

public class TrainSchedule {

    class Train {
        private int number;
        private String stationDispatch;
        private String stationArrival;
        private String timeDispatch;
        private String timeArrival;
        private MyDayOfWeek daysOfWeek;

        public Train(int number) {
            this.number = number;
        }

        public int getNumber() {
            return number;
        }

        public void setNumber(int number) {
            this.number = number;
        }

        public String getStationDispatch() {
            return stationDispatch;
        }

        public void setStationDispatch(String stationDispatch) {
            this.stationDispatch = stationDispatch;
        }

        public String getStationArrival() {
            return stationArrival;
        }

        public void setStationArrival(String stationArrival) {
            this.stationArrival = stationArrival;
        }

        public String getTimeDispatch() {
            return timeDispatch;
        }

        public void setTimeDispatch(String timeDispatch) {
            this.timeDispatch = timeDispatch;
        }

        public String getTimeArrival() {
            return timeArrival;
        }

        public void setTimeArrival(String timeArrival) {
            this.timeArrival = timeArrival;
        }

        public MyDayOfWeek getDays() {
            return daysOfWeek;
        }

        public void setDays(MyDayOfWeek days) {
            this.daysOfWeek = days;
        }

        @Override
        public String toString() {
            return "Train{" +
                    "number=" + number +
                    ", stationDispatch='" + stationDispatch + '\'' +
                    ", stationArrival='" + stationArrival + '\'' +
                    ", timeDispatch='" + timeDispatch + '\'' +
                    ", timeArrival='" + timeArrival + '\'' +
                    ", days=" + daysOfWeek +
                    '}';
        }
    }

    private Train[] trains;

    public TrainSchedule(int numberOfTrains){
        trains = new Train[numberOfTrains];
    }

    public void addTrain(){
        Scanner scanner = new Scanner(System.in);
        for(int i =0;i<trains.length;i++) {

            String str;
            String[] strs;

            System.out.println("Enter number of Train, stations Dispatch and Arrival,");
            System.out.println("time Dispatch and Arrival, day of week");
            str = scanner.next();
            strs = str.split(",");
            System.out.println();

            Train train = new Train(new Integer(strs[0]));
            train.setStationDispatch(strs[1]);
            train.setStationArrival(strs[2]);
            train.setTimeDispatch(strs[3]);
            train.setTimeArrival(strs[4]);
            train.setDays(MyDayOfWeek.valueOf(strs[5].toUpperCase()));
            trains[i]=train;
        }
    }

    public void printTrains(){
        for(int i =0;i<trains.length;i++){
            System.out.println(trains[i].toString());
        }
    }

    public void searchTrain(String stationDispatch, String stationArrival, String day){
        int index = MyDayOfWeek.valueOf(day.toUpperCase()).ordinal();
        for(int i =0 ;i < trains.length; i++){
            if(trains[i].getStationDispatch().equalsIgnoreCase(stationDispatch)
                    && trains[i].getStationArrival().equalsIgnoreCase(stationArrival)
                    && trains[i].getDays().ordinal()>index)
                System.out.println(trains[i].toString());
        }
    }
}
