package com.mainacad.module2.labs.Lab2_13.Lab4;

public class Card {
    private Suit cardSuit;
    private Rank cardRank;

    Card(Suit cardSuit,Rank cardRank){
        this.cardSuit = cardSuit;
        this.cardRank = cardRank;
    }

    @Override
    public String toString() {
        return "The Card:" + cardRank.name() + "_" + cardSuit.name();
    }
}
