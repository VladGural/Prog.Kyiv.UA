package com.mainacad.module2.labs.Lab2_13.Lab4;

public class Main {
    public static void main(String[] args) {
        Card[] cards = new Card[52];
        Suit[] suits = Suit.values();
        Rank[] ranks = Rank.values();
        for(int i = 0;i < suits.length;i++){
            for(int j = 0; j < ranks.length;j++){
                cards[(i * ranks.length + j)] = new Card(suits[i],ranks[j]);
            }
        }
        for(int i = 0;i<cards.length;i++)
            System.out.println(cards[i]);
    }
}
