package com.mainacad.module2.labs.Lab2_6.com.brainacad.carpack;

public class Car {
    private static String colour = "Red";
    private double weight;

    public static String getColour(){
        return colour;
    }

    public double getMass(){
        return weight;
    }

    public void setMass(double weight){
        this.weight = weight;
    }
}
