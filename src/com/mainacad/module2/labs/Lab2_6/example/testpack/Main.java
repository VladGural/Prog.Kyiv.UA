package com.mainacad.module2.labs.Lab2_6.example.testpack;

import com.mainacad.module2.labs.Lab2_6.com.brainacad.carpack.Car;
import static com.mainacad.module2.labs.Lab2_6.com.brainacad.carpack.Car.getColour;//Добавим статический метод
import com.mainacad.module2.labs.Lab2_6.example.applepack.Apple;

public class Main {
    public static void main(String[] args) {
        Apple apple = new Apple();
        apple.setMass(10);
        System.out.println("Weight of Apple is " + apple.getMass());

        Car car = new Car();
        car.setMass(170);
        System.out.println("Weight of Car is " + car.getMass());

        System.out.println("Colour of Car is " + getColour());
    }
}
