package com.mainacad.module2.labs.Lab2_6.example.applepack;

public class Apple {
    private double weight;

    public double getMass(){
        return weight;
    }

    public void setMass(double weight){
        this.weight = weight;
    }
}
