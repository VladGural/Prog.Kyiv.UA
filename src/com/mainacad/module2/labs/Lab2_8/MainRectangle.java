package com.mainacad.module2.labs.Lab2_8;

import java.util.Arrays;

public class MainRectangle {
    public static void main(String[] args) {
        Rectangle rct1 = new Rectangle("Red", 20, 30);
        Rectangle rct2 = new Rectangle("Yellow", 40,20);
        rct1.draw();
        rct2.draw();

        System.out.println(rct1.compareTo(rct2));
        System.out.println(rct2.compareTo(rct1));

        Rectangle[] rectangles = new Rectangle[6];
        for(int i = 0; i<6; i++){
            rectangles[i] = new Rectangle("Red", Math.random()*10, Math.random()*10);
        }

        Arrays.sort(rectangles);

        for(Rectangle rectangle:rectangles)
            rectangle.draw();
    }
}
