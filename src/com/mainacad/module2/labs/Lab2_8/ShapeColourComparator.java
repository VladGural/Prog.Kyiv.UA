package com.mainacad.module2.labs.Lab2_8;

import java.util.Comparator;

public class ShapeColourComparator implements Comparator<Shape> {
    @Override
    public int compare(Shape o1, Shape o2) {
        return o1.getColour()
               .compareToIgnoreCase(o2.getColour());
    }
}
