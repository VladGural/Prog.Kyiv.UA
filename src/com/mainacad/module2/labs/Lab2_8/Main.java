package com.mainacad.module2.labs.Lab2_8;


public class Main {
    public static void main(String[] args) {
        Shape[] shapes = new Shape[9];

        shapes[0] = new Rectangle("Red", 10, 20);
        shapes[1] = new Circle("Green", 20);
        shapes[2] = new Rectangle("Black", 15,25);
        shapes[3] = new Triangle("Yellow", 3, 4, 5);
        shapes[4] = new Rectangle("Grey", 30,20);
        shapes[5] = new Triangle("Pink", 10, 10, 15);
        shapes[6] = new Rectangle("Blue", 5,2);
        shapes[7] = new Circle("White", 10);
        shapes[8] = new Rectangle("Purple", 20,30);

        for(Shape shps:shapes){
            shps.draw();
        }
        System.out.println();
    }
}
