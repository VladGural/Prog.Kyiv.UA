package com.mainacad.module2.labs.Lab2_8;

public class Rectangle extends Shape implements Comparable {
    //Переменные размеры сторон прямоугольника
    private double width;
    private double height;

    //Конструктор
    public Rectangle(String colour, double width, double height){
        super(colour);
        this.width = width;
        this.height = height;
    }

    //Переопределяем метод toSrting
    @Override
    public String toString() {
        return super.toString() +
                ", width is " + String.format("%.2f",width) +
                ", height is " + String.format("%.2f",height) +
                ", Area is " + String.format("%.2f",calcArea());
    }

    //Переопределяем метод calcArea
    @Override
    public double calcArea(){
        return width*height;
    }

    @Override
    public void draw(){
        System.out.println(toString());
    }

    @Override
    public int compareTo(Object o){
        Rectangle rectangle = (Rectangle)o;
        if(calcArea()>rectangle.calcArea()) return 1;
        if(calcArea()<rectangle.calcArea()) return -1;
        return 0;
    }
}
