package com.mainacad.module2.labs.Lab2_8;

public class Circle extends Shape implements Comparable {
    //Переменная Радиус
    private double radius;

    //Конструктор
    public Circle(String colour, double radius){
        super(colour);
        this.radius = radius;
    }

    //Переопределяем метод toSrting
    @Override
    public String toString() {
        return super.toString() +
                ", radius is " + String.format("%.2f",radius) +
                ", Area is " + String.format("%.2f",calcArea());
    }

    //Переопределяем метод calcArea
    @Override
    public double calcArea(){
        return Math.PI*Math.pow(radius,2);
    }

    @Override
    public void draw(){
        System.out.println(toString());
    }

    @Override
    public int compareTo(Object o){
        Circle circle = (Circle)o;
        if(calcArea()>circle.calcArea()) return 1;
        if(calcArea()<circle.calcArea()) return -1;
        return 0;
    }
}
