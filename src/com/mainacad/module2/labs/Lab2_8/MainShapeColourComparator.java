package com.mainacad.module2.labs.Lab2_8;

import java.util.Arrays;

public class MainShapeColourComparator {
    public static void main(String[] args) {
        Shape[] shapes = {
                new Rectangle("Red", 10, 20),
                new Circle("Green", 20),
                new Rectangle("Black", 15,25),
                new Triangle("Yellow", 3, 4, 5),
                new Rectangle("Grey", 30,20),
                new Triangle("Pink", 10, 10, 15),
                new Rectangle("Blue", 5,2),
                new Circle("White", 10),
                new Rectangle("Purple", 20,30),
        };
        Arrays.sort(shapes,new ShapeColourComparator());
        for(Shape shape:shapes)
            shape.draw();
    }
}
