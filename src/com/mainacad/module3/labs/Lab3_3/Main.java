package com.mainacad.module3.labs.Lab3_3;

import java.sql.*;

public class Main {
    public static void main(String[] args) {
        final String db_url="jdbc:mysql://localhost:3306/test1";
        final String db_user="root";
        final String db_password="Cod3281291";
        final String db_driver="com.mysql.jdbc.Driver";

        try{
            Class.forName(db_driver);
        }catch (ClassNotFoundException e){
            e.printStackTrace();
        }

        try(Connection conn= DriverManager.getConnection(db_url,db_user,db_password)){
            Statement st=conn.createStatement();
            st.execute("select * from a");
            //st.executeQuery("select * from a");
            ResultSet rs = st.getResultSet();
            while(rs.next()){
                System.out.println(rs.getString(1)+ " " +rs.getString(2));
            }
        }catch (SQLException e){}
    }
}
