package com.mainacad.module3.labs.Lab3_1.Lab3;

import java.io.*;

public class MyFileCopy {
    public static void main(String[] args) {
        //FileInputStream fins = null;
        //FileOutputStream fouts = null;
        File fin = null;
        File fout = null;

        if(args.length>=2){
            fin = new File(args[0]);
            fout = new File(args[1]);
            try(FileInputStream fins = new FileInputStream(fin);FileOutputStream fouts = new FileOutputStream(fout)){
                int item;
                while((item = fins.read())!=-1){
                    fouts.write(item);
                }
            }catch(FileNotFoundException e){

            }catch(IOException e){

            }

        }
        else{
            System.out.println("Нeверные имена входного и целевого файлов");
        }
    }
}
