package com.mainacad.module3.labs.Lab3_1.Lab4;

import java.io.*;
import java.util.Scanner;

public class ReplacementInFile {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("введите путь к файлу");
        String fileName = scanner.nextLine();

        File fl = new File(fileName);
        File flTemp = new File("C:\\Temp\\Java\\LadWorks\\my-labs\\Files\\temp.tmp");
        try(BufferedReader br = new BufferedReader(new FileReader(fl));BufferedWriter bw = new BufferedWriter(new FileWriter(flTemp))){
            String lineStr = null;
            while((lineStr = br.readLine())!=null){
                lineStr = lineStr.replaceAll("public","private");
                bw.write(lineStr);
                bw.write("\r\n");
            }
            bw.flush();
        }catch(FileNotFoundException e){
            System.out.println("Файл не найден");
        }catch(IOException e){
            System.out.println("Проблемы взаимодействия с файлом");
        }
        fl.delete();
        flTemp.renameTo(fl);
    }
}
