package com.mainacad.module3.labs.Lab3_1.Lab5;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Scanner;

public class AccountingUser {
    private static RandomAccessFile raf=null;

    public static void main(String[] args) {
        String userName;
        Scanner scanner = new Scanner(System.in);

        try(RandomAccessFile raff = new RandomAccessFile("Files/users.txt","rw")){
            raf=raff;
            System.out.println("Enter user's names");
            while(true) {
                userName=scanner.nextLine();
                if(userName.equalsIgnoreCase("q"))
                    break;
                testUser(userName);
            }

            printFile(raf);


        }catch(FileNotFoundException e){

        }catch(IOException e){

        }

    }

    public static void testUser(String userName)throws IOException{
        long pointer=0;
        String str=null;
        raf.seek(0);
        while((str=raf.readLine())!=null){
            String[] strs=str.split(":");
            if(strs[0].equalsIgnoreCase(userName)){
                raf.seek(pointer);
                raf.writeBytes(userName);
                raf.writeBytes(":");
                raf.writeBytes(String.valueOf(Integer.parseInt(strs[1])+1));
                raf.writeBytes("\r\n");
                return;
            }
            pointer+=(str.length()+2);
        }


        raf.seek(raf.length());
        raf.writeBytes(userName);
        raf.writeBytes(":");
        raf.writeBytes(String.valueOf(1));
        raf.writeBytes("\r\n");
    }

    public static void printFile(RandomAccessFile raf) throws IOException{
        String str=null;
        raf.seek(0);
        while(true){
            if((str=raf.readLine())!=null){
                System.out.println(str);
            }
            else break;
        }
    }
}
