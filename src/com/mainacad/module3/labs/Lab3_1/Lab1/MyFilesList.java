package com.mainacad.module3.labs.Lab3_1.Lab1;

import java.io.File;

public class MyFilesList {
    public static void main(String[] args) {
        File directory = null;
        if(args.length==0){
            directory = new File(".");
        }
        else{
            directory = new File(args[0]);
        }
        if(directory.isDirectory()){
            File[] listFiles = directory.listFiles();
            for(File fl:listFiles)
                System.out.println(fl);
        }
        else{
            System.out.println("It's not directory");
        }
    }
}
