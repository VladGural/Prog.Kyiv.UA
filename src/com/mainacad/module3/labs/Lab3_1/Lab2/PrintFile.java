package com.mainacad.module3.labs.Lab3_1.Lab2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class PrintFile {
    public static void main(String[] args) {
        File fl = null;
        if(args.length!=0){
            fl = new File(args[0]);
            if(fl.isFile()){
                try(BufferedReader br = new BufferedReader(new FileReader(fl))){
                    String str;
                    while((str = br.readLine())!=null){
                        System.out.println(str);
                    }
                }catch(IOException e){
                    System.out.println("Error reading file");
                }
            }
            else{
                System.out.println("it's not file");
            }
        }
        else{
            System.out.println("File's name isn't specified");
        }
    }
}
