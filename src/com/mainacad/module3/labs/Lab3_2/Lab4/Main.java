package com.mainacad.module3.labs.Lab3_2.Lab4;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class Main {
    public static void main(String[] args) {
        User[] users = new User[2];
        users[0] = new User("Vladyslav","Gural",45);
        users[1] = new User("Dmitriy","Tkachenco",40);
        for(User user:users)
            System.out.println(user);

        //Serialize
        try(RandomAccessFile raf = new RandomAccessFile("Files/users.ser","rw")){
            raf.seek(raf.length());
            for(User user:users)
                raf.writeBytes(user.toString());
        }catch(FileNotFoundException e){

        }catch(IOException e){

        }

        //Deserialize
        String str;
        User[] usersnew = new User[2];

        try(RandomAccessFile rafr = new RandomAccessFile("Files/users.ser","r")){

            for(int i=0;(str = rafr.readLine())!=null;i++){
                String[] strings = str.split(",");
                User usernew = new User();
                usernew.setFirstname(strings[0]);
                usernew.setLastName(strings[1]);
                usernew.setAge(Integer.parseInt(strings[2]));
                usersnew[i]=usernew;
            }

            for(User user:usersnew)
                System.out.println(user);
        }catch(FileNotFoundException e){

        }catch(IOException e){

        }
    }
}
