package com.mainacad.module3.labs.Lab3_2.Lab4;

import java.io.Serializable;

public class User {
    private String firstName;
    private String lastName;
    private int age;

    public User(){

    }

    public User(String firstName,String lastName,int age){
        this.firstName=firstName;
        this.lastName=lastName;
        this.age=age;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstname(String firstname) {
        this.firstName = firstname;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return firstName + "," + lastName + "," + age + "\r\n";
    }
}
