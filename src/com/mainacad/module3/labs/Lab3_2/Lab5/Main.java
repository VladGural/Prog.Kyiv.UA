package com.mainacad.module3.labs.Lab3_2.Lab5;

import java.io.*;

public class Main {
    public static void main(String[] args) {
        User[] users = new User[2];
        users[0] = new User("Vladyslav","Gural",45);
        users[1] = new User("Dmitriy","Tkachenco",40);
        for(User user:users)
            System.out.println(user);

        //Serialize
        try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("Files/users_lab5.ser"))){
            for(User user:users)
                user.writeExternal(oos);

        }catch(FileNotFoundException e){

        }catch(IOException e){

        }

        //Deserialize
        User[] usersnew = new User[2];

        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream("Files/users_lab5.ser"))){
            User user;
            for(int i=0;i<usersnew.length;i++) {
                user = new User();
                user.readExternal(ois);
                usersnew[i]=user;
            }
            for(User usernew:usersnew)
                System.out.println(usernew);

        }catch(FileNotFoundException e){

        }catch(IOException e){

        }catch (ClassNotFoundException e){

        }
    }
}
