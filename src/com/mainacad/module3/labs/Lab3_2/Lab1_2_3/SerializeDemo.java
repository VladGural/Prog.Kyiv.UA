package com.mainacad.module3.labs.Lab3_2.Lab1_2_3;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class SerializeDemo {
    public static void main(String[] args) {
        Employee emp = new Employee();
        emp.setName("Vladyslav");
        emp.setAddress("12 Lubova str.");
        emp.setSSN(1234567890);
        emp.setNumber(50_234_34_45);
        System.out.println(emp);

        try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("Files/lab1_2_3.ser"))){
            oos.writeObject(emp);
        }catch(IOException e){}
    }
}
