package com.mainacad.module3.labs.Lab3_2.Lab1_2_3;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class DeserializeDemo {
    public static void main(String[] args) {
        Employee emp;
        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream("Files/lab1_2_3.ser"))){
            emp=(Employee)ois.readObject();
            System.out.println(emp);
        }catch(IOException e){

        }catch(ClassNotFoundException e){

        }
    }
}
