package com.mainacad.module3.labs.Lab3_8.Lab1_2;

import java.io.Serializable;
import java.util.Objects;

public class Student implements Serializable{
    private int id;
    private String name;
    private String course;
    private static int NEXTID=1;

    public Student(String name,String course){
        this.name=name;
        this.course=course;
        id=NEXTID++;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", course='" + course + '\'' +
                ", id=" + id +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student)) return false;
        Student student = (Student) o;
        return Objects.equals(getName(), student.getName());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getName());
    }
}
