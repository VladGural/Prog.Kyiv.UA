package com.mainacad.module3.labs.Lab3_8.Lab1_2;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server implements Runnable{
    private List<Student> students = new ArrayList<>();

    private void init(){
        students.add(new Student("Vlad","Java"));
        students.add(new Student("Dmitriy","C++"));
        students.add(new Student("Nika","Frontend"));
        students.add(new Student("Keril","Java"));
        students.add(new Student("Yulya","C#"));
    }

//    //Lab1_8_2_1
//    @Override
//    public void run() {
//        try(ServerSocket listener = new ServerSocket(8899)){
//            System.out.println("Server wait...");
//            Socket socket = listener.accept();
//            System.out.println("Server connected");
//            ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
//            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
//
//            Student std = (Student) ois.readObject();
//            System.out.println(std);
//
//            oos.writeObject(std.getName() + " received");
//            oos.flush();
//            oos.close();
//            ois.close();
//            System.out.println("Server disconnected");
//
//
//        }catch(IOException e){
//
//        }catch(ClassNotFoundException e){
//
//        }
//    }


    @Override
    public void run() {
        try(ServerSocket listener = new ServerSocket(8899)){
            System.out.println("Server waits...");
            init();

            while(true){
                Socket socket = listener.accept();
                System.out.println("Cerver connected");
                new Thread(new ThreadClient(socket,students)).start();
            }

        }catch(IOException e){

        }
    }
}
