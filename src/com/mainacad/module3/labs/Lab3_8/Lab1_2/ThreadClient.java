package com.mainacad.module3.labs.Lab3_8.Lab1_2;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;

public class ThreadClient implements Runnable{
    private Socket socket;
    private List<Student> users;

    public ThreadClient(Socket socket,List<Student> users){
        this.socket=socket;
        this.users=users;
    }

    @Override
    public void run() {
        try(ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
            ObjectOutputStream oos = new ObjectOutputStream((socket.getOutputStream()))){

            Student std = (Student)ois.readObject();
            if(users.contains(std)){
                oos.writeObject("Student " + std.getName() + " access opend");
            }else
                oos.writeObject("Student " + std.getName() + " access denaded");

        }catch(IOException e){

        }catch(ClassNotFoundException e){

        }
    }
}
