package com.mainacad.module3.labs.Lab3_8.Lab1_2;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Scanner;

public class Client implements Runnable{
    private Student std;

    public Client(Student std){
        this.std=std;
    }

    @Override
    public void run() {
        try(Socket socket = new Socket("localhost",8899)){

            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
            oos.writeObject(std);
            oos.flush();
            System.out.println("Server responce: " + ois.readObject());

            oos.close();
            ois.close();


        }catch(IOException e){

        }catch (ClassNotFoundException e){

        }
    }
}
