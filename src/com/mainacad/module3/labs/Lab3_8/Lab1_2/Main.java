package com.mainacad.module3.labs.Lab3_8.Lab1_2;

public class Main {
    public static void main(String[] args) {

//        Lab1
//        Student std = new Student("Vasya","Java");
//
//        new Thread(new Server()).start();
//        new Thread(new Client(std)).start();

        //Lab2
         new Thread(new Server()).start();
         new Thread(new Client(new Student("Vlad","Java"))).start();
         new Thread(new Client(new Student("Gosha","Java"))).start();
         new Thread(new Client(new Student("Dmitriy","Java"))).start();
         new Thread(new Client(new Student("Nika","Java"))).start();
         new Thread(new Client(new Student("Evgen","Java"))).start();
    }
}
