package com.mainacad.module3.labs.Lab3_7.Lab4;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        List<Integer> listInteger = Stream.iterate(10,n -> n+10)
                                            .limit(10)
                                            .map(n -> n/2)
                                            .collect(Collectors.toList());
        System.out.println("List of Integer");
        System.out.println(listInteger);
        System.out.println();

        Stream.of("Anna","Dmitriy","Jorje","Vova","Katya",
                  "Ignat","Nikolay","Yura","Lisa","Natalya")
                .filter(str -> str.charAt(0)=='N')
                .sorted()
                .forEach(System.out::println);
        System.out.println();


        List<Person> people = Arrays.asList(new Person("Ivan",20,Sex.MAN),new Person("Vlada",45,Sex.WOMAN),
                                            new Person("Lida",30,Sex.WOMAN),new Person("Dima",26,Sex.MAN));
        people.stream()
                .filter(person -> (person.getAge()>=18)&&(person.getAge()<=27)&&(person.getGender()==Sex.MAN))
                .forEach(System.out::println);
        System.out.println();

        double avarage = people.stream()
                .filter(person -> person.getGender()==Sex.WOMAN)
                .mapToInt(p -> p.getAge())
                .average()
                .getAsDouble();
        System.out.println("Avarage of woman age is: " + avarage);
    }
}
