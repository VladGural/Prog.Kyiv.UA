package com.mainacad.module3.labs.Lab3_7.Lab1_3;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;

public class Main {
    public static void main(String[] args) {
        Integer[] intArray = createArray(10);
        System.out.println("Start of array");
        System.out.println(Arrays.toString(intArray));
        System.out.println();

        Arrays.sort(intArray,(x,y)->(x.compareTo(y))*-1);
        System.out.println("After sort");
        System.out.println(Arrays.toString(intArray));
        System.out.println();

        System.out.println("Sum of even numbers of array is");
        System.out.println(sumEven(intArray,(x) -> x%2 == 0));

        List<String> string = Arrays.asList("Boris","Jhonn","Yana","Vlad","Jessy","Dmitriy");
        System.out.println("Start of List");
        System.out.println(string);
        System.out.println();

        Collections.sort(string,(x, y) -> (y.compareToIgnoreCase(x)));
        System.out.println("List after sort");
        System.out.println(string);
        System.out.println();

        System.out.println("Strings that begin with \"J\" letter is");
        printJStr(string,(x) -> (x.charAt(0)=='J'));
        System.out.println();

        System.out.println("Use method updateList");
        updateList(string,(str) -> str.toUpperCase());
        System.out.println(string);
        System.out.println();

    }

    public static Integer[] createArray(int numberOfArray){
        Integer[] intArray = new Integer[numberOfArray];
        for(int i =0;i<numberOfArray;i++){
            intArray[i]=(int) (Math.random()*100);
        }
        return intArray;
    }

    public static int sumEven(Integer[] intArray, Predicate<Integer> predicate){
        int sum=0;
        for(Integer integer:intArray){
            if(predicate.test(integer))
                sum+=integer;
        }
        return sum;
    }

    public static void printJStr(List<String> list,Predicate<String> predicate){
        for(String str:list){
            if(predicate.test(str))
                System.out.print(str + " ");
        }
        System.out.println();
    }

    public static void updateList(List<String> list, MyConverer myConvert){
        for(int i = 0;i<list.size();i++){
            list.set(i,myConvert.convertStr(list.get(i)));
        }
    }
}

interface MyConverer{
    public String convertStr(String str);

    public static boolean isNull(String str){
        return str == null;
    }
}
