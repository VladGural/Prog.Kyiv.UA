package com.mainacad.module3.labs.Lab3_7.Lab5;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class Main {
    public static void main(String[] args) {
        LocalDate today = LocalDate.now();
        System.out.println("Today is " + today);
        LocalDate myBirthday = LocalDate.of(1973,6,22);
        System.out.println("My birthday is " + myBirthday);
        Period myAge = myBirthday.until(today);
        System.out.println("My age is " + myAge.getYears() + " years " + myAge.getMonths() + " month and "
                + myAge.getDays() + " days");

        System.out.println("I was born on " + myBirthday.getDayOfWeek());
        System.out.println("In this year my birthday will be on " + myBirthday.plusYears(45).getDayOfWeek());

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy, HH-mm-ss");
        System.out.println("Special format of Date-Time is " + LocalDateTime.now().format(formatter));
        LocalDateTime dateTime = LocalDateTime.ofInstant(Instant.now(), ZoneId.of("Europe/Kiev"));
        System.out.println("Local date and time is " + ZonedDateTime.of(dateTime,ZoneId.of("Europe/Kiev")));
    }
}
