package com.mainacad.module3.labs.Lab3_5.Lab1;

import java.awt.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class Main {
    public static void main(String[] args) {
        MyClass myclass = new MyClass();
        Class<?> infoMyClass = myclass.getClass();
        System.out.println("Name of MyClass");
        System.out.println(infoMyClass.getName());
        System.out.println();

        int modifier = infoMyClass.getModifiers();
        System.out.println("Modifier of MyClass");
        printModifierClass(modifier);
        System.out.println();

        Field[] fields = infoMyClass.getFields();
        System.out.println("Only Public fields of MyClass");
        printFields(fields);
        System.out.println();

        Constructor[] constructors = infoMyClass.getConstructors();
        System.out.println("Constructor of MyClass");
        printConstrucpors(constructors);
        System.out.println();

        Method[] methods = infoMyClass.getMethods();
        System.out.println("Methods of MyClass");
        printMethods(methods);
        System.out.println();
    }

    private static void printModifierClass(int modifier){
        if(Modifier.isAbstract(modifier)) System.out.print("Abstract ");
        if(Modifier.isFinal(modifier)) System.out.print("Final ");
        if(Modifier.isPublic(modifier)) System.out.println("Public ");
    }

    private static void printFields(Field[] fields){
        for(Field field:fields){
            System.out.println("Name " + field.getName());
            System.out.println(("Type " + field.getType()));
        }
    }

    private static void printConstrucpors(Constructor[] constructors){
        int i = 1;
        for(Constructor constructor:constructors){
            System.out.print("Constructors[" + i++ + "]: ");
            Class<?>[] typeParameters = constructor.getParameterTypes();
            for(Class<?> typeParaneter:typeParameters){
                System.out.print("Name of parameter " + typeParaneter + " ");
            }
            System.out.println();
        }
    }

    private static void printMethods(Method[] methods){
        for(Method method:methods){
            System.out.print("Method " + method.getName() + " ");
            Class<?>[] typeParameters = method.getParameterTypes();
            for(Class<?> typeParameter:typeParameters)
                System.out.print("Name of parameter " + typeParameter + " ");
            System.out.println();
        }
    }
}
