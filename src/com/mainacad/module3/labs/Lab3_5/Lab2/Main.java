package com.mainacad.module3.labs.Lab3_5.Lab2;

import java.lang.reflect.Field;

public class Main {
    public static void main(String[] args)
    throws NoSuchFieldException, IllegalAccessException
    {
        String myStr = "abcd";
        Test test = new Test(myStr);
        Class cl = test.getClass();
        Field f = cl.getDeclaredField("str");
        f.setAccessible(true);// Разрешаем доступ к private полю
        System.out.println("Old value private field " + f.get(test));

        f.set(test,"zxcv");

        System.out.println("New value private field " + f.get(test));
    }
}

class Test{
    private String str;

    public Test(String str){
        this.str = str;
    }
}
