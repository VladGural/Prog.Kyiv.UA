package com.mainacad.module3.labs.Lab3_5.Lab3;

public final class MyClass {
    private int a = 100;
    protected int b = 200;
    public int c = 300;
    final static int d =400;

    public MyClass(){

    }

    public MyClass(int a){
        this.a = a;
    }

    public MyClass(int a, int b){
        this.a = a;
        this.b = b;
    }

    public MyClass(int a, int b, int c){
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public int getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
    }

    public static int getD() {
        return d;
    }
}
