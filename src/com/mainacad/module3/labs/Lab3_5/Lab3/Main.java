package com.mainacad.module3.labs.Lab3_5.Lab3;

import java.awt.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {
    public static void main(String[] args)
    throws IllegalAccessException, NoSuchMethodException,InvocationTargetException, InstantiationException
    {
        Constructor con = MyClass.class.getConstructor(int.class);

        MyClass myClass = (MyClass) con.newInstance(1000);

        Field[] fields = MyClass.class.getDeclaredFields();
        printFields(fields,myClass);

        Method method = MyClass.class.getMethod("setA", int.class);
        method.invoke(myClass,33);//Вызываем метод setA(33)

        printFields(fields,myClass);
    }

    public static void printFields(Field[] fields,Object obj)
    throws IllegalAccessException
    {
        for(Field field:fields){
            field.setAccessible(true);
            System.out.println("Field name: " + field.getName());
            System.out.println("Field type: " + field.getType());
            System.out.println("Field value: " + field.get(obj));
            System.out.println();
        }
    }
}
