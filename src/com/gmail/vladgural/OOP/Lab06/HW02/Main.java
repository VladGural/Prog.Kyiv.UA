package com.gmail.vladgural.OOP.Lab06.HW02;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        SumArray sa;
        SumArray sa1;
        SumArray sa2;
        SumArray sa3;
        SumArray sa4;
        Timer tm = new Timer();


        int sumArray=0;

        int[] array = new int[80000000];
        int[] array1;
        int[] array2;
        int[] array3;
        int[] array4;

        for(int i = 0; i<array.length; i++){
            array[i] = (int)(Math.random()*10);
        }

        array1 = Arrays.copyOfRange(array,0,20000000);
        array2 = Arrays.copyOfRange(array,20000000,40000000);
        array3 = Arrays.copyOfRange(array,40000000,60000000);
        array4 = Arrays.copyOfRange(array,60000000,80000000);

        sa = new SumArray(array);
        sa1 = new SumArray(array1);
        sa2 = new SumArray(array2);
        sa3 = new SumArray(array3);
        sa4 = new SumArray(array4);

        Thread tr = new Thread(sa);
        Thread tr1 = new Thread(sa1);
        Thread tr2 = new Thread(sa2);
        Thread tr3 = new Thread(sa3);
        Thread tr4 = new Thread(sa4);

        System.out.println("Start full");
        tm.start();

        tr.start();
        try {
            tr.join();
        }catch (InterruptedException e){
        }

        try {
            sumArray = sa.getSum();
        }catch (IncorrectSumException e){
            System.out.println("Incorrect sum of Array");
        }

        tm.stop();

        System.out.println("Sum of Array is " + sumArray + " and time is " + tm.getTime() + " mls");

        sumArray = 0;


        System.out.println("Start part");
        tm.start();

        tr1.start();
        tr2.start();
        tr3.start();
        tr4.start();

        try {
            tr1.join();
            tr2.join();
            tr3.join();
            tr4.join();
        }catch (InterruptedException e){
        }


        try {
            sumArray += sa1.getSum();
            sumArray += sa2.getSum();
            sumArray += sa3.getSum();
            sumArray += sa4.getSum();

        }catch (IncorrectSumException e){
            System.out.println("Incorrect sum of Array");
        }

        tm.stop();
        System.out.println("Sum of Array is " + sumArray + " and time is " + tm.getTime() + " mls");

        int processors = Runtime.getRuntime().availableProcessors();

        System.out.println("Processors " + processors);
    }
}
