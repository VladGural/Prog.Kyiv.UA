package com.gmail.vladgural.OOP.Lab06.HW02;

public class Timer {
    private long Time = 0;
    private long t1 = 0;
    private long t2 = 0;

    public Timer() {
    }

    public void start(){
        t1 = System.currentTimeMillis();
    }

    public void stop(){
        t2 = System.currentTimeMillis();
        Time = t2 - t1;
    }

    public long getTime(){
        return Time;
    }

}
