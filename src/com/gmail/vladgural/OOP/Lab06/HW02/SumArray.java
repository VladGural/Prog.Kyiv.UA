package com.gmail.vladgural.OOP.Lab06.HW02;

public class SumArray implements Runnable{
    private int[] array;
    private boolean correctSum;
    int sum;

    public SumArray() {
    }

    public SumArray(int[] array){
        this.array = array;
        correctSum = false;
        sum = 0;
    }

    public void setArray(int[] array){
        this.array = array;
        correctSum = false;
        sum = 0;
    }

    public int getSum() throws IncorrectSumException{

            if (correctSum ==false) {
                throw new IncorrectSumException();
            }

        return sum;
    }

    public void sum(){
        if(correctSum == true){
            return;
        }
        for(int i = 0; i<array.length;i++){
            sum+=array[i];
        }
        correctSum = true;
    }

    @Override
    public void run() {
        sum();
    }
}
