package com.gmail.vladgural.OOP.Lab06.CW01;

public class Main {
    public static void main(String[] args) {
        FactorialTask task1 = new FactorialTask(10000);
        FactorialTask task2 = new FactorialTask(10000);
        FactorialTask task3 = new FactorialTask(10000);

        Thread th1 =new Thread(task1);
        Thread th2 =new Thread(task2);
        Thread th3 =new Thread(task3);

        th1.start();
        th2.start();
        th3.start();

        th1.interrupt();
        th2.interrupt();
        th3.interrupt();

        try{
            th1.join();//Main ждет пока не отработает th1
            th2.join();
            th3.join();

        }catch (InterruptedException e){

        }

        Thread th = Thread.currentThread();
        System.out.println(th.getName()+" stop");
    }
}
