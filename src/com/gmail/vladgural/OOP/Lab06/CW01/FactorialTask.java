package com.gmail.vladgural.OOP.Lab06.CW01;

import com.mainacad.module3.labs.Lab3_8.Lab1_2.ThreadClient;

import java.math.BigInteger;

public class FactorialTask implements Runnable{
    private int numberThread;

    public FactorialTask(int numberThread) {
        this.numberThread = numberThread;
    }

    public FactorialTask() {
    }

    public int getNumberThread() {
        return numberThread;
    }

    public void setNumberThread(int numberThread) {
        this.numberThread = numberThread;
    }

    public BigInteger calculateFactorian(int n){
        BigInteger factorial = new BigInteger("1");

        for (int i=1;i<=n;i++){
            factorial = factorial.multiply(new BigInteger(""+i));
        }
        return factorial;
    }

    @Override
    public void run() {
        Thread th = Thread.currentThread();
        for(int i=1;i<=numberThread;i++){
            System.out.println(th.getName()+" -> "+i+"! = "+calculateFactorian(i));

            if(th.isInterrupted()){
                System.out.println(th.getName()+" interrupted");
                return;
            }
        }
    }
}
