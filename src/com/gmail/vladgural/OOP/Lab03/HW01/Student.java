package com.gmail.vladgural.OOP.Lab03.HW01;

public class Student extends Person implements Comparable<Student> {
    private String nameOfGroup;

    public Student(Person person, String nameOfGroup) {
        super.setFirstName(person.getFirstName());
        super.setLastName(person.getLastName());
        super.setAge(person.getAge());
        this.nameOfGroup = nameOfGroup;
    }

    public Student() {
    }

    public String getNameOfGroup() {
        return nameOfGroup;
    }

    public void setNameOfGroup(String nameOfGroup) {
        this.nameOfGroup = nameOfGroup;
    }

    @Override
    public int compareTo(Student student){
        return this.getLastName().compareTo(student.getLastName());
    }

    @Override
    public String toString() {
        return " " +
                super.toString() +
                " nameOfGroup='" + nameOfGroup + '\'';
    }
}
