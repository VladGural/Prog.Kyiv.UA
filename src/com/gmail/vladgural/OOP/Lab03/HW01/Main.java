package com.gmail.vladgural.OOP.Lab03.HW01;

public class Main {
    public static void main(String[] args) {
        Person pr01 = new Person("Vlad", "Gural", 45);
        Person pr02 = new Person("Igor", "Makarenko", 45);
        Person pr03 = new Person("Vladimir","Bormotov",45);
        Person pr04 = new Person("Vladimir","Generalov",44);
        Person pr05 = new Person("Natalya","Litvinova",45);
        Person pr06 = new Person("Mahmud","Alibabaevich", 18);
        Person pr07 = new Person("Vladimir","Abramov",45);
        Person pr08 = new Person("Andrey","Pakormlyak",46);
        Person pr09 = new Person("Phelics","Tabakin", 45);
        Person pr10 = new Person("Oleg","Komov",45);
        Person pr11 = new Person("Alyena","Povaliy",45);

        Group group = new Group("АСУ-1990-А");
        System.out.println("Print empty Group");
        System.out.println(group.toString());


        group.insertStudent(pr01);
        group.insertStudent(pr02);
        group.insertStudent(pr01); //Этот студент не добавится, такая фамилия уже есть
        System.out.println("Try insert same (last name Gural) student in this Group but insert only one");
        System.out.println(group.toString());


        group.insertStudent(pr03);
        group.insertStudent(pr04);
        group.insertStudent(pr05);
        group.insertStudent(pr06);
        group.insertStudent(pr07);
        group.insertStudent(pr08);
        group.insertStudent(pr09);
        group.insertStudent(pr10);
        System.out.println("Print Group of ten students");
        System.out.println(group.toString());

        System.out.println("Try insert eleventh student");
        group.insertStudent(pr11); // Попробуем добавить 11 студента
        System.out.println(group.toString());

        group.deleteOfStudent("Alibabaevich"); //Удалим 6 студента
        System.out.println("Delete students with lastname Alibabaevich");
        System.out.println(group.toString());

        group.insertStudent(pr11); // И теперь добавим 11 студента
        System.out.println("Insert in Group student with last name Povaliy");
        System.out.println(group.toString());
    }
}
