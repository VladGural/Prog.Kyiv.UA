package com.gmail.vladgural.OOP.Lab03.HW01;

import java.util.Arrays;

public class Group {
    private Student[] students = new Student[10];
    private int numberOfStudents;
    private String nameOfGroup;

    public Group(String nameOfGroup) {
        this.nameOfGroup = nameOfGroup;
    }

    public Group() {
    }

    public Student[] getStudents() {
        return students;
    }

    public int getNumberOfStudents() {
        return numberOfStudents;
    }

    public String getNameOfGroup() {
        return nameOfGroup;
    }

    public void insertStudent(Person person){
        try {
            if(numberOfStudents==10){
                throw new GroupIsFullException();
            }
            for(int i = 0; i<numberOfStudents; i++){  // Нельзя вставить студента с такойже Фанилией
                if(students[i].getLastName().equals(person.getLastName())){
                    return;
                }
            }
            students[numberOfStudents] = new Student(person, nameOfGroup);
            numberOfStudents++;
        }catch(GroupIsFullException e){
            System.out.println("We cannot insert new student because group is full \n");
        }
    }

    public void deleteOfStudent(String lastName){
        int number = -1;
        for(int i=0; i<numberOfStudents; i++){
            if(students[i].getLastName().equals(lastName)){
                number = i;
                break;
            }
        }
        if(number!=-1){
            if(number == 0 && numberOfStudents == 1){
                students[number] = null;
                numberOfStudents--;
            }else if(number == 9){
                students[number] = null;
                numberOfStudents--;
            }else {
                for (int i = number; i < (numberOfStudents - 1); i++) {
                    students[i] = students[i + 1];
                }
                students[numberOfStudents - 1] = null;
                numberOfStudents--;
            }
        }
    }

    @Override
    public String toString() {
        String str;

        if(numberOfStudents==0){
            str = "Group " + nameOfGroup + " is enpty \n";
        }else {
            Student[] sortst = new Student[numberOfStudents];
            for(int i = 0; i<numberOfStudents; i++){
                sortst[i]=students[i];
            }

            Arrays.sort(sortst);

            str = "Students of group " + nameOfGroup + " are \n";
            if (numberOfStudents != 0) {
                for (int i = 0; i < numberOfStudents; i++) {
                    str += sortst[i].toString();
                    str += "\n";
                }
            }
            str += "and total number of students is " + numberOfStudents + "\n";
        }
        return str;
    }
}
