package com.gmail.vladgural.OOP.Lab03.CW02;

import java.io.File;
import java.lang.reflect.Modifier;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        File file = new File("a.txt");
        Class <?> fileClass = file.getClass();
        System.out.println(fileClass.getName());
        Class<?> superFileClass = fileClass.getSuperclass();
        System.out.println(superFileClass.getName());
        Class <?>[] implementsInterface = fileClass.getInterfaces();
        for (Class <?> inter : implementsInterface) {
            System.out.println(inter.toString());
        }
        int mod = fileClass.getModifiers();
        System.out.println(Integer.toBinaryString(mod));
        System.out.println("Public class " + Modifier.isPublic(mod));
        System.out.println("Private class " + Modifier.isPrivate(mod));
        System.out.println("Abstarct class " + Modifier.isAbstract(mod));
    }
}
