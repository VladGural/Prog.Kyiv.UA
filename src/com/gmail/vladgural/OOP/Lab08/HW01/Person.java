package com.gmail.vladgural.OOP.Lab08.HW01;

import java.io.Serializable;

import static java.lang.String.format;

public class Person implements Serializable{
    private String firstName;
    private String lastName;
    private int age;
    private Sex sex;

    public Person(String firstName, String lastName, int age, Sex sex) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.sex = sex;
    }

    public Person() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    @Override
    public String toString() {
        return "firstName='" + format("%14s",firstName) + '\'' +
                ", lastName='" + format("%14s",lastName) + '\'' +
                ", age=" + format("%3d",age) +
                ", Sex='" + format("%7s",sex.toString()) + '\'';
    }
}
