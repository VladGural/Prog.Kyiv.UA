package com.gmail.vladgural.OOP.Lab08.HW01;

import java.io.*;

public class SerializableFileWork {
    public static void saveObjeckToFile(Object obj, File file) throws IOException{
        if(obj == null){
            throw new IllegalArgumentException();
        }
        try (ObjectOutput oos = new ObjectOutputStream(new FileOutputStream(file))){
            oos.writeObject(obj);
        }catch (IOException e){
            throw e;
        }
    }

    public static Object loadObjackFromFile(File file) throws IOException, ClassNotFoundException {
        try(ObjectInput ois = new ObjectInputStream(new FileInputStream(file))){
            return ois.readObject();
        }catch (IOException e){
            throw e;
        }
    }

}
