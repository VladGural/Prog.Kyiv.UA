package com.gmail.vladgural.OOP.Lab08.HW02;

public class MyStack {
    private int size;
    private int point;
    private Object[] stack;
    BlackList blackList;

    public MyStack() {
        size = 10;
        point = 0;
        stack = new Object[size];
        blackList = new BlackList();
    }

    public MyStack(int size) {
        this.size = size;
        point = 0;
        stack = new Object[size];
        blackList = new BlackList();
    }

    public int getSize() {
        return size;
    }

    public void push(Object obj)throws StackIsFullException{
        if(blackList.checkList(obj.getClass().getName())){
            return;
        }


        if(point!=(size)){
            stack[point] = obj;
            point++;
        }else{
            throw new StackIsFullException();
        }
    }

    public Object peek()throws StackIsEmptyException{
        if(point!=0){
            return stack[point-1];
        }else {
            throw new StackIsEmptyException();
        }
    }

    public Object poll()throws StackIsEmptyException{
        if(point!=0){
            Object temp;
            temp = stack[point-1];
            stack[point-1] = null;
            point--;
            return temp;
        }else {
            throw new StackIsEmptyException();
        }
    }
}
