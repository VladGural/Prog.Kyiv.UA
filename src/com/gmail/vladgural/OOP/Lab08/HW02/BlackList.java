package com.gmail.vladgural.OOP.Lab08.HW02;

import java.util.ArrayList;
import java.util.List;

public class BlackList {
    List<String> blackList = new ArrayList<>();

    public BlackList() {
    }

    public void addClass(String addClass){
        boolean exist = false;
        for(int i = 0; i<blackList.size();i++){
            if(blackList.get(i).equals(addClass)){
                exist = true;
                break;
            }
        }
        if(!exist){
            blackList.add(addClass);
        }
    }

    public boolean checkList(String existInList){
        boolean exist = false;
        for(int i = 0; i<blackList.size();i++){
            if(blackList.get(i).equals(existInList)){
                exist = true;
                break;
            }
        }
        return exist;
    }
}
