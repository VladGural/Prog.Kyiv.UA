package com.gmail.vladgural.OOP.Lab08.HW02;

public class Main {
    public static void main(String[] args) {
        MyStack myStack = new MyStack(3);

        System.out.println("Cannot push Cat in sink!!!\n");
        myStack.blackList.addClass(Cat.class.getName());

        Plate plate = new Plate("Green");
        Bowl bowl = new Bowl("Aluminium");
        Pan pan = new Pan(10);
        Cat cat = new Cat("Mura");

        System.out.println(plate);
        System.out.println("I am pushed in sink. Can me?");
        try {
            myStack.push(plate);
        } catch (StackIsFullException e) {
            System.out.println("Stack is full");
        }

        System.out.println(bowl);
        System.out.println("I am pushed in sink. Can me?");
        try {
            myStack.push(bowl);
        } catch (StackIsFullException e) {
            System.out.println("Stack is full");
        }

        System.out.println(cat);
        System.out.println("I am pushed in sink. Can me?");
        try {
            myStack.push(cat);
        } catch (StackIsFullException e) {
            System.out.println("Stack is full");
        }

        System.out.println(pan);
        System.out.println("I am pushed in sink. Can me?");
        try {
            myStack.push(pan);
        } catch (StackIsFullException e) {
            System.out.println("Stack is full");
        }
        System.out.println();

        System.out.println("Who first take out from sink?");
        try{
            System.out.println(myStack.poll());
        }catch (StackIsEmptyException e){
            System.out.println("Stack is empty.");
        }

        System.out.println("Who second take out from sink?");
        try{
            System.out.println(myStack.poll());
        }catch (StackIsEmptyException e){
            System.out.println("Stack is empty.");
        }

        System.out.println("Who third take out from sink?");
        try{
            System.out.println(myStack.poll());
        }catch (StackIsEmptyException e){
            System.out.println("Stack is empty.");
        }
    }
}
