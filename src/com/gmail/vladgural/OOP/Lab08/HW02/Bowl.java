package com.gmail.vladgural.OOP.Lab08.HW02;

public class Bowl {
    private String material;

    public Bowl(String material) {
        this.material = material;
    }

    public Bowl() {
    }

    @Override
    public String toString() {
        return "I am Bowl. And I am made from " + material;
    }
}
