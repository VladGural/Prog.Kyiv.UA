package com.gmail.vladgural.OOP.Lab08.HW02;

public class Plate {
    private String colour;

    public Plate(String colour) {
        this.colour = colour;
    }

    public Plate() {
    }

    @Override
    public String toString() {
        return "I am Plate. And I am " + colour;
    }
}
