package com.gmail.vladgural.OOP.Lab08.HW02;

public class Pan {
    private double weight;

    public Pan(double weght) {
        this.weight = weght;
    }

    public Pan() {
    }

    @Override
    public String toString() {
        return "I am Pan. And my weight is " + weight;
    }
}
