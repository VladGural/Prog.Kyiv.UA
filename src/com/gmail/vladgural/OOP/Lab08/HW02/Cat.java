package com.gmail.vladgural.OOP.Lab08.HW02;

public class Cat {
    private String name;

    public Cat(String name) {
        this.name = name;
    }

    public Cat() {
    }

    @Override
    public String toString() {
        return "I am Cat. And my name is " + name;
    }
}
