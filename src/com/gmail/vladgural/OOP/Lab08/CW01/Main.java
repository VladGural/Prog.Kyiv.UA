package com.gmail.vladgural.OOP.Lab08.CW01;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Main {
    public static void main(String[] args) {
        Cat catOne = new Cat("Vasya", 12, 15, "Red");
        Cat catTwo = new Cat("Vasya", 12, 15, "Red");

        Cat catThree = null;

        try {
            catThree = catOne.clone();
        } catch (CloneNotSupportedException e) {
        }

        System.out.println(catOne.hashCode());
        System.out.println(catTwo.hashCode());

        System.out.println(catOne == catTwo);
        System.out.println(catOne.equals(catTwo));

        System.out.println(catOne == catThree);
        System.out.println(catOne.equals(catThree));

        Class catClass = Cat.class;
        System.out.println();

        Field[] fields = catClass.getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            System.out.println(fields[i]);
        }

        System.out.println();
        Method[] methods = catClass.getDeclaredMethods();
        for (int i = 0; i < methods.length; i++) {
            System.out.println(methods[i]);
        }

        System.out.println();
        Constructor[] constructors = catClass.getDeclaredConstructors();
        for (int i = 0; i < constructors.length; i++) {
            System.out.println(constructors[i]);
        }

        System.out.println();
        try {
            Field catAge = catClass.getDeclaredField("age");
            catAge.setAccessible(true);
            catAge.setInt(catOne, 100500);
        } catch (NoSuchFieldException e) {

        } catch (IllegalAccessException e) {

        }

        System.out.println(catOne);



        File file = new File("Cat.txt");

        try{
            SerializableFileWork.saveObjeckToFile(catOne, file);
        }catch(IOException e)
        {

        }

        Cat catFore = null;
        try{
            catFore = (Cat) SerializableFileWork.loadObjackFromFile(file);
        }catch (IOException e){

        }catch (ClassNotFoundException e){

        }

        System.out.println(catFore);
    }
}
