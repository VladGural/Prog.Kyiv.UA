package com.gmail.vladgural.OOP.Lab08.CW01;

import java.io.Serializable;
import java.util.Objects;

public class Cat implements Cloneable, Serializable{
    private String name;
    private int age;
    private double weight;
    private String colour;

    public Cat(String name, int age, double weight, String colour) {
        this.name = name;
        this.age = age;
        this.weight = weight;
        this.colour = colour;
    }

    public Cat() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    @Override
    public String toString() {
        return "Cat{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", weight=" + weight +
                ", colour='" + colour + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Cat)) return false;
        Cat cat = (Cat) o;
        return getAge() == cat.getAge() &&
                Double.compare(cat.getWeight(), getWeight()) == 0 &&
                Objects.equals(getName(), cat.getName()) &&
                Objects.equals(getColour(), cat.getColour());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getName(), getAge(), getWeight(), getColour());
    }

    @Override
    public Cat clone() throws CloneNotSupportedException {
        return (Cat) super.clone();
    }
}
