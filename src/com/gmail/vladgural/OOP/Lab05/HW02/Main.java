package com.gmail.vladgural.OOP.Lab05.HW02;

import java.io.*;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        String dir = "c:\\Temp\\Java\\LadWorks\\my-labs\\Files\\SameWord";
        File fin1 = new File(dir + "\\" + "1.txt");
        File fin2 = new File(dir + "\\" + "2.txt");
        File fout = new File(dir + "\\" + "Out.txt");
        int ch=0;

        try (Reader read1 = new FileReader(fin1);
             Reader read2 = new FileReader(fin2);
             CharArrayWriter chr1 = new CharArrayWriter();
             CharArrayWriter chr2 = new CharArrayWriter();
             PrintWriter prtWrt = new PrintWriter(new FileWriter(fout))) {

            for(;(ch = read1.read())!=-1;){
                chr1.write((char)ch);
            }
            for(;(ch = read2.read())!=-1;){
                chr2.write((char)ch);
            }

            System.out.println("File 1.txt contain ");
            System.out.println(chr1);
            System.out.println();
            System.out.println("File 2.txt contain ");
            System.out.println(chr2);
            System.out.println();

            String str1 = chr1.toString();
            String str2 = chr2.toString();

            str1=str1.toLowerCase();
            str2=str2.toLowerCase();

            String[] strs1 = str1.split("[ ,.:;\r\n]");
            String[] strs2 = str2.split("[ ,.:;\n\r]");

            strs1=deleteSame(strs1);
            strs2=deleteSame(strs2);


            System.out.println("Same words are");
            for(int i = 0;i<strs1.length;i++){
                for(int j = 0;j<strs2.length;j++){
                    if(strs1[i].equals(strs2[j])){
                        System.out.println(strs1[i]);
                        prtWrt.println(strs1[i]);
                    }
                }
            }


        } catch (IOException e){

        }
    }

    public static String[] deleteSame(String[] strs){
        int numberOfWords=0;

        for(int i = 0;i<strs.length;i++){
            for(int j = i+1;j<strs.length;j++){
                if(strs[i].equals(strs[j])){
                    strs[j]="";
                }
            }
        }

        Arrays.sort(strs);
        for (int i = 0;i<strs.length;i++){
            if(!(strs[i].equals(""))){
                numberOfWords++;
            }
        }

        String[] temp = new String[numberOfWords];

        for (int i = 0,j = 0;i<strs.length;i++){
            if(!(strs[i].equals(""))){
                temp[j++]=strs[i];
            }
        }


        return temp;
    }
}
