package com.gmail.vladgural.OOP.Lab05.CW01;

import java.io.*;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        File file = new File("a.txt");
        try(InputStream is = new FileInputStream(file)){

            System.out.println(is.available());

            byte[] buffer = new byte[7];

            int readByte = is.read(buffer);
            System.out.println(Arrays.toString(buffer) + readByte);

            readByte = is.read(buffer);
            System.out.println(Arrays.toString(buffer) + readByte);

        }catch(IOException  e){

        }
    }
}
