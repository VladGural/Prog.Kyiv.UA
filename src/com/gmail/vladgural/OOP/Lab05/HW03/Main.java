package com.gmail.vladgural.OOP.Lab05.HW03;

import java.io.File;

public class Main {
    public static void main(String[] args) {
        String dir = "c:\\Temp\\Java\\LadWorks\\my-labs\\Files\\Group";
        File fl = new File(dir+"\\"+"Group.txt");

        Person pr01 = new Person("Vlad", "Gural", 16,Sex.MALE);
        Person pr02 = new Person("Igor", "Makarenko", 22, Sex.MALE);
        Person pr03 = new Person("Vladimir","Bormotov",17, Sex.MALE);
        Person pr04 = new Person("Vladimir","Generalov",25, Sex.MALE);

        Group group1 = new Group("АСУ-1990-А");

        Group group2 = new Group("АСУ-1990-А");

        group1.insertStudent(pr01,3);
        group1.insertStudent(pr02,6);
        group1.insertStudent(pr03,8);
        group1.insertStudent(pr04,5);

        group1.writeToFile(fl);
        System.out.println(group1);


        System.out.println();
        System.out.println(group2);

        System.out.println();
        group2.readFromFile(fl);
        System.out.println(group2);

    }
}
