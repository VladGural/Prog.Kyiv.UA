package com.gmail.vladgural.OOP.Lab05.HW01;

import java.io.File;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        String[] list;
        String id = "c:\\Temp\\Java\\LadWorks\\my-labs\\Files\\In";
        String od = "c:\\Temp\\Java\\LadWorks\\my-labs\\Files\\In\\Out";


        File indir = new File(id);
        if(!(indir.exists())){
            indir.mkdir();
        }
        File outdir = new File(od);
        if(!(outdir.exists())){
            outdir.mkdir();
        }

        System.out.println("Before copy in IN-DIR are");
        printDir(indir);
        System.out.println();
        System.out.println("and OUT_DIR are");
        printDir(outdir);
        System.out.println();


        list = indir.list();

        for(int i = 0; i<list.length; i++){
            File flin = new File(id + "\\" + list[i]);
            if(flin.isFile()){
                int index = list[i].lastIndexOf(".");
                if(index!=-1){
                    String suf = list[i].substring(index+1);
                    if(suf.equals("pdf") || suf.equals("djvu")){
                        File flout = new File(od + "\\" + list[i]);
                        try{
                            StreamWork.copyFiles(flin,flout);
                        }catch (IOException e){
                            e.printStackTrace();
                        }
                    }
                }
            }
        }

        System.out.println("After copy in IN-DIR are");
        printDir(indir);
        System.out.println();
        System.out.println("and OUT_DIR are");
        printDir(outdir);
        System.out.println();
    }

    public static void printDir(File dir){
        if(dir.isDirectory()){
            String[] dirHeve = dir.list();
            if(dirHeve.length==0){
                System.out.println("dir is empty");
            }else {
                for (String str : dirHeve) {
                    System.out.println(str);
                }
            }
        }
    }
}
