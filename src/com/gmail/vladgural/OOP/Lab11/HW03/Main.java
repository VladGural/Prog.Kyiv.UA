package com.gmail.vladgural.OOP.Lab11.HW03;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {

        String url = "https://prog.kiev.ua/forum/index.php";
        String htmlCode = "";

        try{
            htmlCode = InetWork.getStringFromURL(url, "utf-8");
        }catch(IOException e){

        }
        System.out.println(htmlCode);

        List<String> links = new ArrayList<>();
        int currentIndex = 0;

        for(;(currentIndex = htmlCode.indexOf("href=",currentIndex))>0;){
            int startIndex = currentIndex+6;
            currentIndex = htmlCode.indexOf('"',startIndex);
            links.add(htmlCode.substring(startIndex,currentIndex));
        }

        for(String link:links){
            System.out.println(link);
        }

        String dirName = "c:\\Temp\\Java\\LadWorks\\my-labs\\Files\\Inet";
        File file = new File(dirName,"Links.txt");

        try(PrintWriter pw = new PrintWriter(file)){
            for(String link:links){
                pw.println(link);
            }
        }catch (FileNotFoundException e){

        }
    }
}
