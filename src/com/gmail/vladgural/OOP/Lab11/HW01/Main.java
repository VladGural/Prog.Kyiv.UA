package com.gmail.vladgural.OOP.Lab11.HW01;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        File file = new File("c:\\Temp\\Java\\LadWorks\\my-labs\\Files\\Inet","urls.txt");
        ArrayList<String> urls = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(file))){
            String url = "";
            for(;(url = br.readLine())!=null;){
                urls.add(url);
            }
        }catch (FileNotFoundException e){

        }catch (IOException e){

        }


        Map<String, List<String>> header = null;

        for(String url:urls) {
            header = InetWork.getHeaderFromURL(url);
            if(!header.isEmpty()){
                System.out.println("URL addres "+url+" is available and its header is");
                Set<String> keys = header.keySet();
                for(String key:keys){
                    System.out.println(key + " " + header.get(key));
                }
                System.out.println();
            }else{
                System.out.println("URL addres "+url+" is not available");
                System.out.println();
            }
        }
    }
}
