package com.gmail.vladgural.OOP.Lab11.HW01;

import com.gmail.vladgural.OOP.Lab11.CW01.StreamWork;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;

public class InetWork {

    public static void saveURLFileToFolder(String url, File folder)throws IOException {
        URL urlConnection = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) urlConnection.openConnection();
        int index = url.lastIndexOf("/");
        String fileName = url.substring(index+1);
        File file = new File(folder,fileName);
        try(InputStream is = connection.getInputStream();
            OutputStream os = new FileOutputStream(file)){

            StreamWork.streamCopy(is,os);

        }catch (IOException e){

        }
    }

    public static String getStringFromURL(String url, String decode)throws IOException{
        String htmlCode = "";
        URL urlConnection = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) urlConnection.openConnection();

        try(BufferedReader br =
                    new BufferedReader(new InputStreamReader(connection.getInputStream(), decode))){

            String str = "";
            for(;(str = br.readLine())!=null;){
                htmlCode+=str+System.lineSeparator();
            }

        }catch (IOException e){
            throw e;
        }

        return  htmlCode;
    }

    public static Map<String, List<String>> getHeaderFromURL(String url){
        Map<String, List<String>> result = null;

        try {
            URL urlConnection = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) urlConnection.openConnection();
            result = connection.getHeaderFields();
        }    catch (IOException e){

        }
        return result;
    }
}
