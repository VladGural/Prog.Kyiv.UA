package com.gmail.vladgural.OOP.Lab09.CW01;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
//        AnyClass<Integer> ac = new AnyClass<>(10);
//        Integer c = 5 + ac.getObj();
//        System.out.println("c = " + c);
//        AnyClass<String> str = new AnyClass<>("Hello World");

        Integer[] a = new Integer[] {10, 3, 7, -2, 8};

        Integer minOne = getMin(a);

        System.out.println(minOne);

        String[] b = new String[] {"AA", "BB", "A", "b"};

        String minString = getMin(b);

        System.out.println(minString);
    }

    public static <T extends Comparable<T>> T getMin(T[] array){
        T min = array[0];
        for(int i=0; i< array.length;i++){
            if(array[i].compareTo(min)<0){
                min = array[i];
            }
        }
        return min;
    }
}
