package com.gmail.vladgural.OOP.Lab09.CW01;

public class AnyClass<T extends Comparable<T>> {
    private T obj;

    public AnyClass(T obj) {
        this.obj = obj;
    }

    public AnyClass() {
    }

    public T getObj() {
        return obj;
    }

    public void setObj(T obj) {
        this.obj = obj;
    }

    @Override
    public String toString() {
        return "AnyClass{" +
                "obj=" + obj +
                '}';
    }
}
