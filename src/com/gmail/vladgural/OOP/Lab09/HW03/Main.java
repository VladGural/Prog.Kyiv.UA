package com.gmail.vladgural.OOP.Lab09.HW03;

import java.io.*;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        File file = new File("c:\\Temp\\Java\\LadWorks\\my-labs\\Files\\NumberChars\\Text.txt");
        char[] arrchar = new char[1024];
        int number;
        String str = "";
        try(Reader readFile = new FileReader(file)){
            for(;;){
                if((number = readFile.read(arrchar))==-1)
                    break;
                str=str+String.valueOf(arrchar,0,number);
            }
        }catch(FileNotFoundException e){

        }catch (IOException e){

        }

        System.out.println("Basic text is:\n\n");
        System.out.println(str+"\n\n");


        str = str.toUpperCase();
        arrchar = str.toCharArray();
        List<Character> list = new ArrayList<>();
        for(int i = 0;i<arrchar.length;i++){
            if(arrchar[i]>='A' && arrchar[i]<='Z'){
                list.add(arrchar[i]);
            }
        }

        Map<Character,Integer> map = new HashMap<>();
        map = getFrequencyOfItem(list);

        ArrayMap arrayMap = new ArrayMap();

        for(int i=0; i<26; i++){
            Character key = (char)('A'+i);
            Integer value = map.get(key);
            if(value!=null)
                arrayMap.add(new MapItem(key,value));
        }

        arrayMap.sort();
        System.out.println(arrayMap);
    }


    public static Map<Character, Integer> getFrequencyOfItem(List<Character> list){
        Map<Character, Integer> result = new HashMap<>();
        for(int i=0; i<list.size(); i++){
            Integer count = result.get(list.get(i));
            if(count == null){
                result.put(list.get(i),1);

            }else {
                result.put(list.get(i),count+1);
            }
        }
        return result;
    }
}
