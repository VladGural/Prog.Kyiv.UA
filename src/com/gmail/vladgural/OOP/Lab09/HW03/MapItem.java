package com.gmail.vladgural.OOP.Lab09.HW03;

public class MapItem
{
    private Character letter;
    private Integer frequency;

    public MapItem(Character letter, Integer frequency) {
        this.letter = letter;
        this.frequency = frequency;
    }

    public MapItem() {
    }

    public Character getLetter() {
        return letter;
    }

    public void setLetter(Character letter) {
        this.letter = letter;
    }

    public Integer getFrequency() {
        return frequency;
    }

    public void setFrequency(Integer frequency) {
        this.frequency = frequency;
    }

    @Override
    public String toString() {
        return "letter=" + letter + ", frequency=" + frequency;
    }
}
