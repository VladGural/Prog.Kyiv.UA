package com.gmail.vladgural.OOP.Lab09.HW03;

import java.util.ArrayList;

public class ArrayMap {
    ArrayList<MapItem> arrayMap = new ArrayList<>();

    public ArrayMap() {
    }

    public void add(MapItem mapItem){
        arrayMap.add(mapItem);
    }

    public void sort(){
        arrayMap.sort(  (a,b) -> b.getFrequency() - a.getFrequency()
                        );
    }

    @Override
    public String toString() {
        String str = "";
        str = "Letters and frequency\n";
        for(int i=0; i<arrayMap.size(); i++){
            str += arrayMap.get(i).toString();
            str += "\n";
        }

        return str;
    }
}
