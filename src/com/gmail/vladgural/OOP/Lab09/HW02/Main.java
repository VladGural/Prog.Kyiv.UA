package com.gmail.vladgural.OOP.Lab09.HW02;

import com.mainacad.Practise.Spring.StandartOutMessageRenderer;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Person pr01 = new Person("Vlad", "Gural", 16, Sex.MALE);
        Person pr02 = new Person("Igor", "Makarenko", 22, Sex.MALE);
        Person pr03 = new Person("Vladimir","Bormotov",17, Sex.MALE);
        Person pr04 = new Person("Vladimir","Generalov",25, Sex.MALE);
        Person pr05 = new Person("Natalya","Litvinova",20, Sex.FEMALE);
        Person pr06 = new Person("Alyena","Povaliy",16, Sex.FEMALE);
        Person pr07 = new Person("Vladimir","Abramov",17, Sex.MALE);
        Person pr08 = new Person("Andrey","Pakormlyak",28, Sex.MALE);
        Person pr09 = new Person("Phelics","Tabakin", 16, Sex.MALE);
        Person pr10 = new Person("Oleg","Komov",17, Sex.MALE);

        GroupDB gDB = new GroupDB();
        Group group1 = new Group("АСУ1990");
        Group group2 = new Group("АТ1990");
        Group group3 = new Group("ПО1990");



        group1.insertStudent(pr01,3);
        group1.insertStudent(pr02,6);
        group1.insertStudent(pr03,8);
        group1.insertStudent(pr04,5);

        group2.insertStudent(pr05,3);
        group2.insertStudent(pr06,8);

        group3.insertStudent(pr07,4);
        group3.insertStudent(pr08,8);
        group3.insertStudent(pr09,6);
        group3.insertStudent(pr10,9);

        gDB.addGroupToDB(group1);
        gDB.addGroupToDB(group2);

        String[] grList;

        System.out.println("These groups are in DB");
        grList = gDB.getGroupList();
        for(String grstr:grList){
            System.out.println(grstr);
        }
        System.out.println();

        Group group4 = null;

        System.out.println("Find Group ПО1990 in DB");
        try{
            group4 = gDB.getGroupFromDB("ПО1990");
            System.out.println(group4);
            System.out.println();
        }catch (NotFoundInDBException e){
            System.out.println("This Group not found in DB");
        }
        System.out.println();

        System.out.println("Add Group ПО1990 in DB");
        gDB.addGroupToDB(group3);
        System.out.println();

        System.out.println("These groups are in DB");
        grList = gDB.getGroupList();
        for(String grstr:grList){
            System.out.println(grstr);
        }
        System.out.println();

        System.out.println("Find Group ПО1990 in DB");
        try{
            group4 = gDB.getGroupFromDB("ПО1990");
            System.out.println(group4);
            System.out.println();
        }catch (NotFoundInDBException e){
            System.out.println("This Group not found in DB");
        }

        System.out.println("Sort group ПО1990 by FirstName");
        group4.sortByParameter(SortParameter.FIRST_NAME);
        System.out.println(group4);

        ArrayList<Student> recruts;
        recruts = group4.getRecrut();

        System.out.println("Recruts in this group are");
        for(Student st:recruts){
            System.out.println(st);
        }

    }
}
