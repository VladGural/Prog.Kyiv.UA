package com.gmail.vladgural.OOP.Lab09.HW02;

import java.io.*;
import java.util.*;

public class Group implements Voencom, Serializable {
    private ArrayList<Student> students = new ArrayList<>();
    private int numberOfStudents;
    private String nameOfGroup;

    public Group(String nameOfGroup) {
        this.nameOfGroup = nameOfGroup;
    }

    public Group() {
    }

    public ArrayList<Student> getStudents() {
        return students;
    }

    public int getNumberOfStudents() {
        return numberOfStudents;
    }

    public String getNameOfGroup() {
        return nameOfGroup;
    }

    private String getStudentFirstName(Scanner scanner, String str){ //"Enter students' first name"
        while (true){
            System.out.println(str);
            return scanner.nextLine();
        }
    }

    private String getStudentLastName(Scanner scanner, String str){//"Enter students' last name"
        while (true){
            System.out.println(str);
            return scanner.nextLine();
        }
    }

    private int getStudentAge(Scanner scanner, String str){//"Enter students' age"
        while (true){
            try {
                System.out.println(str);
                int age = scanner.nextInt();
                scanner.nextLine();
                return age;
            }catch (InputMismatchException e){
                System.out.println("Enter number");
                scanner.nextLine();
            }
        }
    }

    private Sex getStudentSex(Scanner scanner, String str){//"Enter students' sex (1 = male or 2 = female)"
        String s;
        while (true){
            System.out.println(str);
            s = scanner.nextLine();

            if(s == null)
                s = "male";

            if(s.equalsIgnoreCase("male")){
                return Sex.MALE;
            }else {
                return Sex.FEMALE;
            }
        }
    }

    private int getStudentMiddleMark(Scanner scanner, String str){//"Enter students' middle mark"
        while (true){
            try {
                System.out.println(str);
                int middleMark = scanner.nextInt();
                scanner.nextLine();
                return middleMark;
            }catch (InputMismatchException e){
                System.out.println("Enter number");
                scanner.nextLine();
            }
        }
    }

    public void insertStudent(){
        Scanner scanner = new Scanner(System.in);
        Person person = new Person();
        int middleMark;
        System.out.println("Insert new student into group " + nameOfGroup);

        person.setFirstName(getStudentFirstName(scanner,"Enter students' first name"));
        person.setLastName(getStudentLastName(scanner, "Enter students' last name"));
        person.setAge(getStudentAge(scanner, "Enter students' age"));
        person.setSex(getStudentSex(scanner, "Enter students' sex (male or female)"));
        middleMark = getStudentMiddleMark(scanner,"Enter students' middle mark");

        scanner.close();

        insertStudent(person, middleMark);

    }


    public void insertStudent(Person person, int middleMark){

            for(int i = 0; i<numberOfStudents; i++){  // Нельзя вставить студента с такойже Фанилией
                if(students.get(i).getLastName().equals(person.getLastName())){
                    return;
                }
            }
            students.add(new Student(person, nameOfGroup, middleMark));
            numberOfStudents++;
    }

    public void deleteOfStudent(String lastName){
        int number = -1;
        for(int i=0; i<numberOfStudents; i++){
            if(students.get(i).getLastName().equals(lastName)){
                number = i;
                break;
            }
        }
        if(number!=-1){
            students.remove(number);
            numberOfStudents--;
        }
    }

    public void sortByParameter(SortParameter parameter){
        switch (parameter) {
            case FIRST_NAME:
                students.sort( (a, b)-> {
                    if(CheckNull.checkNull(a,b)!= CheckNull.NOT_NULL){
                        return CheckNull.checkNull(a,b);
                    }
                        return a.getFirstName().compareTo(b.getFirstName());
                    });
                break;
            case LAST_NAME:
                students.sort( (a, b)-> {
                    if(CheckNull.checkNull(a,b)!= CheckNull.NOT_NULL){
                        return CheckNull.checkNull(a,b);
                    }
                    return a.getLastName().compareTo(b.getLastName());
                });
                break;
            case MIDDLE_MARK:
                students.sort( (a, b)-> {
                    if(CheckNull.checkNull(a,b)!= CheckNull.NOT_NULL){
                        return CheckNull.checkNull(a,b);
                    }
                    return a.getMiddlemark() - b.getMiddlemark();
                });
                break;
        }
    }

    public ArrayList<Student> getRecrut(){
        ArrayList<Student> recruts = new ArrayList<>();
        for(int i = 0; i < numberOfStudents; i++){
            if(students.get(i).getAge()>18 && students.get(i).getSex() == Sex.MALE){
                recruts.add(students.get(i));
            }
        }

        return recruts;
    }

    @Override
    public String toString() {
        String str;

        if(numberOfStudents==0){
            str = "Group " + nameOfGroup + " is enpty \n";
        }else {

            str = "Students of group " + nameOfGroup + " are \n";
            for (int i = 0; i < numberOfStudents; i++) {
                str += students.get(i).toString();
                str += "\n";
            }
            str += "and total number of students is " + numberOfStudents + "\n";
        }
        return str;
    }

    public void writeToFile(File fl) {
        try (PrintWriter pw = new PrintWriter(fl)){
            for(int i = 0; i<numberOfStudents; i++){
                String str = null;
                str  = students.get(i).getFirstName()+",";
                str += students.get(i).getLastName()+",";
                str += students.get(i).getAge()+",";
                str += students.get(i).getSex()+",";
                str += students.get(i).getMiddlemark()+",";
                str += students.get(i).getNameOfGroup();
                pw.println(str);
            }

        } catch (FileNotFoundException e) {

        } catch (IOException e) {

        }
    }

    public void readFromFile(File fl){
        try(BufferedReader br = new BufferedReader(new FileReader(fl));){
            String strline = null;

            students = new ArrayList<>();
            numberOfStudents = 0;
            nameOfGroup = null;

            while((strline = br.readLine())!=null){
                String firstName;
                String lastName;
                int age;
                Sex sex;
                int middleMark;
                Person person = null;

                String[] str = strline.split("[,]");
                firstName = str[0];
                lastName = str[1];
                age = Integer.valueOf(str[2]);
                sex = Sex.valueOf(str[3]);
                middleMark = Integer.valueOf(str[4]);
                nameOfGroup = str[5];

                person = new Person(firstName,lastName,age, sex);
                insertStudent(person,middleMark);
            }
        }catch(FileNotFoundException e){

        }catch(IOException e){

        }
    }
}
