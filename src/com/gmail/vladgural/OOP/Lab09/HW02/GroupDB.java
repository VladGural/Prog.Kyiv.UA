package com.gmail.vladgural.OOP.Lab09.HW02;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GroupDB {
    private String dir = "c:\\Temp\\Java\\LadWorks\\my-labs\\Files\\Group";

    public void addGroupToDB(Group gr){
        File file = new File(dir + "\\" + gr.getNameOfGroup() + ".gdb");

        try {
            SerializableFileWork.saveObjeckToFile(gr, file);

        } catch (IOException e) {
            System.out.println("IOException in addGroupToDB method");
        }
    }

    public Group getGroupFromDB(String groupName)throws NotFoundInDBException {
        String[] groupList = getGroupList();
        boolean isInDB = false;

        groupList = getGroupList();

        for(int i = 0; i< groupList.length; i++){
            if(groupList[i].equals(groupName)){
                isInDB = true;
            }
        }

        if(!isInDB){
            throw new NotFoundInDBException();
        }

        File file = new File(dir + "\\" + groupName + ".gdb");

        Group group = null;
        try{
            group = (Group) SerializableFileWork.loadObjackFromFile(file);
        }catch (ClassNotFoundException e){
            System.out.println(e);
        }catch (IOException e){
            System.out.println(e);
        }
        return group;


    }

    public String[] getDirList(){
        String[] dirList = null;
        File dirDB = new File(dir);
        dirList = dirDB.list();
        return dirList;
    }

    public String[] getGroupList(){
        String[] dirList = null;
        String[] groupList = null;
        List<String> list = new ArrayList<>();
        int length;

        dirList = getDirList();

        for(int i=0; i<dirList.length; i++){
            String temp;
            temp = dirList[i].substring(dirList[i].length()-3);
            if(temp.equals("gdb")){
                list.add(dirList[i].substring(0,dirList[i].length()-4));
            }
        }
        length = list.size();
        groupList = new String[length];
        for(int i = 0; i<length; i++){
            groupList[i] = list.get(i);
        }


        return groupList;
    }
}
