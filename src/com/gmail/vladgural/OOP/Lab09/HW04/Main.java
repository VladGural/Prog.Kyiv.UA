package com.gmail.vladgural.OOP.Lab09.HW04;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Queue;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Deque<String> deque = new ArrayDeque<>();

        deque.addLast("Шелдон");
        deque.addLast("Леонард");
        deque.addLast("Воловиц");
        deque.addLast("Кутрапалли");
        deque.addLast("Пенни");

        System.out.println(deque);

        System.out.println("Как много стаканчиков может выдать бормен?");
        Scanner scanner = new Scanner(System.in);
        int number;
        number = scanner.nextInt();

        for(int i=0; i<number; i++){
            String person;
            person = deque.pollFirst();
            deque.addLast(person);
            deque.addLast(person);
        }

        System.out.println("Очередь после " + number + " стаканчиков выглядит так:");
        System.out.println(deque);
    }
}
