package com.gmail.vladgural.OOP.Lab09.CW02;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Integer> myList = new ArrayList<>();
        Integer[] array;

        myList.add(10);
        myList.add(7);
        myList.add(2);
        System.out.println(myList);

        array = myList.toArray(new Integer[0]);

        myList.add(1,4);
        System.out.println(myList);

        Integer number = myList.get(0);
        System.out.println(number);

        myList.remove(3);
        System.out.println(myList);

        myList.set(1,50);
        System.out.println(myList);

        for (Integer el:myList){
            System.out.println(el);
        }
        System.out.println();

        Iterator<Integer> itr = myList.iterator();

        for(;itr.hasNext();){
            Integer el = itr.next();
            System.out.println(el);
        }

        Iterator<Integer> itr1 = myList.iterator();

        for(;itr1.hasNext();){
            Integer el = itr1.next();
            if(el>20){
                itr1.remove();
            }
        }

        System.out.println(myList);

        Collections.sort(myList);
        System.out.println(myList);
    }
}
