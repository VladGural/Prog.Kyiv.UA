package com.gmail.vladgural.OOP.Lab07.CW01;

public class Vlad implements Runnable {
    private Synh s;
    private String text;
    private boolean flag;

    public Vlad(String text,Synh s, boolean flag) {
        this.text = text;
        this.s = s;
        this.flag =flag;
    }

    public Synh getS() {
        return s;
    }

    public void setS(Synh s) {
        this.s = s;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    @Override
    public void run() {
        for(int i = 0; i<5; i++){
            s.print(this);
        }
    }
}
