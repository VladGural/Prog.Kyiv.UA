package com.gmail.vladgural.OOP.Lab07.CW01;

public class Synh {
    private boolean flag = true;


    public synchronized void print(Vlad vl){
        for(;vl.isFlag()!=flag;) {
            try {
                wait();
            } catch (InterruptedException e) {

            }
        }
            System.out.print(vl.getText()+" ");
            this.flag = !this.flag;
            notifyAll();
    }
}
