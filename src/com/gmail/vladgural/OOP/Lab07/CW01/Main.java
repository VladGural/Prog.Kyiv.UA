package com.gmail.vladgural.OOP.Lab07.CW01;

public class Main {
    public static void main(String[] args) {
        Synh sh = new Synh();
        Vlad v1 = new Vlad("Posh",sh,true);
        Vlad v2 = new Vlad("Poll",sh,false);

        Thread th1 = new Thread(v1);
        Thread th2 = new Thread(v2);

        th1.start();
        th2.start();
    }
}
