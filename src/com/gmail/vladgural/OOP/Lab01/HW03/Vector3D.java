package com.gmail.vladgural.OOP.Lab01.HW03;

public class Vector3D {
    private double x;
    private double y;
    private double z;

    public Vector3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Vector3D() {
        x = 1;
        y = 1;
        z = 1;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    @Override
    public String toString() {
        return "Vector3D{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }

    public Vector3D sum(Vector3D vector){
        return new Vector3D((this.x + vector.getX()),(this.y + vector.getY()),(this.z + vector.getZ()));
    }

    public Vector3D vectorMultiplication(Vector3D vector){
        return new Vector3D((this.y * vector.getZ() - vector.getY() * this.z),
                            ((this.x * vector.getZ() - vector.getX() * this.z) * -1),
                            (this.x * vector.getY() - vector.getX() * this.y));
    }

    public double scalarMultiplikation(Vector3D vector){
        return this.x * vector.getX() + this.y * vector.getY() + this.z * vector.getZ();
    }
}
