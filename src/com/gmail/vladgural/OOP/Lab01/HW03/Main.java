package com.gmail.vladgural.OOP.Lab01.HW03;

import static java.lang.String.format;

public class Main {
    public static void main(String[] args) {
        Vector3D vector1 = new Vector3D(1,2,3);
        Vector3D vector2 = new Vector3D(-2,0,4);
        Vector3D vector3;

        vector3 = vector1.sum(vector2);
        System.out.println("Sun of vector " + vector1.toString());
        System.out.println("and vector " + vector2.toString());
        System.out.println("is vector " + vector3.toString());

        vector3 = vector1.vectorMultiplication(vector2);
        System.out.println();
        System.out.println("Vector's multiplication of vector " + vector1);
        System.out.println("and vector " + vector2);
        System.out.println("is vector " + vector3.toString());

        System.out.println();
        System.out.println("Scalar's multiplication of vector " + vector1);
        System.out.println("and vector " + vector2);
        System.out.println("is number " + format("%.2f",vector1.scalarMultiplikation(vector2)));
    }
}
