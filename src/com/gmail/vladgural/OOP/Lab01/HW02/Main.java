package com.gmail.vladgural.OOP.Lab01.HW02;

import static java.lang.String.format;

public class Main {
    public static void main(String[] args) {
        Triangle tr1 = new Triangle();
        Triangle tr2 = new Triangle(6,6,6);

        System.out.println(tr1 + " has square of " + tr1.square() + " mm^2" );
        System.out.println(tr2 + " has square of " + format("%.2f",tr2.square()) + " mm^2" );
    }
}
