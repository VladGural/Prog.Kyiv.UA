package com.gmail.vladgural.OOP.Lab01.HW02;

public class Triangle {
    private double a;
    private double b;
    private double c;

    public Triangle(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public Triangle() {
        a = 3;
        b = 4;
        c = 5;
    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    public double getC() {
        return c;
    }

    public void setC(double c) {
        this.c = c;
    }

    @Override
    public String toString() {
        return "Tirangle{" +
                "a=" + a +
                ", b=" + b +
                ", c=" + c +
                '}';
    }

    public double square(){
        double p;
        double s;

        p = (a + b + c)/2;
        s = Math.sqrt(p * (p - a) * (p - b) * (p - c));
        return s;
    }
}
