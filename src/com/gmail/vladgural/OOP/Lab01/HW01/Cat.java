package com.gmail.vladgural.OOP.Lab01.HW01;

public class Cat {
    private String name;
    private double weight;
    private int age;
    private String temper;

    public Cat(String name, double weight, int age, String temper) {
        this.name = name;
        this.weight = weight;
        this.age = age;
        this.temper = temper;
    }

    public Cat() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getTemper() {
        return temper;
    }

    public void setTemper(String temper) {
        this.temper = temper;
    }

    @Override
    public String toString() {
        return "Cat{" +
                "name='" + name + '\'' +
                ", weight=" + weight +
                ", age=" + age +
                ", temper='" + temper + '\'' +
                '}';
    }

    public String petCat(){
        String doing;
        switch(temper){
            case "kind":
                doing = "Мурррр... Муррр...";
                break;
            case "angry":
                doing = "Сшшш... Сшшшш... Гав!!!";
                break;
            default:
                doing = "ничего";
                break;
        }
        return doing;
    }
}
