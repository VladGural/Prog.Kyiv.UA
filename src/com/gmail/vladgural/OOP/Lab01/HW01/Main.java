package com.gmail.vladgural.OOP.Lab01.HW01;

public class Main {
    public static void main(String[] args) {
        Cat cat1 = new Cat("Мурка",10,5,"kind");
        Cat cat2 = new Cat("Егор",15,9,"angry");
        Cat cat3 = new Cat("Булочка",4,2,"shy");

        System.out.println(cat1.toString());
        System.out.println("Гладим кошку " + cat1.getName() + " и она делает " + cat1.petCat());
        System.out.println();

        System.out.println(cat2.toString());
        System.out.println("Гладим кошку " + cat2.getName() + " и она делает " + cat2.petCat());
        System.out.println();

        System.out.println(cat3.toString());
        System.out.println("Гладим кошку " + cat3.getName() + " и она делает " + cat3.petCat());
        System.out.println();
    }
}
