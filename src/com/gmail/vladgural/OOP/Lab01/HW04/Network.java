package com.gmail.vladgural.OOP.Lab01.HW04;

public class Network {
    private String[] phonsInNetwork;
    private int numberPhonesInNetwork;

    public Network() {
        phonsInNetwork = new String[10];
        numberPhonesInNetwork = 0;
    }

    public String[] getPhonsInNetwork() {
        return phonsInNetwork;
    }

    public void setPhonsInNetwork(String[] phonsInNetwork) {
        this.phonsInNetwork = phonsInNetwork;
    }

    public int getNumberPhonesInNetwork() {
        return numberPhonesInNetwork;
    }

    public void setNumberPhonesInNetwork(int numberPhonesInNetwork) {
        this.numberPhonesInNetwork = numberPhonesInNetwork;
    }

    public boolean isRegistered(Phone phone){
        for(int i = 0; i<numberPhonesInNetwork; i++){
            if (phonsInNetwork[i].equals(phone.getNumber())){
                return true;
            }
        }
        return false;
    }
}
