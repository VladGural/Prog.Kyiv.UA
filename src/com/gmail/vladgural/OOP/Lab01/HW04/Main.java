package com.gmail.vladgural.OOP.Lab01.HW04;

public class Main {
    public static void main(String[] args) {
        Network network = new Network();
        Phone phone1 = new Phone("05010","Nokia");
        Phone phone2 = new Phone("05020","Sony");
        Phone phone3 = new Phone("05030","Lenovo");

        phone1.registerPhoneToNetwork(network);
        phone2.registerPhoneToNetwork(network);

        System.out.println(phone1.call(phone2,network));
        System.out.println();
        System.out.println(phone1.call(phone3,network));
    }
}
