package com.gmail.vladgural.OOP.Lab01.HW04;

public class Phone {
    private String number;
    private String model;

    public Phone(String number, String model) {
        this.number = number;
        this.model = model;
    }

    public Phone() {
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Override
    public String toString() {
        return "Phone{" +
                "number='" + number + '\'' +
                ", model='" + model + '\'' +
                '}';
    }

    public boolean registerPhoneToNetwork(Network network){
        String[] numbers;

        if(network.getNumberPhonesInNetwork() != 10){
            numbers = network.getPhonsInNetwork();
            numbers[network.getNumberPhonesInNetwork()]=number;
            network.setNumberPhonesInNetwork(network.getNumberPhonesInNetwork() + 1);
            return true;
        } else {
            return false;
        }
    }

    public String call(Phone phone,Network netvork){
        if(netvork.isRegistered(phone)){
            return "Phone namber " + this.getNumber() + " calls to phone number " + phone.getNumber() + " Dzzzinnn";
        }else {
            return "Called phone number " + phone.getNumber() + " isn't registered in this Network!!!";
        }
    }
}
