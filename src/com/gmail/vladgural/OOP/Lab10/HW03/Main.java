package com.gmail.vladgural.OOP.Lab10.HW03;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Integer[] array = new Integer[100];
        Random rn = new Random();

        for(int i=0;i< array.length;i++){
            array[i]=rn.nextInt(11);
        }

        Map<Integer,Integer> map;

        map = getStatistic(array);
        System.out.println(map);
    }


    public static <T> Map<T, Integer> getStatistic(T[] array){
        Map<T, Integer> result = new HashMap<>();
        for(T key: array){
            Integer count = result.get(key);
            if(count == null){
                result.put(key,1);

            }else {
                result.put(key,count+1);
            }
        }
        return result;
    }
}
