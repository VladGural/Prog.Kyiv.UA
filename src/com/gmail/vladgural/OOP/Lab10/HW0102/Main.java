package com.gmail.vladgural.OOP.Lab10.HW0102;

import java.io.*;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        String dir = "c:\\Temp\\Java\\LadWorks\\my-labs\\Files\\Translate";
        ArrayList<String> linesEn;
        ArrayList<String> linesRu = new ArrayList<>();


        Translate tr = new Translate();
        tr.readDictionaryFromFile(dir,"test.dct");
        System.out.println("Our dictionary is");
        System.out.println(tr.getDictionary());

        System.out.println("Do you want add new words?");
        tr.addToDictionary();
        tr.writeDictionaryToFile(dir,"test.dct");

        linesEn = readLinesFromFile(dir, "English.in");
        System.out.println(linesEn);

        for(int i=0; i<linesEn.size();i++){
            linesRu.add(tr.translateLine(linesEn.get(i)));
        }

        writeLinesToFile(dir,"Russian.out",linesRu);
        System.out.println(linesRu);
    }

    public static ArrayList<String> readLinesFromFile(String dirName, String fileName){
        File file = new File(dirName,fileName);
        ArrayList<String> lines = new ArrayList<>();

        try(BufferedReader br =  new BufferedReader(new FileReader(file))){
            String line = "";

            for(;(line = br.readLine())!= null;){
                lines.add(line);
            }
        }catch (FileNotFoundException e){
            System.out.println("File not found");
        }catch (IOException e){

        }
        return lines;
    }

    public static void writeLinesToFile(String dirName, String fileName, ArrayList<String> linesRu){
        File file = new File(dirName,fileName);

        try(PrintWriter pw = new PrintWriter(file)){
            for(String lineRu:linesRu){
                pw.println(lineRu);
            }
        }catch (FileNotFoundException e){

        }
    }
}
