package com.gmail.vladgural.OOP.Lab10.HW0102;

import java.io.*;
import java.util.*;

public class Translate {
    Map<String,String> dictionary;

    public Translate() {
        dictionary = new HashMap<>();
    }

    public Translate(String dirName, String fileName) {
        dictionary = new HashMap<>();
        readDictionaryFromFile(dirName,fileName);
    }

    public Map<String, String> getDictionary() {
        return dictionary;
    }

    public void addToDictionary(String wordEn, String wordRu){
        dictionary.put(wordEn,wordRu);
    }

    public void addToDictionary(){
        Scanner scanner = new Scanner(System.in);

        for(;;){
            String wordEn = "";
            String wordRu = "";
            System.out.println("Enter English word");
            wordEn = scanner.nextLine();
            if(wordEn.equals(""))
                break;
            System.out.println("Enter Russian word");
            wordRu = scanner.nextLine();
            dictionary.put(wordEn,wordRu);
            System.out.println();
        }
    }


    public void readDictionaryFromFile(String dirName, String fileName){
        File file = new File(dirName, fileName);

        try(BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file),"utf-8"))){
            String strLine = "";
            for(;(strLine = br.readLine())!=null;){
                String[] strs = strLine.split("[,]");
                dictionary.put(strs[0],strs[1]);
            }
        }catch (FileNotFoundException e){
            System.out.println("Don't correct file name");
        }catch (IOException e){

        }
    }

    public void writeDictionaryToFile(String dirName, String fileName){
        File file = new File(dirName, fileName);
        Set<String> set = dictionary.keySet();

        try(PrintWriter prw = new PrintWriter(file)){
            for(String str:set){
                String result = "";
                result+=str+",";
                result+=dictionary.get(str);
                prw.println(result);
            }
        }catch(FileNotFoundException e){

        }
    }

    public String translateLine(String lineEn){
        String lineRu = "";
        String[] strpars;

        lineEn = lineEn.toLowerCase();
        strpars = lineEn.split("[ ,.]");
        for(int i = 0; i<strpars.length; i++){
            String temp = dictionary.get(strpars[i]);
            if(temp == null){
                lineRu+=strpars[i];
                lineRu+=" ";
            }else{
                lineRu+=temp;
                lineRu+=" ";
            }
        }
        return lineRu;
    }
}
