package com.gmail.vladgural.OOP.Lab10.CW01;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Map<Integer, String> map = new HashMap<>();

        map.put(1,"one");
        map.put(5,"five");
        map.put(2,"two");

        System.out.println(map);
        System.out.println();

        String number = map.get(5);

        System.out.println(number);
        System.out.println();

        Set<Integer> keys = map.keySet();
        for(Integer key:keys){
            System.out.println(key+" "+map.get(key));
        }
        System.out.println();

        map.forEach((key,value) -> System.out.println(key+" "+value));
        System.out.println();

        map.put(1,"One");

        System.out.println(map.get(1));
        System.out.println();

        map.remove(1);
        System.out.println(map);
    }
}
