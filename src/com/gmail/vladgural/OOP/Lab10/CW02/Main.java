package com.gmail.vladgural.OOP.Lab10.CW02;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Integer[] array = new Integer[100];
        Random rn = new Random();

        for(int i=0;i< array.length;i++){
            array[i]=rn.nextInt(11);
        }

        Map<Integer,Integer> map;

        map = getStatistic(array);
        System.out.println(map);

        map = getStar(array);
        System.out.println(map);
    }

    public static Map<Integer, Integer> getStatistic(Integer[] array) {

        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < array.length; i++){
            Integer key;
            Integer value;
            key = array[i];
            if(map.get(key)==null){
                map.put(key,1);
            }
            else{
                value = map.get(key);
                map.put(key,++value);
            }
        }
        return map;
    }

    public static <T> Map<T, Integer> getStar(T[] array){
        Map<T, Integer> result = new HashMap<>();
        for(T key: array){
            Integer count = result.get(key);
            if(count == null){
                result.put(key,1);

            }else {
                result.put(key,count+1);
            }
        }
        return result;
    }
}
