package com.gmail.vladgural.OOP.Lab02.HW01;

import java.util.Arrays;

public class Board {
    private Shape[] shapes = new Shape[4];

    public Board() {
    }

    public Shape[] getShapes() {
        return shapes;
    }

    public void insertShape(Shape shape, int numberOfQuarter){
        if(numberOfQuarter<0 || numberOfQuarter>3){
            return;
        }
        getShapes()[numberOfQuarter] = shape;
    }

    public void deleteShape(int numberOfQuarter){
        if(numberOfQuarter<0 || numberOfQuarter>3){
            return;
        }
        getShapes()[numberOfQuarter] = null;
    }

    @Override
    public String toString() {
        String[] str = new String[4];
        double s = 0;

        for(int i = 0; i<4; i++){
            if(getShapes()[i]!=null){
                str[i] = "in quarter " + i + " is " + getShapes()[i].toString() + "\n";
                s += getShapes()[i].getArea();
            }else {
                str[i] = "in quarter " + i + " is empty" + "\n";
            }
        }


        return "Board has{ \n" +
                str[0] +
                str[1] +
                str[2] +
                str[3] +
                "and total area is " + s + "\n" +
                '}';
    }
}
