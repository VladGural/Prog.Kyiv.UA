package com.gmail.vladgural.OOP.Lab02.HW01;

import javax.sound.midi.Soundbank;

public class Main {
    public static void main(String[] args) {
        Triangle tr1 = new Triangle();
        Triangle tr2 = new Triangle(new Point(0,0), new Point(0,5), new Point(5,0));
        Circle cr1 = new Circle();
        Board board = new Board();

        System.out.println(tr1);
        System.out.println("Perinetr is " + tr1.getPerimetr() + " and area is " + tr1.getArea());
        System.out.println();
        System.out.println(tr2);
        System.out.println("Perinetr is " + tr2.getPerimetr() + " and area is " + tr2.getArea());
        System.out.println();
        System.out.println(cr1);
        System.out.println("Perinetr is " + cr1.getPerimetr() + " and area is " + cr1.getArea());
        System.out.println();
        System.out.println(board); //in the bigining board is empty
        System.out.println();

        board.insertShape(tr1,0);
        board.insertShape(tr2,2);
        board.insertShape(cr1,3);
        System.out.println(board);
        System.out.println();

        board.deleteShape(2);
        System.out.println(board);
        System.out.println();
    }
}
