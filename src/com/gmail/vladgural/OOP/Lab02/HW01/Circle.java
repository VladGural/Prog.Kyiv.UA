package com.gmail.vladgural.OOP.Lab02.HW01;

public class Circle extends Shape{
    private Point centre;
    private Point inOrbirt;

    public Circle(Point centre, Point inOrbirt) {
        this.centre = centre;
        this.inOrbirt = inOrbirt;
    }

    public Circle() {
        centre = new Point(0,0);
        inOrbirt = new Point(0,1);
    }

    public Point getCentre() {
        return centre;
    }

    public void setCentre(Point centre) {
        this.centre = centre;
    }

    public Point getInOrbirt() {
        return inOrbirt;
    }

    public void setInOrbirt(Point inOrbirt) {
        this.inOrbirt = inOrbirt;
    }

    public double getModOfVector(Point p1, Point p2){
        return Math.sqrt(Math.pow(p1.getX()-p2.getX(),2)+Math.pow(p1.getY()-p2.getY(),2));
    }

    @Override
    public double getPerimetr(){
        double r;

        r = getModOfVector(getInOrbirt(),getCentre());

        return 2 * Math.PI * r;
    }

    @Override
    public double getArea(){
        double r;

        r = getModOfVector(getInOrbirt(),getCentre());

        return Math.PI * Math.pow(r, 2);
    }

    @Override
    public String toString() {
        double r;

        r = getModOfVector(getInOrbirt(),getCentre());

        return "Circle{" +
                "Radius=" + r +
                '}';
    }
}
