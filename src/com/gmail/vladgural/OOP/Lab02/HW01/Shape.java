package com.gmail.vladgural.OOP.Lab02.HW01;

abstract public class Shape {
    abstract public double getPerimetr();
    abstract public double getArea();
}
