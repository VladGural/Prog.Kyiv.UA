package com.gmail.vladgural.OOP.Lab02.HW01;

public class Triangle extends Shape{
    private Point p1;
    private Point p2;
    private Point p3;

    public Triangle(Point p1, Point p2, Point p3) {
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
    }

    public Triangle() {
        p1 = new Point(0,0);
        p2 = new Point(0,3);
        p3 = new Point(4,0);
    }

    public Point getP1() {
        return p1;
    }

    public void setP1(Point p1) {
        this.p1 = p1;
    }

    public Point getP2() {
        return p2;
    }

    public void setP2(Point p2) {
        this.p2 = p2;
    }

    public Point getP3() {
        return p3;
    }

    public void setP3(Point p3) {
        this.p3 = p3;
    }

    public double getModOfVector(Point p1, Point p2){
        return Math.sqrt(Math.pow(p1.getX()-p2.getX(),2)+Math.pow(p1.getY()-p2.getY(),2));
    }

    @Override
    public double getPerimetr(){
        double a;
        double b;
        double c;

        a = getModOfVector(getP1(),getP2());
        b = getModOfVector(getP2(),getP3());
        c = getModOfVector(getP3(),getP1());

        return a + b + c;
    }

    @Override
    public double getArea(){
        double a;
        double b;
        double c;
        double p;
        double s;

        a = getModOfVector(getP1(),getP2());
        b = getModOfVector(getP2(),getP3());
        c = getModOfVector(getP3(),getP1());

        p = (a + b + c)/2;
        s = Math.sqrt(p * (p - a) * (p - b) * (p - c));

        return s;
    }

    @Override
    public String toString() {
        double a;
        double b;
        double c;

        a = getModOfVector(getP1(),getP2());
        b = getModOfVector(getP2(),getP3());
        c = getModOfVector(getP3(),getP1());

        return "Triangle{" +
                "a=" + a +
                ", b=" + b +
                ", c=" + c +
                '}';
    }
}
