package com.gmail.vladgural.OOP.Lab02.CW01;

import com.mainacad.module2.labs.Lab2_16.Lab1.Animal;

public class Main {
    public static void main(String[] args) {
        Cat cat1 = new Cat(3,6,true,"Milk","Vasya","HomeCat");
        System.out.println("This Cat like " + cat1.getRation());
        System.out.println(cat1.hashCode());

        cat1.getVoice();

        System.out.println(cat1);

        makeVoice(cat1);

        Animals[] zoo = new Animals[10];

        zoo[0] = cat1;
    }

    public static void makeVoice(Animals an){
        an.getVoice();
    }
}
