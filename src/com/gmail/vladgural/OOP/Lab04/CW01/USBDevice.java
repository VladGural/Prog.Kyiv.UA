package com.gmail.vladgural.OOP.Lab04.CW01;

public interface USBDevice {
    public int getDeviceID();

    public String getDeviceDescription();
}
