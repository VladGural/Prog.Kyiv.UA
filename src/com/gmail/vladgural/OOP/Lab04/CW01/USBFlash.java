package com.gmail.vladgural.OOP.Lab04.CW01;

public class USBFlash implements USBDevice, Comparable<USBFlash> {
    private int size;
    private String vendor;
    private String model;

    public USBFlash(int size, String vendor, String model) {
        this.size = size;
        this.vendor = vendor;
        this.model = model;
    }

    public USBFlash() {
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getVender() {
        return vendor;
    }

    public void setVender(String vender) {
        this.vendor = vender;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Override
    public String toString() {
        return "USBFlash{" +
                "size=" + size +
                ", vendor='" + vendor + '\'' +
                ", model='" + model + '\'' +
                '}';
    }

    @Override
    public int getDeviceID() {
        return hashCode();
    }

    @Override
    public String getDeviceDescription() {
        return toString();
    }

    @Override
    public int compareTo(USBFlash usbFlash) {
        return this.size - usbFlash.getSize();
    }
}
