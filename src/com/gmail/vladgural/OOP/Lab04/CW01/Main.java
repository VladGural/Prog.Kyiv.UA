package com.gmail.vladgural.OOP.Lab04.CW01;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Comp comp = new Comp("I7", 16, "AMD");
        USBFlash usbFlash1 = new USBFlash(16,"Sumsung", "Sum12");
        USBFlash usbFlash2 = new USBFlash(64,"IBM", "IBM222");
        USBFlash usbFlash3 = new USBFlash(4,"Sony", "S0012");
        USBFlash usbFlash4 = new USBFlash(8,"AMD", "AMD12");

        USBFlash[] usbflashes = {usbFlash1, usbFlash2, usbFlash3, usbFlash4};

        for(USBFlash flash: usbflashes){
            System.out.println(flash);
        }
        System.out.println();

        Arrays.sort(usbflashes);

        for(USBFlash flash: usbflashes){
            System.out.println(flash);
        }
    }
}
