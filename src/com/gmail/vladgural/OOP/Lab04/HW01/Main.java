package com.gmail.vladgural.OOP.Lab04.HW01;

public class Main {
    public static void main(String[] args) {
        Person pr01 = new Person("Vlad", "Gural", 16,Sex.MALE);
        Person pr02 = new Person("Igor", "Makarenko", 22, Sex.MALE);
        Person pr03 = new Person("Vladimir","Bormotov",17, Sex.MALE);
        Person pr04 = new Person("Vladimir","Generalov",25, Sex.MALE);
        Person pr05 = new Person("Natalya","Litvinova",20, Sex.FEMALE);
        Person pr06 = new Person("Alyena","Povaliy",16, Sex.FEMALE);
        Person pr07 = new Person("Vladimir","Abramov",17, Sex.MALE);
        Person pr08 = new Person("Andrey","Pakormlyak",28, Sex.MALE);
        Person pr09 = new Person("Phelics","Tabakin", 16, Sex.MALE);
        Person pr10 = new Person("Oleg","Komov",17, Sex.MALE);

        Group group = new Group("АСУ-1990-А");

        group.insertStudent(pr01,3);
        group.insertStudent(pr02,6);

        group.insertStudent(pr03,8);

        //group.insertStudent();


        group.insertStudent(pr04,5);
        group.insertStudent(pr05,7);
        group.insertStudent(pr06,9);
        group.insertStudent(pr07,3);
        group.insertStudent(pr08,5);
        //group.insertStudent(pr09,6);
        //group.insertStudent(pr10,7);

        System.out.println("Print not sorted Group");
        System.out.println(group.toString());

        group.sortByParameter(SortParameter.FIRST_NAME);

        System.out.println("Print sorted Group by first name");
        System.out.println(group.toString());

        group.sortByParameter(SortParameter.LAST_NAME);

        System.out.println("Print sorted Group by last name");
        System.out.println(group.toString());

        group.sortByParameter(SortParameter.MIDDLE_MARK);

        System.out.println("Print sorted Group by middle mark");
        System.out.println(group.toString());
        System.out.println();

        Student[] recruts;
        recruts = group.getRecrut();

        System.out.println("Recruts in this group are");
        for (Student st:recruts) {
            System.out.println(st);
        }



    }
}
