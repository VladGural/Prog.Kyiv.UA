package com.gmail.vladgural.OOP.Lab04.HW01;

public class Student extends Person implements Comparable<Student> {
    private String nameOfGroup;
    private int middleMark;

    public Student(Person person, String nameOfGroup, int middlemark) {
        super.setFirstName(person.getFirstName());
        super.setLastName(person.getLastName());
        super.setAge(person.getAge());
        super.setSex(person.getSex());
        this.middleMark = middlemark;
        this.nameOfGroup = nameOfGroup;
    }

    public Student() {
    }

    public String getNameOfGroup() {
        return nameOfGroup;
    }

    public void setNameOfGroup(String nameOfGroup) {
        this.nameOfGroup = nameOfGroup;
    }

    public int getMiddlemark() {
        return middleMark;
    }

    public void setMiddlemark(int middlemark) {
        this.middleMark = middlemark;
    }

    @Override
    public int compareTo(Student student){
        return this.getLastName().compareTo(student.getLastName());
    }

    @Override
    public String toString() {
        return " " +
                super.toString() +
                " middleMark= " + middleMark +
                " nameOfGroup='" + nameOfGroup + '\'';
    }
}
