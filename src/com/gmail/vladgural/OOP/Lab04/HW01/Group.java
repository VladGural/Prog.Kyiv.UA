package com.gmail.vladgural.OOP.Lab04.HW01;

import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Group implements Voencom{
    private Student[] students = new Student[10];
    private int numberOfStudents;
    private String nameOfGroup;

    public Group(String nameOfGroup) {
        this.nameOfGroup = nameOfGroup;
    }

    public Group() {
    }

    public Student[] getStudents() {
        return students;
    }

    public int getNumberOfStudents() {
        return numberOfStudents;
    }

    public String getNameOfGroup() {
        return nameOfGroup;
    }

    private String getStudentFirstName(Scanner scanner, String str){ //"Enter students' first name"
        while (true){
            System.out.println(str);
            return scanner.nextLine();
        }
    }

    private String getStudentLastName(Scanner scanner, String str){//"Enter students' last name"
        while (true){
            System.out.println(str);
            return scanner.nextLine();
        }
    }

    private int getStudentAge(Scanner scanner, String str){//"Enter students' age"
        while (true){
            try {
                System.out.println(str);
                int age = scanner.nextInt();
                scanner.nextLine();
                return age;
            }catch (InputMismatchException e){
                System.out.println("Enter number");
                scanner.nextLine();
            }
        }
    }

    private Sex getStudentSex(Scanner scanner, String str){//"Enter students' sex (1 = male or 2 = female)"
        String s;
        while (true){
            System.out.println(str);
            s = scanner.nextLine();

            if(s == null)
                s = "male";

            if(s.equalsIgnoreCase("male")){
                return Sex.MALE;
            }else {
                return Sex.FEMALE;
            }
        }
    }

    private int getStudentMiddleMark(Scanner scanner, String str){//"Enter students' middle mark"
        while (true){
            try {
                System.out.println(str);
                int middleMark = scanner.nextInt();
                scanner.nextLine();
                return middleMark;
            }catch (InputMismatchException e){
                System.out.println("Enter number");
                scanner.nextLine();
            }
        }
    }

    public void insertStudent(){
        Scanner scanner = new Scanner(System.in);
        Person person = new Person();
        int middleMark;
        System.out.println("Insert new student into group " + nameOfGroup);

        person.setFirstName(getStudentFirstName(scanner,"Enter students' first name"));
        person.setLastName(getStudentLastName(scanner, "Enter students' last name"));
        person.setAge(getStudentAge(scanner, "Enter students' age"));
        person.setSex(getStudentSex(scanner, "Enter students' sex (male or female)"));
        middleMark = getStudentMiddleMark(scanner,"Enter students' middle mark");

        scanner.close();

        insertStudent(person, middleMark);

    }


    public void insertStudent(Person person, int middleMark){
        try {
            if(numberOfStudents==10){
                throw new GroupIsFullException();
            }
            for(int i = 0; i<numberOfStudents; i++){  // Нельзя вставить студента с такойже Фанилией
                if(students[i].getLastName().equals(person.getLastName())){
                    return;
                }
            }
            students[numberOfStudents] = new Student(person, nameOfGroup, middleMark);
            numberOfStudents++;
        }catch(GroupIsFullException e){
            System.out.println("We cannot insert new student because group is full \n");
        }
    }

    public void deleteOfStudent(String lastName){
        int number = -1;
        for(int i=0; i<numberOfStudents; i++){
            if(students[i].getLastName().equals(lastName)){
                number = i;
                break;
            }
        }
        if(number!=-1){
            if(number == 0 && numberOfStudents == 1){
                students[number] = null;
                numberOfStudents--;
            }else if(number == 9){
                students[number] = null;
                numberOfStudents--;
            }else {
                for (int i = number; i < (numberOfStudents - 1); i++) {
                    students[i] = students[i + 1];
                }
                students[numberOfStudents - 1] = null;
                numberOfStudents--;
            }
        }
    }

    public void sortByParameter(SortParameter parameter){
        switch (parameter) {
            case FIRST_NAME:
                Arrays.sort(students, (a, b)-> {
                    if(CheckNull.checkNull(a,b)!=CheckNull.NOT_NULL){
                        return CheckNull.checkNull(a,b);
                    }
                        return a.getFirstName().compareTo(b.getFirstName());
                    });
                break;
            case LAST_NAME:
                Arrays.sort(students, (a, b)-> {
                    if(CheckNull.checkNull(a,b)!=CheckNull.NOT_NULL){
                        return CheckNull.checkNull(a,b);
                    }
                    return a.getLastName().compareTo(b.getLastName());
                });
                break;
            case MIDDLE_MARK:
                Arrays.sort(students, (a, b)-> {
                    if(CheckNull.checkNull(a,b)!=CheckNull.NOT_NULL){
                        return CheckNull.checkNull(a,b);
                    }
                    return a.getMiddlemark() - b.getMiddlemark();
                });
                break;
        }
    }

    public Student[] getRecrut(){
        Student[] recruts;
        int numberOfRecruts=0;
        for(int i = 0; i < numberOfStudents; i++){
            if(students[i].getAge()>18 && students[i].getSex() == Sex.MALE){
                numberOfRecruts ++;
            }
        }
        recruts = new Student[numberOfRecruts];
        int j = 0;
        for(int i = 0; i < numberOfStudents; i++){
            if(students[i].getAge()>18 && students[i].getSex() == Sex.MALE){
                recruts[j++]=students[i];
            }
        }

        return recruts;
    }

    @Override
    public String toString() {
        String str;

        if(numberOfStudents==0){
            str = "Group " + nameOfGroup + " is enpty \n";
        }else {

            str = "Students of group " + nameOfGroup + " are \n";
            for (int i = 0; i < numberOfStudents; i++) {
                str += students[i].toString();
                str += "\n";
            }
            str += "and total number of students is " + numberOfStudents + "\n";
        }
        return str;
    }
}
